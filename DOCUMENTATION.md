# Dokumentation Website SONOTEC
## TYPO3-Installation
- TYPO3 v10.4.37
- PHP 7.4.33
- Hinweise zur Installation: siehe README.md

## Lokale Entwicklung
- ddev / Docker
- Assets (JS, CSS): siehe README.md

## Seitenbaum im Backend
- SONOTEC: sonotec.de / sonotec.eu / sonotecusa.com
  - Homepage
- Addi-q.com: addi-q.com
  - zusätzliche Landingpage

<div style="page-break-after: always; visibility: hidden">
\pagebreak
</div>

## Extension-Repository
**webp**\
rendert Bilder zusätzlich als webp, htaccess-Direktive liefert webp anstelle von jpg oder png aus, sofern der Browser webp interpretieren kann

**rte-ckeditor-image**\
ermöglicht Bilder im CK-Editor

**cookieman**\
stellt Cookie-Dialog bereit. Tracking-Scripte werden unter der Bedingung geladen, dass der Benutzer dem Tracking zugestimmt hat.

**min**\
komprimiert Quellcode (entfernt unnötige Leerzeichen) und Assets

**gridelements**\
Grid-Container für Content-Elemente

**bootstrap-grids**\
Vorkonfiguration von Standard-Container für gridelements basierend auf bootstrap 4

**content-defender**\
Zusatzkonfiguration für gridelements. Konfiguriert die zulässigen Inhaltselemente pro Spalte in gridelements und normalen TYPO3-Inhaltsspalten

**news**\
Pressemitteilungen, Blog (konfiguriert, jedoch nicht freigeschaltet), Veranstaltungen/Messen - siehe:\
https://www.sonotec.eu/en/news/press-releases/

**spreadsheets**\
Inhaltselement zur Ausgabe von Tabellen im Frontend, deren Inhalt aus einem Excel-Sheet generiert wird

**typo3db-legacy**\
Legacy-DB-Abstraktion `$GLOBALS['TYPO3_DB']`

**femanager**\
Frontend-Benutzer-Registrierung für Login-Bereich unter **mysonaphone**

**fe_change_pwd**\
PW-Änderung für Frontend-Benutzer.

**typo3-form-to-database**\
speichert Formulardaten in der Datenbank. Scheduler-Task löscht Datensätze älter als 180 Tage

**static-info-tables**\
Datenbank-Dump mit Informationen zu Landesnamen, Währungen etc.

**static-info-tables-de**\
Datenbank-Dump deutscher Übersetzungen für static-info-tables

**web2pdf**\
rendert HTMl als PDF und Verwendung der Styles unter `@media print`

**trusted-url-params**\
Bugfix für addQueryString bei TypoLink für TYPO3 v9 und v10

**deletefiles**\
Scheduler-Task zum Löschen von Dateien / Verzeichnissen anhand des Datei-Alters. Löscht hochgeladene Formular-Daten (Bilder & PDFs bei Bewerbungen)

**fal-protect**\
Konfiguration für geschützte / nicht öffentlich erreichbare Verzeichnisse im fileadmin

<div style="page-break-after: always; visibility: hidden">
\pagebreak
</div>

## Lokale Extensions
**bb_extend_news**\
erweitert news-Extension. Stellt zusätzlichen Datensatz für Autoren bereit und verknüpft diesen mit news-Datensätzen. Frontend: Listing aller Autoren sowie Listing von News-Artikeln nach Autor.

**ff_country_select**\
erweitert form-Extension. Rendert Select-Feld mit Länderauswahl aus **static_info_tables** für Formulare

**ff_friendly_captcha**\
 erweitert form-Extension. Rendert ein FriendlyCaptcha. Credentials für FriendlyCaptcha-Zugang sind über SONOTEC zu beziehen. Key & Secret über `Backend>Settings>Extension Configuration` einsehbar.

**ff_privacy**: erweitert form-Extension. Rendert eine Checkbox, Label der Checkbox wird aus verknüpften Inhaltselement gebildet - ermöglicht verlinkten Text in Label sowie wiederverwertbare Label-Texte aus einem zentral hinterlegten Inhaltselement, z.B. für Checkbox Datenschutz

**smapone_jobposting**\
Listing und Detailansicht für Jobangebote. Zusätzlich können die Jobangebote über bereitgestelltes XML automatisch bei XING publiziert werden. Nicht mehr in Verwendung, kann vermutlich entfernt werden.

**sonotec_config**\
siehe separaten Abschnitt unten

**sonotec_configurator**\
Konfigurator für die Auswahl von Sensor-Typen anhand von Filterkriterien. Sensor-Typen werden im Backend unter `SONOTEC>Inhalte>Sensor Types` gepflegt.\
Beispiel: https://www.sonotec.eu/en/products/non-invasive-fluid-monitoring/expertise/sensor-selection/

**sonotec_contact**\
Kontaktverwaltung für Ansprechpartner, die unter `SONOTEC>Inhalte>Ansprechpartner` gepflegt werden. Hinterlegte E-Mail-Adressen können als Empfänger beim Kontaktformular dienen

**sonotec_customers**\
siehe separaten Abschnitt unten

**sonotec_map**\
Übersichtskarte mit Auslandsvertretungen / Ansprechpartnern weltweit. Ansprechpartner können direkt über das Kontaktformular kontaktiert werden. Inhalte pflegbar unter `SONOTEC>Inhalte>SONOTEC Worldwide`. \
Einbindung Karte: https://www.sonotec.eu/en/sonotec-worldwide/

**sonotec_references**\
Inhaltselement Slider für Referenzen / Kundenstimmer , pflegbar im Verzeichnis `SONOTEC>Inhalte>Kundenstimmen`

**sonotec_retour**\
erweitert form-Extension. Stellt zusätzliches Select-Feld mit den verfügbaren Produkten zur Verfügung, die im Verzeichnis `SONOTEC>Inhalte>Retour Produkte` pflegbar sind. Stellt Konfiguration für das Retouren-Formular bereit:\
https://www.sonotec.eu/en/service-returns/

## sonotec_config
Website-Konfiguration. Erweitert die Tabellen `pages` und `tt_content`.

In den Seiteneingeschaften lässt sich festlegen, ob am Seitenende ein Formular angezeigt werden soll. Dazu gibt es eine Auswahl verschiedener Formularvarianten, die ausgespielt werden können. Der Ansprechpartner (E-Mail-Empfänger) kann über einen verknüpften Kontaktdatensatz festgelegt werden.

Für die Navigationspunkte der zweiten Ebene lässt sich ein Icon festlegen.

Zudem lässt sich ein "Karriere-Popup" aktivieren, dass bei Seitenaufruf angezeigt wird.

### Classes

`Classes\Domain\Finishers\EmailFinisher.php`\
Überschreibt den Standard-Email-Finisher. E-Mail-Empfänger wird dynamisch gesetzt, falls entsprechende Paremter übergeben werden. Oder ein Ansprechpartner in den Seiteneigenschaften hinterlegt ist

`Classes\Hooks\ReceiverPrefill.php`\
Sorgt dafür, dass hidden Inputs vorausgefüllt werden:
* Tracking-Parameter
* Seitenpfad bzw. Seitenpfad Referer-Url
* Anzeige der Empfänger-E-Mail

`Classes\Hooks\SaveCloseHook.php`\
Fügt einen "Speichern & schließen"-Button im Backend hinzu

`Classes\HrefLang\EventListener\RemoveHrefLangTags.php`\
Entfernt hreflang-Tags von Übersetzungen, die nicht existieren

`Classes\Middleware\Redirect.php`\
Redirect auf sonotec.eu/en/, falls sonotec.eu direkt aufgerufen wird - also die entsprechenden Segmente für konfigurierte Sprachen (en,fr,it,es) fehlen.

`Classes\Middleware\Session.php`\
Speichert GTM-Parameter in Session, damit diese, falls gesetzt, an Formulare weitergegeben werden können

<div style="page-break-after: always; visibility: hidden">
\pagebreak
</div>

`Classes\Service\FormRecipient.php`
* **getContact:** setzt den Formular-Empfänger:
  * default: Kontakt ist in den Seiteneigenschaften hinterlegt
  * Sonotec Worldwide: Kontakt-ID wird als Get-Parameter übergeben - Empfänger-E-Mail-Adresse wird aus den hinterlegten Kontaktdaten ermittelt
* **getRecipientByProduct:** Retouren-Formular, Empfänger-Adresse wird anhand der zugehörigen Produktgruppe gesetzt

`Classes\Utility\TranslationUtility.php`\
erweitert `Classes\HrefLang\EventListener\RemoveHrefLangTags.php`

### Configuration
`Configuration/TypoScript/setup`\
TypoScript-Konfiguration für Installation

`Configuration/TypoScript/Environment`\
* **Local:** Settings für lokales Arbeiten\
* **Testing:** Settings für Staging-Umgebung
* **Testing-FF:** Settings für temporäre Staging-Umgebung

### Resources
`Resources/Private/ContentElements`\
Layouts / Partials / Templates Inhaltselemente

`Resources/Private/Extensions`\
Überschreibt Layouts / Partials / Templates der verwendeten Extensions

`Resources/Private/Forms`\
Konfiguration für Formulare

`Resources/Private/Language`\
Sprachdateien

`Resources/Private/Website`\
Layouts / Partials / Templates Website

<div style="page-break-after: always; visibility: hidden">
\pagebreak
</div>

## sonotec_customers
Konfiguration für Login-Bereich **mysonaphone** (https://www.sonotec.eu/en/mysonaphonecom/)

### Registrierung
Für die Registrierung kommt die Extension `femanager` zum Einsatz. Das Registrierungs-Formular ist erweitert durch die Custom-Felder "Gerätetyp" und "Seriennummer". Bei Registrierung wird geprüft, ob die Seriennummer vorhanden und noch nicht anderweitig vergeben ist. Bei positiver Prüfung wird die Seriennummer mit dem neu angelegten FE-User verknüpft.

### Login
Im Login-Bereich werden dem FE-User anhand des verknüpften Gerätetyps (der an der Seriennummer hängt) Software-Updates zum Download gelistet.

### Import Seriennummern
Die Seriennummern werden durch SONOTEC als CSV-Datei auf dem Web-Server (`fileadmin/csv-import/sp3sn.csv`) abgelegt und per Scheduler-Task einmal pro Stunde in die TYPO3-Datenbank importiert.

### Backend-Modul "Release-Notification"
Newsletter-/Mailing-Modul. Versendet Mails an Benutzer, die bei Registrierung eingewilligt haben, Info-Mails zu beziehen.

### Classes
`Controller/BackendController.php`\
Backend-Modul "Release-Notification"

`Controller/ConfirmController.php`\
Handelt Double-Opt-In für den Empfang der "Release-Notifications"

`Controller/ContactController.php`\
Deprecated / nicht mehr in Verwendung: Ansprechpartner

`Controller/EditController.php`\
Erweitert femanager. Fügt Länder-Dropdown zum Profil-bearbeiten-Formular hinzu.

`Controller/LicenseController.php`\
Listet verknüpfte Lizenzen. Nicht im Einsatz.\
Wurde bei Erstprogrammierung vorgesehen - Thema wurde nicht weiter behandelt.

`Controller/NewController.php`\
Erweitert femanager.
- Fügt Länder-Dropdown zum Registrierungs-Formular hinzu.
- Fügt die Felder Seriennummer und Gerätetyp zum Registrierungs-Formular hinzu.

`Controller/SerialnumberController.php`\
Listet die zu den verknüpften Seriennummer zugehörigen Software-Updates auf der Übersichtsseite.

`Controller/TypeController.php`\
Nicht im Einsatz. Default-Controller aus Extension-Builder

`Controller/UserController.php`
- **profileAction:** Listet User-Angaben
- **deleteAction:** Löscht Benutzer

`Middleware/DownloadService.php`\
Löst anhand einer übergebenen File-ID einen Download aus einem geschützten Verzeichnis aus.

`Service/CountryService.php`\
Rendert Dropdown mit Länderauswahl

`Service/DeviceService.php`\
Rendert Dropdown mit Geräteauswahl

`Task/ImportTask.php`\
Scheduler-Task für den Import der Seriennummer aus hinterlegtem CSV-File

`Utility/MailUtility.php`\
Mail-Versand "Release-Notifications"
