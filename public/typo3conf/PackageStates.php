<?php
# PackageStates.php

# This file is maintained by TYPO3's package management. Although you can edit it
# manually, you should rather use the extension manager for maintaining packages.
# This file will be regenerated automatically if it doesn't exist. Deleting this file
# should, however, never become necessary if you use the package commands.

return [
    'packages' => [
        'core' => [
            'packagePath' => 'typo3/sysext/core/',
        ],
        'scheduler' => [
            'packagePath' => 'typo3/sysext/scheduler/',
        ],
        'extbase' => [
            'packagePath' => 'typo3/sysext/extbase/',
        ],
        'fluid' => [
            'packagePath' => 'typo3/sysext/fluid/',
        ],
        'frontend' => [
            'packagePath' => 'typo3/sysext/frontend/',
        ],
        'fluid_styled_content' => [
            'packagePath' => 'typo3/sysext/fluid_styled_content/',
        ],
        'filelist' => [
            'packagePath' => 'typo3/sysext/filelist/',
        ],
        'impexp' => [
            'packagePath' => 'typo3/sysext/impexp/',
        ],
        'form' => [
            'packagePath' => 'typo3/sysext/form/',
        ],
        'install' => [
            'packagePath' => 'typo3/sysext/install/',
        ],
        'info' => [
            'packagePath' => 'typo3/sysext/info/',
        ],
        'linkvalidator' => [
            'packagePath' => 'typo3/sysext/linkvalidator/',
        ],
        'reports' => [
            'packagePath' => 'typo3/sysext/reports/',
        ],
        'redirects' => [
            'packagePath' => 'typo3/sysext/redirects/',
        ],
        'recordlist' => [
            'packagePath' => 'typo3/sysext/recordlist/',
        ],
        'backend' => [
            'packagePath' => 'typo3/sysext/backend/',
        ],
        'indexed_search' => [
            'packagePath' => 'typo3/sysext/indexed_search/',
        ],
        'recycler' => [
            'packagePath' => 'typo3/sysext/recycler/',
        ],
        'setup' => [
            'packagePath' => 'typo3/sysext/setup/',
        ],
        'rte_ckeditor' => [
            'packagePath' => 'typo3/sysext/rte_ckeditor/',
        ],
        'belog' => [
            'packagePath' => 'typo3/sysext/belog/',
        ],
        'beuser' => [
            'packagePath' => 'typo3/sysext/beuser/',
        ],
        'extensionmanager' => [
            'packagePath' => 'typo3/sysext/extensionmanager/',
        ],
        'felogin' => [
            'packagePath' => 'typo3/sysext/felogin/',
        ],
        'lowlevel' => [
            'packagePath' => 'typo3/sysext/lowlevel/',
        ],
        'seo' => [
            'packagePath' => 'typo3/sysext/seo/',
        ],
        't3editor' => [
            'packagePath' => 'typo3/sysext/t3editor/',
        ],
        'tstemplate' => [
            'packagePath' => 'typo3/sysext/tstemplate/',
        ],
        'viewpage' => [
            'packagePath' => 'typo3/sysext/viewpage/',
        ],
        'cookieman' => [
            'packagePath' => 'typo3conf/ext/cookieman/',
        ],
        'news' => [
            'packagePath' => 'typo3conf/ext/news/',
        ],
        'sonotec_contact' => [
            'packagePath' => 'typo3conf/ext/sonotec_contact/',
        ],
        'spreadsheets' => [
            'packagePath' => 'typo3conf/ext/spreadsheets/',
        ],
        'sonotec_config' => [
            'packagePath' => 'typo3conf/ext/sonotec_config/',
        ],
        'static_info_tables' => [
            'packagePath' => 'typo3conf/ext/static_info_tables/',
        ],
        'femanager' => [
            'packagePath' => 'typo3conf/ext/femanager/',
        ],
        'fe_change_pwd' => [
            'packagePath' => 'typo3conf/ext/fe_change_pwd/',
        ],
        'sonotec_customers' => [
            'packagePath' => 'typo3conf/ext/sonotec_customers/',
        ],
        'addons_em' => [
            'packagePath' => 'typo3conf/ext/addons_em/',
        ],
        'static_info_tables_de' => [
            'packagePath' => 'typo3conf/ext/static_info_tables_de/',
        ],
        'gridelements' => [
            'packagePath' => 'typo3conf/ext/gridelements/',
        ],
        'bootstrap_grids' => [
            'packagePath' => 'typo3conf/ext/bootstrap_grids/',
        ],
        'rte_ckeditor_image' => [
            'packagePath' => 'typo3conf/ext/rte_ckeditor_image/',
        ],
        'bb_extend_news' => [
            'packagePath' => 'typo3conf/ext/bb_extend_news/',
        ],
        'content_defender' => [
            'packagePath' => 'typo3conf/ext/content_defender/',
        ],
        'deletefiles' => [
            'packagePath' => 'typo3conf/ext/deletefiles/',
        ],
        'fal_protect' => [
            'packagePath' => 'typo3conf/ext/fal_protect/',
        ],
        'ff_form_country_select' => [
            'packagePath' => 'typo3conf/ext/ff_form_country_select/',
        ],
        'ff_friendly_captcha' => [
            'packagePath' => 'typo3conf/ext/ff_friendly_captcha/',
        ],
        'ff_privacy' => [
            'packagePath' => 'typo3conf/ext/ff_privacy/',
        ],
        'form_to_database' => [
            'packagePath' => 'typo3conf/ext/form_to_database/',
        ],
        'min' => [
            'packagePath' => 'typo3conf/ext/min/',
        ],
        'smapone_jobposting' => [
            'packagePath' => 'typo3conf/ext/smapone_jobposting/',
        ],
        'sonotec_configurator' => [
            'packagePath' => 'typo3conf/ext/sonotec_configurator/',
        ],
        'sonotec_map' => [
            'packagePath' => 'typo3conf/ext/sonotec_map/',
        ],
        'sonotec_references' => [
            'packagePath' => 'typo3conf/ext/sonotec_references/',
        ],
        'sonotec_retour' => [
            'packagePath' => 'typo3conf/ext/sonotec_retour/',
        ],
        'typo3db_legacy' => [
            'packagePath' => 'typo3conf/ext/typo3db_legacy/',
        ],
        'web2pdf' => [
            'packagePath' => 'typo3conf/ext/web2pdf/',
        ],
        'webp' => [
            'packagePath' => 'typo3conf/ext/webp/',
        ],
    ],
    'version' => 5,
];
