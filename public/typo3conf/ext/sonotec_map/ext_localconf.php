<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'SonotecMap',
	'Pi1',
	array(
        \TYPO3\SonotecMap\Controller\MapController::class=>'index',
		\TYPO3\SonotecMap\Controller\AddressController::class => 'list',
        \TYPO3\SonotecMap\Controller\CategoryController::class => 'list',
		\TYPO3\SonotecMap\Controller\CountryController::class => 'list'
	),
	// non-cacheable actions
	array(
        \TYPO3\SonotecMap\Controller\MapController::class=>'',
        \TYPO3\SonotecMap\Controller\AddressController::class => '',
        \TYPO3\SonotecMap\Controller\CategoryController::class => '',
        \TYPO3\SonotecMap\Controller\CountryController::class => ''
	)
);


$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1613641535] = [
    'nodeName' => 'SonotecMapElement',
    'priority' => 40,
    'class' => \TYPO3\SonotecMap\Form\Element\MapElement::class,
];
