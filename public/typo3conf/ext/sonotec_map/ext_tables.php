<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_sonotecmap_domain_model_category', 'EXT:sonotec_map/Resources/Private/Language/locallang_csh_tx_sonotecmap_domain_model_category.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_sonotecmap_domain_model_category');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_sonotecmap_domain_model_country', 'EXT:sonotec_map/Resources/Private/Language/locallang_csh_tx_sonotecmap_domain_model_country.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_sonotecmap_domain_model_country');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_sonotecmap_domain_model_address', 'EXT:sonotec_map/Resources/Private/Language/locallang_csh_tx_sonotecmap_domain_model_address.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_sonotecmap_domain_model_address');
