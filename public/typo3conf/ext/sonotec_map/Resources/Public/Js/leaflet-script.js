$(function(){
    var tiles = 'https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png',
        attribution = '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>',
        sonotecmap = initMap([33,0],2,tiles,attribution),
        markers = L.markerClusterGroup({showCoverageOnHover:false,maxClusterRadius:40});

    sonotecmap.addLayer(markers);
    markers = changeFilter(markers,addresses,sonotecmap);

    $('#sonotec-map-addresses-singleview .close').click(function(){
       // markers = changeFilter(markers,addresses,sonotecmap);
       $('#sonotec-map-addresses,#sonotec-map-addresses-singleview').hide();
       sonotecmap.zoomOut(7);
    });
    $('#sonotec-map-addresses .close').click(function(){
        $('#sonotec-map-addresses').hide();
        $('#sonotec-map-countries').val('0');
      /*
        var zoom = sonotecmap.getZoom()-7;
        console.log(zoom);
        sonotecmap.zoomOut(zoom);
        console.log(sonotecmap.getZoom());
        */

    });

    $('.sonotec-map-category').on('change',function(){
        markers = changeFilter(markers,addresses,sonotecmap);
    });

    $('#sonotec-map-countries').on('change',function(){
        $('.sonotec-map-category').each(function(){
            $(this).prop('checked',true);
        });
        markers = changeFilter(markers,addresses,sonotecmap);
    });


    $('.contactWrapper a.mail').attr('target','_top');
});

function changeFilter(markers,addresses,sonotecmap) {
    //marker layer zurücksetzen
    markers.eachLayer(function(r){
        markers.removeLayer(r);
    });
    $('#sonotec-map-addresses-singleview').hide();
    //overlay ein- und ausblenden
    if($('#sonotec-map-countries').val()>0) {
        $('#sonotec-map-addresses').show().find('li').hide();
    }
    else {
        $('#sonotec-map-addresses').hide();
    }
    var numberOfMarkers = 0;
    addresses.forEach(function(m){
        var test = false;

        $('.sonotec-map-category').each(function(){
            if(m.categories.hasOwnProperty($(this).val()) && $(this).prop('checked')) {
                test = true;
            }
        });

        if(($('#sonotec-map-countries').val()>0 && ($('#sonotec-map-countries').val()==m.country) && test) || ($('#sonotec-map-countries').val()==0 && test)) {
            test = true;
        }
        else {
            test = false;
        }

        if(test) {
            var myIcon = L.divIcon({html:'<span class="ff-map-icon"></span> '});
            var marker = L.marker(new L.LatLng(m.coordsLon,m.coordsLat), { icon: myIcon});
            marker.on('click',function(){markerClick(m,sonotecmap)});
            markers.addLayer(marker);
            $('#sonotec-map-addresses li.address-'+m.uid).show();
            numberOfMarkers++;
        }
    });

    if(numberOfMarkers==0) {
        sonotecmap.setView([33,0],2);
        $('#sonotec-map-addresses,#sonotec-map-addresses li').hide();
    }
    else {
        sonotecmap.fitBounds(markers.getBounds(), {paddingTopLeft: [($('#sonotec-map-countries').val()>0 ? 300 : 0 ),0],maxZoom:12});
        sonotecmap.invalidateSize();
    }
    return markers;
}

function markerClick(marker,map) {
    $('#sonotec-map-addresses').show().find('li').hide();
    $('#sonotec-map-addresses-singleview').show().find('.content').html($('#sonotec-map-addresses li.address-'+marker.uid).html());
    map.fitBounds(L.latLngBounds(new L.LatLng(marker.coordsLon,marker.coordsLat), new L.LatLng(marker.coordsLon,marker.coordsLat)), {paddingTopLeft: [300,0],maxZoom:12});
}


function initMap(center,zoom,tiles,attribution) {
    var map = L.map('sonotec-map',
        {
            zoomControl: false,
            minZoom:'1',
            scrollWheelZoom:false,
            maxBounds:[[-90, -180],[90, 180]]
    });
    map.on('load', function(e) {
        //$('#sonotec-map-loading-animation').hide();
    });
    map.setView(center, zoom);
    L.control.zoom({
        position:'topright'
    }).addTo(map);
    L.tileLayer(tiles,{maxZoom: 18,attribution: attribution}).addTo(map);
    return map;
}


/*

function makeMarker(map,coords,title,description,color,image,url,active){
    var myIcon = L.divIcon({html:'<span class="ff-map-icon ff-map-icon-id" style="background:'+color+'"></span> ',popupAnchor: [1, -25]});
    var marker = L.marker(coords, {icon: myIcon}).addTo(map);
    if(title || description || image) {
        marker.bindPopup(image+"<b>"+title+"</b><br />"+description+url);
        if(active) marker.openPopup();
        var popup = L.popup();
    }
};
*/
function onMapClick(e) {
  console.log(e.latlng);
    popup.setLatLng(e.latlng).openOn(map);
}
