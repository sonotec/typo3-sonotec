
class MapElement {
  constructor()
  {
    this.center = sonotecLat && sonotecLon ? [sonotecLat,sonotecLon] :  [51.188327697178906,10.486450195312502];
    this.map = this.initMap(this.center);
    this.initTiles(this.map);
    this.initMarker(this.map,this.center);
    this.map.invalidateSize();
    this.refreshMap(this.map);
  }

  refreshMap(map) {
    document.getElementById('reload-map').addEventListener('click',function(e){
      e.preventDefault();
      map.invalidateSize();
    });
  }

  initMarker(map,center) {
    let myIcon = L.divIcon({html:'<span class=\"ff-map-icon\"></span> ',iconSize: [36, 36],iconAnchor: [36, 18]});
    let marker = L.marker(center,{draggable:true,icon: myIcon});

    marker.on('dragend',function(event){
      let marker = event.target;
      let position = marker.getLatLng();
      document.getElementById('ff-maplication--coordinates').value = position.lat+','+position.lng;
      map.panTo(position);
      map.invalidateSize();
    });

    marker.addTo(map);
  }

  initMap(center) {
    return L.map(
      'sonotec-backend-map',
      {
        scrollWheelZoom: false
      }
    ).setView(
      center,6
    );
  }


  initTiles(map) {
    L.tileLayer(
      'https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png',
      {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
        maxZoom: 18
      }
    ).addTo( map );
  }
}

