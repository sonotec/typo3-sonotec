<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:sonotec_map/Resources/Private/Language/locallang_db.xlf:tx_sonotecmap_domain_model_address',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'title,street,zip,city,phone,mobile,fax,email,url,category,country,',
		'iconfile' => 'EXT:sonotec_map/Resources/Public/Icons/tx_sonotecmap_domain_model_address.gif'
	),
	'types' => array(
		'1' => array('showitem' => '

		title, street, phone, mobile, fax, email, url, category, country,--div--;Koordinaten,map,
        --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,hidden, starttime, endtime
        '),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(

        'hidden' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ],
        ],
        'starttime' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'endtime' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],



		'title' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:sonotec_map/Resources/Private/Language/locallang_db.xlf:tx_sonotecmap_domain_model_address.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'street' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:sonotec_map/Resources/Private/Language/locallang_db.xlf:tx_sonotecmap_domain_model_address.street',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 4,
				'eval' => 'trim'
			)
		),
		'zip' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:sonotec_map/Resources/Private/Language/locallang_db.xlf:tx_sonotecmap_domain_model_address.zip',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'city' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:sonotec_map/Resources/Private/Language/locallang_db.xlf:tx_sonotecmap_domain_model_address.city',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'phone' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:sonotec_map/Resources/Private/Language/locallang_db.xlf:tx_sonotecmap_domain_model_address.phone',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'mobile' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:sonotec_map/Resources/Private/Language/locallang_db.xlf:tx_sonotecmap_domain_model_address.mobile',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'fax' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:sonotec_map/Resources/Private/Language/locallang_db.xlf:tx_sonotecmap_domain_model_address.fax',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'email' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:sonotec_map/Resources/Private/Language/locallang_db.xlf:tx_sonotecmap_domain_model_address.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'url' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:sonotec_map/Resources/Private/Language/locallang_db.xlf:tx_sonotecmap_domain_model_address.url',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'category' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:sonotec_map/Resources/Private/Language/locallang_db.xlf:tx_sonotecmap_domain_model_address.category',
			'config' => array(
				'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
				'foreign_table' => 'tx_sonotecmap_domain_model_category',
                                'foreign_table_where' => 'AND tx_sonotecmap_domain_model_category.sys_language_uid IN (-1,0)',
				'MM' => 'tx_sonotecmap_address_category_mm',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0
			),
		),
		'country' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:sonotec_map/Resources/Private/Language/locallang_db.xlf:tx_sonotecmap_domain_model_address.country',
			'config' => array(
				'type' => 'select',
                'renderType' => 'selectSingle',
				'foreign_table' => 'tx_sonotecmap_domain_model_country',
                                'foreign_table_where' => 'AND tx_sonotecmap_domain_model_country.sys_language_uid IN (-1,0) ORDER BY tx_sonotecmap_domain_model_country.title ASC',
				'minitems' => 0,
				'maxitems' => 1,
			),
		),
        'map' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:sonotec_map/Resources/Private/Language/locallang_db.xlf:tx_sonotecmap_domain_model_address.map',
			'config' => array(
                'type' => 'user',
                'renderType' => 'SonotecMapElement'
			),
		),

	),
);
