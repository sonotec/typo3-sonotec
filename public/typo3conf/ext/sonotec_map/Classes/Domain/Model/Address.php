<?php
namespace TYPO3\SonotecMap\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Felix Franz <email@felixfranz.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Address
 */
class Address extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * title
	 *
	 * @var string
	 */
	protected $title = '';

        /**
	 * map
	 *
	 * @var string
	 */
	protected $map = '';

	/**
	 * street
	 *
	 * @var string
	 */
	protected $street = '';

	/**
	 * zip
	 *
	 * @var string
	 */
	protected $zip = '';

	/**
	 * city
	 *
	 * @var string
	 */
	protected $city = '';

	/**
	 * phone
	 *
	 * @var string
	 */
	protected $phone = '';

	/**
	 * mobile
	 *
	 * @var string
	 */
	protected $mobile = '';

	/**
	 * fax
	 *
	 * @var string
	 */
	protected $fax = '';

	/**
	 * email
	 *
	 * @var string
	 */
	protected $email = '';

        /**
	 * emailMasked
	 *
	 * @var string
	 */
	protected $emailMasked = '';

	/**
	 * url
	 *
	 * @var string
	 */
	protected $url = '';

	/**
	 * country
	 *
	 * @var \TYPO3\SonotecMap\Domain\Model\Country
	 */
	protected $country = NULL;

	/**
	 * category
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\SonotecMap\Domain\Model\Category>
	 */
	protected $category = NULL;

    /**
	 * categoryjson
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\SonotecMap\Domain\Model\Category>
	 */
	protected $categoryjson = NULL;

	/**
	 * Returns the title
	 *
	 * @return string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

        /**
	 * Returns the map
	 *
	 * @return string $map
	 */
	public function getMap() {
		return explode(',',$this->map);
	}

	/**
	 * Sets the map
	 *
	 * @param string $map
	 * @return void
	 */
	public function setMap($map) {
		$this->map = $map;
	}

	/**
	 * Returns the street
	 *
	 * @return string $street
	 */
	public function getStreet() {
		return $this->street;
	}

	/**
	 * Sets the street
	 *
	 * @param string $street
	 * @return void
	 */
	public function setStreet($street) {
		$this->street = $street;
	}

	/**
	 * Returns the zip
	 *
	 * @return string $zip
	 */
	public function getZip() {
		return $this->zip;
	}

	/**
	 * Sets the zip
	 *
	 * @param string $zip
	 * @return void
	 */
	public function setZip($zip) {
		$this->zip = $zip;
	}

	/**
	 * Returns the city
	 *
	 * @return string $city
	 */
	public function getCity() {
		return $this->city;
	}

	/**
	 * Sets the city
	 *
	 * @param string $city
	 * @return void
	 */
	public function setCity($city) {
		$this->city = $city;
	}

	/**
	 * Returns the phone
	 *
	 * @return string $phone
	 */
	public function getPhone() {
		return $this->phone;
	}

	/**
	 * Sets the phone
	 *
	 * @param string $phone
	 * @return void
	 */
	public function setPhone($phone) {
		$this->phone = $phone;
	}

	/**
	 * Returns the mobile
	 *
	 * @return string $mobile
	 */
	public function getMobile() {
		return $this->mobile;
	}

	/**
	 * Sets the mobile
	 *
	 * @param string $mobile
	 * @return void
	 */
	public function setMobile($mobile) {
		$this->mobile = $mobile;
	}

	/**
	 * Returns the fax
	 *
	 * @return string $fax
	 */
	public function getFax() {
		return $this->fax;
	}

	/**
	 * Sets the fax
	 *
	 * @param string $fax
	 * @return void
	 */
	public function setFax($fax) {
		$this->fax = $fax;
	}

	/**
	 * Returns the email
	 *
	 * @return string $email
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * Sets the email
	 *
	 * @param string $email
	 * @return void
	 */
	public function setEmail($email) {
		$this->email = $email;
	}



        /**
	 * Returns the emailMasked
	 *
	 * @return string $emailMasked
	 */
	public function getEmailMasked() {



		return str_replace(array('.','@'),array('_-dot-_','_-at-_'),$this->email);
	}

	/**
	 * Sets the emailMasked
	 *
	 * @param string $emailMasked
	 * @return void
	 */
	public function setEmailMasked($emailMasked) {
		$this->emailMasked = $emailMasked;
	}




	/**
	 * Returns the url
	 *
	 * @return string $url
	 */
	public function getUrl() {
		return $this->url;
	}

	/**
	 * Sets the url
	 *
	 * @param string $url
	 * @return void
	 */
	public function setUrl($url) {
		$this->url = $url;
	}

	/**
	 * Returns the country
	 *
	 * @return \TYPO3\SonotecMap\Domain\Model\Country $country
	 */
	public function getCountry() {
		return $this->country;
	}

	/**
	 * Sets the country
	 *
	 * @param \TYPO3\SonotecMap\Domain\Model\Country $country
	 * @return void
	 */
	public function setCountry(\TYPO3\SonotecMap\Domain\Model\Country $country) {
		$this->country = $country;
	}

	/**
	 * __construct
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		$this->category = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Adds a Category
	 *
	 * @param \TYPO3\SonotecMap\Domain\Model\Category $category
	 * @return void
	 */
	public function addCategory(\TYPO3\SonotecMap\Domain\Model\Category $category) {
		$this->category->attach($category);
	}

	/**
	 * Removes a Category
	 *
	 * @param \TYPO3\SonotecMap\Domain\Model\Category $categoryToRemove The Category to be removed
	 * @return void
	 */
	public function removeCategory(\TYPO3\SonotecMap\Domain\Model\Category $categoryToRemove) {
		$this->category->detach($categoryToRemove);
	}

	/**
	 * Returns the category
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\SonotecMap\Domain\Model\Category> $category
	 */
	public function getCategory() {
            return $this->category;
	}

    /**
	 * Returns the categoryjson
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\SonotecMap\Domain\Model\Category> $category
	 */
	public function getCategoryjson() {
            $categories = [];
            foreach($this->category as $category) {
                $categories[$category->getUid()] = $category->getUid();
            }
            return json_encode($categories);

	}



	/**
	 * Sets the category
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\SonotecMap\Domain\Model\Category> $category
	 * @return void
	 */
	public function setCategory(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $category) {
		$this->category = $category;
	}


    /**
     * mapJS
     *
     * @var string
     */
    protected $mapJS = '';

    /**
     * Returns the mapJS
     *
     * @return string $mapJS
     */
    public function getMapJS() {
        $script = [
            'uid' => $this->getUid(),
            'coordsLat' => $this->getMap()[1],
            'coordsLon' => $this->getMap()[0],
            'country' => $this->getCountry()->getUid(),
            'categories' => json_decode($this->getCategoryjson())
        ];


        return json_encode($script);
    }

    /**
     * Sets the mapJS
     *
     * @param string $mapJS
     * @return void
     */
    public function setMapJS($mapJS) {
        $this->mapJS = $mapJS;
    }
}
