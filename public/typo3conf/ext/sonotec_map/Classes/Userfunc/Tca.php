<?php
namespace TYPO3\SonotecMap\Userfunc;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Felix Franz <email@felixfranz.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Tca
 */
class Tca {

    
    
    public function record ($PA) {
       
        $out = $this->makeInput($PA);
        $out.= $this->makeMap();
        $out.= $this->scriptRecord($PA['row']);
        
        return $out;
        
    }
    
    
    /*script record*/
    protected function scriptRecord($config) {
        
        $url = $config['url'] ? '<br/><a target="_blank" href="'.$config['url'].'">'.$config['url'].'</a>' :'';
        
        return 
        '<link rel="stylesheet" href="../typo3conf/ext/sonotec_map/Resources/Public/Css/leaflet.css" />'
        .''."
          <script>
          requirejs( [
                '../typo3conf/ext/sonotec_map/Resources/Public/Js/leaflet.js',
              ], function( Leaflet ) {
                var map = L.map('map',{scrollWheelZoom:false}).setView([".($config['map'] ? $config['map']:'33,0')."], 2);
            L.tileLayer('http://{s}.tile.openstreetmap.se/hydda/full/{z}/{x}/{y}.png', {maxZoom: 18,attribution:'Tiles courtesy of <a href=\"http://openstreetmap.se/\" target=\"_blank\">OpenStreetMap Sweden</a> &ndash; Map data &copy; <a href=\"http://www.openstreetmap.org/copyright\">OpenStreetMap</a>'}).addTo(map);
            var marker = L.marker([".($config['map'] ? $config['map']:'33,0')."],{draggable:true});
            marker.addTo(map);
            marker.on('dragend',function(event){
                var marker = event.target;
                var position = marker.getLatLng();
                TYPO3.jQuery('#mapInput').val(position.lat+','+position.lng);
                map.panTo(position);
                map.invalidateSize();
            });
            setTimeout(function(){ map.invalidateSize();}, 100);
              });


            
          </script>";
    }
    
    /*map*/
    protected function makeMap($settings=array()) {
        $map = '<div style="width:100%;height:400px;" id="map"></div>';
        return $map;
    }
    
    /*input*/
    protected function makeInput($config) {
      
       $input = '<input style="width:300px;margin-bottom:1em;" id="mapInput" type="text" name="'.$config['itemFormElName'].'" value="'.($config['itemFormElValue'] ? $config['itemFormElValue'] : '33,0').'">'; 
       return $input;
    }
}