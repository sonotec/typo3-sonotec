<?php
namespace TYPO3\SonotecMap\Controller;

use Sonotec\SonotecCustomers\Service\CountryService;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Felix Franz <email@felixfranz.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * MapController
 */
class MapController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * addressRepository
	 *
	 * @var \TYPO3\SonotecMap\Domain\Repository\AddressRepository
	 * @TYPO3\CMS\Extbase\Annotation\Inject
	 */
	protected $addressRepository = NULL;

	/**
	 * action index
	 *
	 * @return void
	 */
	public function indexAction() {

        $languageAspect = GeneralUtility::makeInstance(Context::class)->getAspect('language');

        //addresses
		$addresses = $this->addressRepository->findAll();
        $this->view->assign('addresses', $addresses);

        //countries
        $this->view->assign('countries', CountryService::renderCountryListOptions());

        //categories
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_sonotecmap_domain_model_category');
        $statement = $queryBuilder
            ->select('uid', 'title', 'l10n_parent', 'pid')
            ->from('tx_sonotecmap_domain_model_category')
            ->where(
                $queryBuilder->expr()->eq('sys_language_uid', $languageAspect->getId()),
                $queryBuilder->expr()->eq('pid', $this->settings['pid'])
            )
            ->orderBy('sorting')
            ->execute();

        $fixedCategories = [];
        while ($row = $statement->fetch()) {
            $fixedCategories[] = [
                'uid' => $row['l10n_parent'] > 0 ? $row['l10n_parent'] : $row['uid'],
                'title' => $row['title']
            ];
        }

		$this->view->assign('categories', $fixedCategories);
	}
}
