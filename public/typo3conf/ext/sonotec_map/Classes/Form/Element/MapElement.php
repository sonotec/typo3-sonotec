<?php

declare(strict_types = 1);

namespace TYPO3\SonotecMap\Form\Element;

use TYPO3\CMS\Backend\Form\Element\AbstractFormElement;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Object\ObjectManager;

class MapElement extends AbstractFormElement
{
    /**
     * @return array|string
     */
    public function render()
    {
        $result = $this->initializeResultArray();

        $result['stylesheetFiles']['leaflet-vendor'] = 'EXT:sonotec_map/Resources/Public/Css/leaflet.css';
        $result['stylesheetFiles']['backend-map'] = 'EXT:sonotec_map/Resources/Public/Css/backend-map.css';

        $result['html'] .= MapElement::getMapJS();

        $result['html'] .= MapElement::getField($this->data['parameterArray']);

        return $result;
    }

    /**
     * @param $params
     * @return string
     */
    protected static function getField($params) : string
    {
        $html = [];
        $html[] = '<div class="form-control-wrap">';
        $html[] =   '<div class="form-wizards-wrap">';
        $html[] =      '<div class="form-wizards-element">';
        //coordinates input


        //map container
        $html[] = MapElement::getMapHtml($params);

        $html[] =           '<div class="form-control-clearable"><input id="ff-maplication--coordinates" type="text" readonly="1" value="'.($params['itemFormElValue'] ? $params['itemFormElValue'] : '0,0').'" class="form-control" name="'.$params['itemFormElName'].'" ></div>';
        $html[] =      '</div>';
        $html[] =   '</div>';
        $html[] = '</div>';

        return  implode(LF, $html);
    }

    /**
     * return javascript for map initialization
     *
     * @return string
     */
    private static function getMapJS() : string
    {
        return "<script>
                    requirejs( [
                        '../typo3conf/ext/sonotec_map/Resources/Public/Js/leaflet.js',
                        '../typo3conf/ext/sonotec_map/Resources/Public/Js/backend-map.js',
                    ], function( Leaflet ) {
                        let map =  document.getElementById('sonotec-backend-map');
                        if(typeof(map) != 'undefined' && map != null){
                            new MapElement();
                        }
                    });
                </script>";
    }

    /**
     * return html of map container
     *
     * @param array $params
     * @return string
     */
    private static function getMapHtml(array $params) : string
    {
        list($lat,$lon) = explode(',', $params['itemFormElValue']);

        return '<button class="btn" style="display: inline-block;margin-bottom: 10px;" id="reload-map">Karte neu laden</button>

            <div id="sonotec-backend-map" class="sonotec-map"></div>
            <script>var sonotecLat='.($lat?$lat:0).', sonotecLon='.($lon?$lon:0).'; </script>';
    }


    /**
     * Get definitive TypoScript Settings from
     * plugin.tx_snmvectormap.settings.
     *
     * @return array
     */
    private static function getSettings(): array
    {
        return GeneralUtility::makeInstance(ObjectManager::class)
                ->get(ConfigurationManagerInterface::class)
                ->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT)['plugin.']['tx_haemablooddonation.']['settings.'] ?? [];
    }
}
