<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "smapone_jobposting"
 *
 * Auto generated by Extension Builder 2019-03-06
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => 'Jobs',
    'description' => '',
    'category' => 'plugin',
    'author' => 'Felix Franz',
    'author_email' => 'email@felixfranz.de',
    'state' => 'alpha',
    'uploadfolder' => 1,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [

        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
