# smapOne Jobpostings #

### What does it do? ###

TYPO3 extension for creation of jobpostings. Outputs a list of jobpostings as xml-feed.

### How do I get set up? ###

* Install the extension
* Create a sysfolder and add "smapOne jobposting TS Page Configuration (smapone_jobposting)" in page resources for proper richtext handling
* Include the static TypoScript template "smapOne jobposting typoscript configuration (smapone_jobposting)"
* Configure "plugin.tx_smaponejobposting_jobposting.persistence.storagePid" in the constants editor. Enter the UID of the created sysfolder.
* Access the xml-feed by calling www.yourdomain.tld/?type=1982
