<?php
defined('TYPO3_MODE') || die('Access denied.');

$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['rte_smapone_jobposting'] = 'EXT:smapone_jobposting/Configuration/RTE/rte.yaml';

//register evaluation of geocoordinates
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tce']['formevals']['Smapone\\SmaponeJobposting\\Evaluation\\GeoCoordinatesEvaluation'] = '';

//register ID hook
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1612969623] = [
    'nodeName' => 'uidForPostings',
    'priority' => 40,
    'class' => \Smapone\SmaponeJobposting\UserFuncs\UidForPostings::class
];

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'SmaponeJobposting',
            'Jobposting',
            [
                \Smapone\SmaponeJobposting\Controller\JobpostingController::class => 'xmlFeed'
            ],
            // non-cacheable actions
            [
                \Smapone\SmaponeJobposting\Controller\JobpostingController::class => 'xmlFeed'
            ]
        );


        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'SmaponeJobposting',
            'Pi1',
            [
                \Smapone\SmaponeJobposting\Controller\JobController::class => 'list,show'
            ],
            // non-cacheable actions
            [
                \Smapone\SmaponeJobposting\Controller\JobController::class => ''
            ],
            \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
        );


        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'SmaponeJobposting',
            'Pi2',
            [
                \Smapone\SmaponeJobposting\Controller\JobController::class => 'slider'
            ],
            // non-cacheable actions
            [
                \Smapone\SmaponeJobposting\Controller\JobController::class => ''
            ],
            \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
        );

        $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);

        $iconRegistry->registerIcon(
            'smaponejobposting_pi1',
            \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
            ['source' => 'EXT:smapone_jobposting/Resources/Public/Icons/user_plugin_pi1.svg']
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            '
               mod.wizards.newContentElement.wizardItems.common.elements.smaponejobposting_pi1 {
                    iconIdentifier = smaponejobposting_pi1
                    title = Jobs
                    description = Jobs listing and singleview
                    tt_content_defValues {
                        CType = smaponejobposting_pi1
                    }
                }
                mod.wizards.newContentElement.wizardItems.common.show := addToList(smaponejobposting_pi1)

                mod.wizards.newContentElement.wizardItems.common.elements.smaponejobposting_pi2 {
                    iconIdentifier = smaponejobposting_pi1
                    title = Jobs Slider
                    description = Selected jobs as Slider
                    tt_content_defValues {
                        CType = smaponejobposting_pi2
                    }
                }
                mod.wizards.newContentElement.wizardItems.common.show := addToList(smaponejobposting_pi2)


           '
        );
    }
);
