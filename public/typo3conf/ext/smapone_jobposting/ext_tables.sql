#
# Table structure for table 'tx_smaponejobposting_domain_model_jobposting'
#
CREATE TABLE tx_smaponejobposting_domain_model_jobposting (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

    show_in_slider smallint(5) unsigned DEFAULT '0' NOT NULL,
    job_title varchar(255) DEFAULT '' NOT NULL,
    seo_title varchar(255) DEFAULT '' NOT NULL,
    seo_description text,
    slug varchar(2048),
    introtext text,
    description text,
    description2 text,
    description3 text,
    description4 text,
    description5 text,
    images int(11) unsigned NOT NULL default '0',
    meta_image int(11) unsigned NOT NULL default '0',
	url varchar(255) DEFAULT '' NOT NULL,
	locations int(11) unsigned DEFAULT '0',
	company_name varchar(255) DEFAULT '' NOT NULL,
    banner_image_url int(11) unsigned NOT NULL default '0',
	contact_email varchar(255) DEFAULT '' NOT NULL,
	poster_url varchar(255) DEFAULT '' NOT NULL,
	reply_setting varchar(255) DEFAULT '' NOT NULL,
	job_type varchar(255) DEFAULT '' NOT NULL,
	career_level varchar(255) DEFAULT '' NOT NULL,
	salary_range_start int(11) DEFAULT '0' NOT NULL,
    salary_range_end int(11) DEFAULT '0' NOT NULL,
    contact varchar(255) DEFAULT '' NOT NULL,
    category varchar(255) DEFAULT '' NOT NULL,
    unit varchar(255) DEFAULT '' NOT NULL,

    sorting int(11) unsigned NOT NULL default '0',
	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted smallint(5) unsigned DEFAULT '0' NOT NULL,
	hidden smallint(5) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state smallint(6) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,
	l10n_state text,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_smaponejobposting_domain_model_location'
#
CREATE TABLE tx_smaponejobposting_domain_model_location (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	city varchar(255) DEFAULT '' NOT NULL,
	country varchar(255) DEFAULT '' NOT NULL,
	address varchar(255) DEFAULT '' NOT NULL,
	zip_code varchar(255) DEFAULT '' NOT NULL,
    latitude varchar(255) DEFAULT '' NOT NULL,
    longitude varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted smallint(5) unsigned DEFAULT '0' NOT NULL,
	hidden smallint(5) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state smallint(6) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,
	l10n_state text,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)

);



#
# Table structure for table 'tx_smaponejobposting_domain_model_category'
#
CREATE TABLE tx_smaponejobposting_domain_model_category (
    title varchar(255) DEFAULT '' NOT NULL,
);

    #
# Table structure for table 'tx_smaponejobposting_domain_model_unit'
#
CREATE TABLE tx_smaponejobposting_domain_model_unit (
    title varchar(255) DEFAULT '' NOT NULL,
);
