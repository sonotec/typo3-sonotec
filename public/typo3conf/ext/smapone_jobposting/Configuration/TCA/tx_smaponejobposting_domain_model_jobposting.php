<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting',
        'label' => 'job_title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'sortby' => 'sorting',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'crdate,tstamp,job_title,description,unit,description5,description2,description3,description4,url,company_name,locations,banner_image_url,slug,contact_email,poster_url,reply_setting,job_type,career_level,salary_range_start,salary_range_end,introtext,meta_image',
        'iconfile' => 'EXT:smapone_jobposting/Resources/Public/Icons/tx_smaponejobposting_domain_model_jobposting.gif'
    ],
    'types' => [
        '1' => [
            'showitem' => '--palette--;;headerData, introtext,images, description5,description,description2,description3,description4,'
                . '--div--;LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smapone_jobposting_jobposting.tab.contact,'
                . 'locations,company_name,contact,' //reply_setting, contact_email, poster_url,
                . '--div--;XING,'
                . 'uid,--palette--;;typeData, banner_image_url,'
                . '--div--;Meta,seo_title,seo_description,meta_image,'
                /*
                . '--div--;LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smapone_jobposting_jobposting.tab.salary,'
                . '--palette--;;salaryData,'*/
                . '--div--;LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smapone_jobposting_jobposting.tab.language,'
                . 'sys_language_uid, l10n_parent, l10n_diffsource,  '
                . '--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, hidden, starttime, endtime,tstamp,crdate'],
    ],
    'palettes' => [
        'headerData' => [
            'showitem' => '
               job_title,slug,--linebreak--,show_in_slider, category,unit,
            ',
        ],
        'typeData' => [
            'showitem' => '
               job_type, career_level,
            ',
        ],
        'salaryData' => [
            'showitem' => '
               salary_range_start, salary_range_end,
            ',
        ]
    ],
    'columns' => [
        'tstamp' => [
            'label' => 'tstamp',
            'config' => [
                'type' => 'passthrough',
            ]
        ],
        'crdate' => [
            'label' => 'crdate',
            'config' => [
                'type' => 'passthrough',
            ]
        ],

        'sys_language_uid' => [

            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 1,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_smaponejobposting_domain_model_jobposting',
                'foreign_table_where' => 'AND tx_smaponejobposting_domain_model_jobposting.pid=###CURRENT_PID### AND tx_smaponejobposting_domain_model_jobposting.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [

            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/Resources/Private/Language/locallang_core.xlf:labels.enabled'
                    ]
                ],
            ],
        ],


        'starttime' => [

            'behaviour' => [
                'allowLanguageSynchronization' => true
            ],
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],
        'endtime' => [

            'behaviour' => [
                'allowLanguageSynchronization' => true
            ],
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
            ],
        ],
        'uid' => [
            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.uid',
            'config' => [
                'type' => 'user',
                'renderType' => 'uidForPostings'
            ],
        ],
        'job_title' => [

            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.job_title',
            'config' => [
                'type' => 'input',
                'eval' => 'trim,required',
                'size' => 50
            ],
        ],
        'seo_title' => [

            'label' => 'Seo title',
            'config' => [
                'type' => 'input',
                'eval' => 'trim',
                'size' => 50
            ],
        ],
        'seo_description' => [

            'label' => 'Seo description',
            'config' => [
                'type' => 'text',
                'eval' => 'trim',
                'rows' => 3
            ],
        ],

        'show_in_slider' => [

            'label' => 'In Job-Slider anzeigen',
            'config' => [
                'type' => 'check'
            ],
        ],
        'slug' => [
            'label' => 'Url-Pfad',
            'config' => [
                'type' => 'slug',
                'generatorOptions' => [
                    'fields' => ['job_title'],
                    'prefixParentPageSlug' => false,
                    'replacements' => [
                        '/' => '-',
                    ],
                ],
                'fallbackCharacter' => '-',
                'prependSlash' => false,
                'eval' => 'unique',
            ],
        ],

        'description' => [

            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.description',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 3,
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
            ],

        ],

        'description5' => [

            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.description5',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 3,
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
            ],

        ],
        'description2' => [

            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.description2',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 3,
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
            ],

        ],

        'description3' => [

            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.description3',
            'config' => [

                'type' => 'text',
                'cols' => 40,
                'rows' => 3,
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
                'default' => '<ul>
	<li>Festanstellung in einem inhabergeführten, international agierenden und wachsenden Technologieunternehmen, welches am Markt als Lösungsspezialist in der Ultraschallmesstechnik etabliert ist</li>
	<li>Ein unbefristeter Arbeitsvertrag mit flexibler Arbeitszeitgestaltung</li>
	<li>Eine strukturierte Einarbeitung sowie regelmäßige Weiterbildungsmaßnahmen</li>
	<li>Attraktiver Arbeitgeberzuschuss zur betrieblichen Altersvorsorge und verschiedene Mitarbeiterveranstaltungen</li>
	<li>Ein innovatives und dynamisches Arbeitsumfeld, eine vertrauensvolle Unternehmenskultur, die sich durch ein offenes und konstruktives Miteinander auszeichnet</li>
	<li>Abwechslungsreiche und anspruchsvolle Aufgaben</li>
	<li>Flache Hierarchien und kurze Entscheidungswege</li>
	<li>Persönlicher Freiraum mit einem hohen Maß an Eigenverantwortung</li>
</ul>
'
            ],

        ],

        'description4' => [

            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.description4',
            'config' => [

                'type' => 'text',
                'cols' => 40,
                'rows' => 3,
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
                'default' => '<p>Wollen Sie mehr erfahren oder sich direkt bewerben? Dann zögern Sie bitte nicht uns anzurufen!<br> Oder senden Sie gleich Ihre Bewerbung mit Lebenslauf, Zeugnissen, Verfügbarkeit und Gehaltsvorstellungen an:</p>'
            ],

        ],

        'introtext' => [

            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.introtext',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 3
            ],

        ],

        'company_name' => [

            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.company_name',
            'config' => [
                'type' => 'input',
                'eval' => 'trim,required',
                'default' => 'SONOTEC GmbH',
                'size' => 30
            ],
        ],
        'banner_image_url' => [

            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.banner_image_url',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'banner_image_url',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                    ],

                    'overrideChildTca' => [
                        'types' => [
                            '0' => [
                                'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                                'showitem' => '
                                    --palette--;;imageoverlayPalette,
                                    --palette--;;filePalette'
                            ],
                        ],
                        'columns' => [
                            'crop' => [
                                'config' => [
                                    'cropVariants' => [
                                        'default' => [
                                            'title' => 'Xing',
                                            'allowedAspectRatios' => [
                                                'default' => [
                                                    'title' => ' 936:200',
                                                    'value' =>936/200
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'maxitems' => 1
                ],
                'jpg,png'
            ),
        ],
        'meta_image' => [

            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.meta_image',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'meta_image',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                    ],
                    'overrideChildTca' => [
                        'types' => [
                            '0' => [
                                'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette'
                                ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                                'showitem' => '
                                    --palette--;;imageoverlayPalette,
                                    --palette--;;filePalette'
                            ],
                        ],
                        'columns' => [
                            'crop' => [
                                'config' => [
                                    'cropVariants' => [
                                        'default' => [
                                            'title' => 'Social Media',
                                            'allowedAspectRatios' => [
                                                'default' => [
                                                    'title' => ' 1600:720',
                                                    'value' => 1600/720
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                    'maxitems' => 1
                ],
                'jpg,png'
            ),
        ],

        'images' => [

            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.images',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'images',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                    ],
                    'overrideChildTca' => [
                        'types' => [
                            '0' => [
                                'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                                'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                                'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                                'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                                'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                                'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                            ]
                        ],
                    ],
                    'maxitems' => 10
                ],
                'jpg,png'
            ),
        ],

        'url' => [
            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.url',
            'config' => [
                'type' => 'input',
                'eval' => 'trim',
                'size' => 30
            ],
        ],
        'contact_email' => [
            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.contact_email',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
            ],
        ],
        'poster_url' => [
            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.poster_url',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'reply_setting' => [

            /*'onChange' => 'reload',*/
            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.reply_setting',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.reply_setting.url', 'url'],
                    ['LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.reply_setting.email', 'email'],
                    ['LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.reply_setting.private_message', 'private_message']
                ],
                'size' => 1,
                'maxitems' => 1,
                'minitems' => 1,
                'default' => 'url'
            ],
        ],
        'job_type' => [

            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.job_type',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['',''],
                    ['LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.job_type.fulltime', 'FULL_TIME'],
                    ['LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.job_type.parttime', 'PART_TIME'],
                    ['LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.job_type.contractor', 'CONTRACTOR'],
                    ['LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.job_type.intern', 'INTERN'],
                    ['LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.job_type.seasonal', 'SEASONAL'],
                    ['LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.job_type.temporary', 'TEMPORARY'],
                    ['LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.job_type.voluntary', 'VOLUNTARY'],
                ],
                'size' => 1,
                'maxitems' => 1,
            ],
        ],
        'career_level' => [

            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.career_level',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['',''],
                    ['LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.career_level.1', 'JOBLEVEL_1'],
                    ['LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.career_level.2', 'JOBLEVEL_2'],
                    ['LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.career_level.3', 'JOBLEVEL_3'],
                    ['LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.career_level.4', 'JOBLEVEL_4'],
                    ['LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.career_level.5', 'JOBLEVEL_5'],
                    ['LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.career_level.6', 'JOBLEVEL_6'],
                ],
                'size' => 1,
                'maxitems' => 1,
            ],
        ],
        'salary_range_start' => [

            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.salary_range_start',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'salary_range_end' => [

            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.salary_range_end',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'locations' => [

            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.locations',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_smaponejobposting_domain_model_location',
                'foreign_table_where' => 'AND tx_smaponejobposting_domain_model_location.sys_language_uid IN(-1,0)',
                'minitems' => 0,
                'maxitems' => 1
            ],
        ],
        'contact' => [
            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.contactsperson',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'allowed' => 'tx_sonoteccontact_domain_model_contact',
                'foreign_table' => 'tx_sonoteccontact_domain_model_contact',
                'foreign_table_where' => 'AND ORDER BY tx_sonoteccontact_domain_model_contact.name ASC',
                'maxitems' => 1,
                'minitems' => 1,
                'default' => 12
            ],
        ],
        'category' => [
            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.category',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['','']
                ],
                'foreign_table' => 'tx_smaponejobposting_domain_model_category',
                'foreign_table_where' => 'AND tx_smaponejobposting_domain_model_category.sys_language_uid IN(-1,0)',
                'minitems' => 0,
                'maxitems' => 1,
                'size' => 1,
                'required' => 1,
                'default' => '',

            ],
        ],

        'unit' => [
            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_jobposting.unit',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['','']
                ],
                'foreign_table' => 'tx_smaponejobposting_domain_model_unit',
                'foreign_table_where' => 'AND tx_smaponejobposting_domain_model_unit.sys_language_uid IN(-1,0)',
                'size' => 1,
                'maxitems' => 1,
                'minitems' => 1,
                'required' => 1,
                'default' => ''
            ],
        ],

    ],
];
