<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_location',
        'label' => 'city',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'country,city,zip_code,address',
        'iconfile' => 'EXT:smapone_jobposting/Resources/Public/Icons/tx_smaponejobposting_domain_model_location.gif'
    ],
    'types' => [
        '1' => [
            'showitem' => 'country,--palette--;;cityZipData,address,--palette--;LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_location.geo;geoData,'
                . '--div--;LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smapone_jobposting_jobposting.tab.language,'
                . 'sys_language_uid, l10n_parent, l10n_diffsource,  '
                . '--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, hidden, starttime, endtime'],
    ],
    'palettes' => [
        'cityZipData' => [
            'showitem' => '
               city,zip_code,
            ',
        ],
        'geoData' => [
            'showitem' => '
               latitude,longitude,
            ',
        ]
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_smaponejobposting_domain_model_jobposting',
                'foreign_table_where' => 'AND tx_smaponejobposting_domain_model_jobposting.pid=###CURRENT_PID### AND tx_smaponejobposting_domain_model_jobposting.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/Resources/Private/Language/locallang_core.xlf:labels.enabled'
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'behaviour' => [
                'allowLanguageSynchronization' => true
            ],
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'behaviour' => [
                'allowLanguageSynchronization' => true
            ],
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
            ],
        ],

        'address' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_location.address',
            'config' => [
                'type' => 'input',
                'size' => 30
            ],
        ],
        'zip_code' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_location.zip_code',
            'config' => [
                'type' => 'input',
                'size' => 5,
                'eval' => 'trim,num,nospace',
                'max' => 5,
                'min' => 5
            ],
        ],
        'city' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_location.city',
            'config' => [
                'type' => 'input',
                'eval' => 'trim,required',
                'size' => 30
            ],
        ],
        'country' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_location.country',
            'config' => [
                'type' => 'select',
                'eval' => 'required',
                'renderType' => 'selectSingle',
                'items' => [
                    ['LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_location.country.de', 'DE'],
                ],
                'size' => 1,
                'maxitems' => 1,
                'minitems' => 1
            ],
        ],
        'latitude' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_location.latitude',
            'config' => [
                'type' => 'input',
                'eval' => 'trim,Smapone\\SmaponeJobposting\\Evaluation\\GeoCoordinatesEvaluation',
                'size' => 30,
                'default' => '51.47067098662117'
            ],
        ],
        'longitude' => [
            'exclude' => true,
            'label' => 'LLL:EXT:smapone_jobposting/Resources/Private/Language/locallang_db.xlf:tx_smaponejobposting_domain_model_location.longitude',
            'config' => [
                'type' => 'input',
                'eval' => 'trim,Smapone\\SmaponeJobposting\\Evaluation\\GeoCoordinatesEvaluation',
                'size' => 30,
                'default' => '11.984200030370406'
            ],
        ],
    ],
];
