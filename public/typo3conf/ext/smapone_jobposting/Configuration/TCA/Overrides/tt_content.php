<?php

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'SmaponeJobposting',
    'Pi1',
    'Jobs'
);

$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['smaponejobposting_pi1'] = 'smaponejobposting_pi1';


// Configure the default backend fields for the content element
$GLOBALS['TCA']['tt_content']['types']['smaponejobposting_pi1'] = array(
    'showitem' => '
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.headers;headers,
        pi_flexform,
      --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
         --palette--;;language,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
         --palette--;;hidden,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
         categories,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
         rowDescription,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,
   '
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue('smaponejobposting_pi1', 'FILE:EXT:smapone_jobposting/Configuration/FlexForms/Jobs.xml');
$GLOBALS['TCA']['tt_content']['columns']['pi_flexform']['config']['ds'][',smaponejobposting_pi1'] = 'FILE:EXT:smapone_jobposting/Configuration/FlexForms/Jobs.xml';





\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'SmaponeJobposting',
    'Pi2',
    'Jobs Slider'
);

$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['smaponejobposting_pi2'] = 'smaponejobposting_pi1';


// Configure the default backend fields for the content element
$GLOBALS['TCA']['tt_content']['types']['smaponejobposting_pi2'] = array(
    'showitem' => '
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.headers;headers,
      --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
         --palette--;;language,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
         --palette--;;hidden,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
         categories,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
         rowDescription,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,
   '
);
