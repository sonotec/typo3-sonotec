<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

call_user_func(function () {

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
        'smapone_jobposting', 
        'Configuration/TypoScript', 
        'smapOne jobposting typoscript configuration'
    );
    
});
