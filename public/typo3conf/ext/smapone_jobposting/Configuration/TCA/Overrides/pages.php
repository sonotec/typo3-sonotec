<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

call_user_func(function () {

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
        'smapone_jobposting',
        'Configuration/PageTS/page.ts',
        'smapOne jobposting TS Page Configuration'
    );
    
});
