<?php
declare(strict_types = 1);

namespace Smapone\SmaponeJobposting\UserFuncs;

/***
 *
 * This file is part of the "smapOne Jobposting" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Felix Franz <email@felixfranz.de>
 *
 ***/

use TYPO3\CMS\Backend\Form\Element\AbstractFormElement;
use \TYPO3\CMS\Core\Utility\GeneralUtility;

/*
 * manipulate TCA fields
 */
class UidForPostings  extends AbstractFormElement
{
    /**
     * @return array
     */
    public function render() : array
    {
        $ids = [];
        foreach($this->data['databaseRow']['locations'] as $location) {
            $ids[] = 'SONOTEC' . sprintf('%06d', $this->data['parameterArray']['itemFormElValue']) .'L'.$location;
        }

        $html = ''
            . '<div class="formengine-field-item t3js-formengine-field-item"><div class="form-control-wrap">'
            . '<input type="text" readonly class="form-control" value="' . implode(' / ', $ids) . '" />'
            . '</div></div>';

        $result = $this->initializeResultArray();
        $result['html'] = $html;
        return $result;



    }

}
