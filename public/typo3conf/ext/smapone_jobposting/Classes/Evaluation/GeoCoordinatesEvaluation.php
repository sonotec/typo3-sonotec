<?php
namespace Smapone\SmaponeJobposting\Evaluation;

/***
 *
 * This file is part of the "smapOne Jobposting" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Felix Franz <email@felixfranz.de>
 *
 ***/

/**
 * GeoCoordinatesEvaluation
 */
class GeoCoordinatesEvaluation {
    
    /**
    * JavaScript code for client side validation/evaluation
    *
    * @return string JavaScript code for client side validation/evaluation
    */
   public function returnFieldJS() {
      return '
         return value.replace(/[^\d.]/g, "");
      ';
   }

   /**
    * Server-side validation/evaluation on saving the record
    *
    * @param string $value The field value to be evaluated
    * @param string $is_in The "is_in" value of the field configuration from TCA
    * @param bool $set Boolean defining if the value is written to the database or not. Must be passed by reference and changed if needed.
    * @return string Evaluated field value
    */
   public function evaluateFieldValue($value, $is_in, &$set) {
      return $this->evaluateGeodCoordinates($value);
   }

   /**
    * Server-side validation/evaluation on opening the record
    *
    * @param array $parameters Array with key 'value' containing the field value from the database
    * @return string Evaluated field value
    */
   public function deevaluateFieldValue(array $parameters) {
      return $this->evaluateGeodCoordinates($parameters['value']);
   }
   
   public function evaluateGeodCoordinates ($value) {
       return preg_replace("/[^0-9.]/", "", $value);
   }
}
