<?php
namespace Smapone\SmaponeJobposting\Controller;

use Smapone\SmaponeJobposting\Utility\FluidStandaloneUtility;
use Smapone\SmaponeJobposting\Domain\Model\Jobposting;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/***
 *
 * This file is part of the "smapOne Jobposting" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Felix Franz <email@felixfranz.de>
 *
 ***/

/**
 * JobsController
 */
class JobController extends ActionController
{
    /**
     * jobpostingRepository
     *
     * @var \Smapone\SmaponeJobposting\Domain\Repository\JobpostingRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $jobpostingRepository = null;

    /**
     * unitRepository
     *
     * @var \Smapone\SmaponeJobposting\Domain\Repository\UnitRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $unitRepository = null;


    /**
     * categoryRepository
     *
     * @var \Smapone\SmaponeJobposting\Domain\Repository\CategoryRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $categoryRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        if($this->settings['category']) {
            $jobs = $this->jobpostingRepository->findByCategory($this->settings['category']);
        }
        else {
            $jobs = $this->jobpostingRepository->findAll();
        }

        $this->view->assignMultiple([
            'jobs' => $jobs,
            'units'  => $this->unitRepository->findAll(),
            'categories'  => $this->categoryRepository->findAll(),
        ]);
    }

    /**
     * action slider
     *
     * @return void
     */
    public function sliderAction()
    {
        $this->view->assignMultiple([
            'jobs' => $this->jobpostingRepository->findForSlider()
        ]);
    }

    /**
     * action show
     *
     * @param Jobposting $job
     * @return void
     */
    public function showAction(Jobposting $job)
    {
        //page title
        $title = $job->getSeoTitle() ? $job->getSeoTitle() : $job->getJobTitle().' - '.$GLOBALS['TSFE']->page['title'];
        $description = $job->getSeoDescription() ? $job->getSeoDescription() : ($job->getIntrotext() ? $job->getIntrotext() : $job->getDescription());
        $GLOBALS['TSFE']->page['title'] = $title;
        //enhance meta tags
        $metaTags = FluidStandaloneUtility::renderView(['job' => $job, 'title' => $job->getSeoTitle(), 'description' => $description], 'Meta.html');
        $GLOBALS['TSFE']->additionalHeaderData[] = $metaTags;


        // json ld JobPosting
        $this->response->addAdditionalHeaderData($this->getJsonLd($job));

        $this->view->assignMultiple([
            'job' => $job,
            'jsonLd' => $this->getJsonLd($job)
        ]);
    }

    /**
     * json ld JobPosting
     *
     * @param Jobposting $job
     * @return string
     */
    public function  getJsonLd(Jobposting $job) : string
    {
        //set location address
        $address = [];
        if($job->getLocations() !== NULL && is_object($job->getLocations()) && isset($job->getLocations()[0])) {

            $location = $job->getLocations()[0];

            $address = [
                '@type' => 'PostalAddress',
                'streetAddress' => $location->getAddress(),
                'addressLocality' => $location->getCity(),
                'addressRegion'=> '',
                'postalCode' => $location->getZipCode(),
                'addressCountry' => $location->getCountry()
            ];
        }

        $jsonLd  = [
            '@context' => 'http://schema.org',
            '@type' => 'JobPosting',
            'title' => $job->getJobTitle(),
            'description' => $job->getIntrotext() ? $job->getIntrotext() : ($job->getDescription5() ? $job->getDescription5() : $job->getDescription()),
            'identifier' => [
                '@type' => 'PropertyValue',
                'name' => 'Sonotec',
                'value' => 'Sonotec-JopPosting-'.$job->getUid()
            ],

            # baseSalary
            # employmentType
            # validThrough

            'datePosted' => strftime('%Y-%m-%d',$job->getTstamp()),
            'validThrough' => '',
            'employmentType' => '',

            'hiringOrganization' => [
                '@type' => 'Organization',
                'name' => 'SONOTEC GmbH',
                'sameAs' => 'https://www.sonotec.de/',
                'logo' => 'https://www.sonotec.de/typo3conf/ext/sonotec_config/Resources/Public/Img/logo.svg'
            ],

            'jobLocation' => [
                '@type' => 'Place',
                'address' => $address
            ]
        ];

        # print_r($jsonLd);
        # echo json_encode($jsonLd);

        return '<script type="application/ld+json">'.json_encode($jsonLd).'</script>';
        /*
      "baseSalary": {
          "@type": "MonetaryAmount",
          "currency": "USD",
          "value": {
              "@type": "QuantitativeValue",
              "value": 40.00,
              "unitText": "HOUR"
          }
      }
  */
    }
}
