<?php
namespace Smapone\SmaponeJobposting\Controller;

/***
 *
 * This file is part of the "smapOne Jobposting" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Felix Franz <email@felixfranz.de>
 *
 ***/

/**
 * JobpostingController
 */
class JobpostingController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * jobpostingRepository
     *
     * @var \Smapone\SmaponeJobposting\Domain\Repository\JobpostingRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $jobpostingRepository = null;

    /**
     * action xmlFeed
     *
     * @return void
     */
    public function xmlFeedAction()
    {
        $jobpostings = $this->jobpostingRepository->findAll();
        $this->view->assign('jobpostings', $jobpostings);
    }
}
