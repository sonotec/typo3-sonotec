<?php
namespace Smapone\SmaponeJobposting\Utility;

use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class FluidStandaloneUtility
 *
 * @package RS\RsRecipe\Utility
 */
class FluidStandaloneUtility {

    /**
     * Creates a fluid instance with given template-file and controller-settings
     * @param string $file Path below Template-Root-Path (Resources/Private/Templates/$file)
     * @return \TYPO3\CMS\Fluid\View\StandaloneView Fluid-Template-Renderer
     */
    public static function getFluidRenderer($file, $path) {
        // create another instance of Fluid
        $objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');

        $renderer = $objectManager->get('TYPO3\\CMS\\Fluid\\View\\StandaloneView');
        $renderer->setFormat("html");
        $renderer->setTemplatePathAndFilename(GeneralUtility::getFileAbsFileName( $path. 'Templates/Job/'. $file));
        $renderer->setPartialRootPaths([GeneralUtility::getFileAbsFileName($path.'Partials/Job/')]);

        return $renderer;
    }

    /**
     * Creates a fluid instance with given template-file and controller-settings
     * @param array $assign
     * @param string $file
     * @return void
     */
    public static function renderView($assign, $file, $path='typo3conf/ext/smapone_jobposting/Resources/Private/') {
        $view = FluidStandaloneUtility::getFluidRenderer($file, $path);
        $view->assignMultiple($assign);
        return $view->render();
    }
}
