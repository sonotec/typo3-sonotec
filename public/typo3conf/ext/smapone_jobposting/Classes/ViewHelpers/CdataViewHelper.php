<?php
namespace Smapone\SmaponeJobposting\ViewHelpers;

/***
 *
 * This file is part of the "smapOne Jobposting" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Felix Franz <email@felixfranz.de>
 *
 ***/

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * CdataViewHelper
 *
 * wraps given in content with <![CDATA[...]]>
 */
class CdataViewHelper extends AbstractViewHelper
{
   /**
    * As this ViewHelper renders HTML, the output must not be escaped.
    *
    * @var bool
    */
    protected $escapeOutput = false;

    public function initializeArguments()
    {
        $this->registerArgument('content', 'string', 'Content to wrap in <![CDATA[...]]>', false, null);
    }

    public function render()
    {
        return $this->arguments['content'] ? '<![CDATA['.trim($this->arguments['content']).']]>' : '';
    }
}
