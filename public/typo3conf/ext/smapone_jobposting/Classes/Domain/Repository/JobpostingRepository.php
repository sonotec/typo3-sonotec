<?php
namespace Smapone\SmaponeJobposting\Domain\Repository;

/***
 *
 * This file is part of the "smapOne Jobposting" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Felix Franz <email@felixfranz.de>
 *
 ***/

/**
 * The repository for Jobpostings
 */
class JobpostingRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    protected $defaultOrderings = [
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    public function findForSlider() {
        $query = $this->createQuery();
        $query->matching(
          $query->equals('showInSlider',1)
        );
        return $query->execute();
    }
}
