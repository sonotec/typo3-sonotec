<?php
namespace Smapone\SmaponeJobposting\Domain\Model;

/***
 *
 * This file is part of the "smapOne Jobposting" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Felix Franz <email@felixfranz.de>
 *
 ***/

/**
 * Location
 */
class Category extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * l10n_parent
     *
     * @var int
     */
    protected $l10nParent = 0;

    /**
     * Returns the l10n_parent
     *
     * @return int $l10nParent
     */
    public function getL10nParent()
    {
        return $this->l10nParent;
    }

    /**
     * Sets the l10nParent
     *
     * @param int $l10nParent
     * @return void
     */
    public function setL10nParent($l10nParent)
    {
        $this->l10nParent = $l10nParent;
    }

    /**
     * title
     *
     * @var string
     */
    protected $title = '';


    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
}
