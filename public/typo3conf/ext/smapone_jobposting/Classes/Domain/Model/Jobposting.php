<?php
namespace Smapone\SmaponeJobposting\Domain\Model;

/***
 *
 * This file is part of the "smapOne Jobposting" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Felix Franz <email@felixfranz.de>
 *
 ***/

/**
 * Jobposting
 */
class Jobposting extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{


    /**
     * @var int The uid of the record. The uid is only unique in the context of the database table.
     */
    protected $uid;

    /**
     * jobTitle
     *
     * @var string
     */
    protected $jobTitle = '';

    /**
     * seoTitle
     *
     * @var string
     */
    protected $seoTitle = '';

    /**
     * seoDescription
     *
     * @var string
     */
    protected $seoDescription = '';

    /**
     * showInSlider
     *
     * @var bool
     */
    protected $showInSlider = '';

    /**
     * unit
     *
     * @var \Smapone\SmaponeJobposting\Domain\Model\Unit
     */
    protected $unit = '';

    /**
     * category
     *
     * @var \Smapone\SmaponeJobposting\Domain\Model\Category
     */
    protected $category = '';

    /**
     * description
     *
     * @var string
     */
    protected $description = '';

    /**
     * description2
     *
     * @var string
     */
    protected $description2 = '';

    /**
     * description3
     *
     * @var string
     */
    protected $description3 = '';

    /**
     * description4
     *
     * @var string
     */
    protected $description4 = '';

    /**
     * description5
     *
     * @var string
     */
    protected $description5 = '';

    /**
     * introtext
     *
     * @var string
     */
    protected $introtext = '';

    /**
     * url
     *
     * @var string
     */
    protected $url = '';

    /**
     * companyName
     *
     * @var string
     */
    protected $companyName = '';

    /**
     * address
     *
     * @var string
     */
    protected $address = '';

    /**
     * zipCode
     *
     * @var string
     */
    protected $zipCode = '';

    /**
     * city
     *
     * @var string
     */
    protected $city = '';

    /**
     * country
     *
     * @var string
     */
    protected $country = '';

    /**
     * bannerImageUrl
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $bannerImageUrl = null;

    /**
     * $metaImage
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $metaImage = null;

    /**
     * contactEmail
     *
     * @var string
     */
    protected $contactEmail = '';

    /**
     * posterUrl
     *
     * @var string
     */
    protected $posterUrl = '';

    /**
     * replySetting
     *
     * @var string
     */
    protected $replySetting = '';

    /**
     * jobType
     *
     * @var string
     */
    protected $jobType = '';

    /**
     * careerLevel
     *
     * @var string
     */
    protected $careerLevel = '';

    /**
     * salaryRangeStart
     *
     * @var int
     */
    protected $salaryRangeStart = 0;

    /**
     * salaryRangeEnd
     *
     * @var int
     */
    protected $salaryRangeEnd = 0;


    /**
     * tstamp
     *
     * @var string
     */
    protected $tstamp = '';

    /**
     * crdate
     *
     * @var string
     */
    protected $crdate = '';

    /**
     * locations
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Smapone\SmaponeJobposting\Domain\Model\Location>
     */
    protected $locations = null;


    /**
     * contact
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\SONOTEC\SonotecContact\Domain\Model\Contact>
     */
    protected $contact = null;


    /**
     * Adds a Location
     *
     * @param \Smapone\SmaponeJobposting\Domain\Model\Location $location
     * @return void
     */
    public function addLocation(\Smapone\SmaponeJobposting\Domain\Model\Location $location) {
           $this->location->attach($location);
    }

    /**
     * Removes a Location
     *
     * @param \CSmapone\SmaponeJobposting\Domain\Model\Location $locationToRemove The Category to be removed
     * @return void
     */
    public function removeLocation(\Smapone\SmaponeJobposting\Domain\Model\Location $locationToRemove) {
           $this->locations->detach($locationToRemove);
    }

    /**
     * Returns the Locations
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Smapone\SmaponeJobposting\Domain\Model\Location> $locations
     */
    public function getLocations() {
           return $this->locations;
    }

    /**
     * Sets the Locations
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Smapone\SmaponeJobposting\Domain\Model\Location> $locations
     * @return void
     */
    public function setLocations(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $locations) {
           $this->locations = $locations;
    }


    /**
     * images
     *
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     */
    protected $images = NULL;


    /**
     * __construct
     */
    public function __construct() {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects() {
        $this->images = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Adds a FileReference
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     */
    public function addImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image) {
        $this->images->attach($image);
    }

    /**
     * Removes a FileReference
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $imageToRemove The FileReference to be removed
     * @return void
     */
    public function removeImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $imageToRemove) {
        $this->images->detach($imageToRemove);
    }

    /**
     * Returns the images
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $images
     */
    public function getImages() {
        return $this->images;
    }

    /**
     * Sets the images
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $images
     * @return void
     */
    public function setImages(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $images) {
        $this->images = $images;
    }


    /**
     * Returns the jobTitle
     *
     * @return string jobTitle
     */
    public function getJobTitle()
    {
        return $this->jobTitle;
    }

    /**
     * Sets the jobTitle
     *
     * @param string $jobTitle
     * @return void
     */
    public function setJobTitle($jobTitle)
    {
        $this->jobTitle = $jobTitle;
    }


    /**
     * Returns the showInSlider
     *
     * @return bool showInSlider
     */
    public function getShowInSlider()
    {
        return $this->showInSlider;
    }

    /**
     * Sets the showInSlider
     *
     * @param bool $showInSlider
     * @return void
     */
    public function setShowInSlider($showInSlider)
    {
        $this->showInSlider = $showInSlider;
    }

    /**
     * Returns the unit
     *
     * @return \Smapone\SmaponeJobposting\Domain\Model\Unit unit
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * Sets the unit
     *
     * @param \Smapone\SmaponeJobposting\Domain\Model\Unit $unit
     * @return void
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
    }

    /**
     * Returns the category
     *
     * @return \Smapone\SmaponeJobposting\Domain\Model\Category category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Sets the category
     *
     * @param \Smapone\SmaponeJobposting\Domain\Model\Category $category
     * @return void
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }



    /**
     * Getter for uid.
     *
     * @return int the uid or NULL if none set yet.
     */
    public function getUid(): ?int
    {
        if ($this->uid !== null) {
            return (int)$this->uid;
        }
        return null;
    }

    /**
     * Sets the jobUid
     *
     * @param string $jobUid
     * @return void
     */
    public function setJobUid($jobUid)
    {
        $this->jobUid = $jobUid;
    }

    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     *
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }


    /**
     * Returns the description2
     *
     * @return string $description2
     */
    public function getDescription2()
    {
        return $this->description2;
    }

    /**
     * Sets the description2
     *
     * @param string $description2
     * @return void
     */
    public function setDescription2($description2)
    {
        $this->description2 = $description2;
    }

    /**
     * Returns the description3
     *
     * @return string $description3
     */
    public function getDescription3()
    {
        return $this->description3;
    }

    /**
     * Sets the description3
     *
     * @param string $description3
     * @return void
     */
    public function setDescription3($description3)
    {
        $this->description3 = $description3;
    }

    /**
     * Returns the description4
     *
     * @return string $description4
     */
    public function getDescription4()
    {
        return $this->description4;
    }

    /**
     * Sets the description4
     *
     * @param string $description4
     * @return void
     */
    public function setDescription4($description4)
    {
        $this->description4 = $description4;
    }


    /**
     * Returns the description5
     *
     * @return string $description5
     */
    public function getDescription5()
    {
        return $this->description5;
    }

    /**
     * Sets the description5
     *
     * @param string $description5
     * @return void
     */
    public function setDescription5($description5)
    {
        $this->description5 = $description5;
    }


    /**
     * Returns the introtext
     *
     * @return string $introtext
     */
    public function getIntrotext()
    {
        return $this->introtext;
    }

    /**
     * Sets the introtext
     *
     * @param string $introtext
     * @return void
     */
    public function setIntrotext($introtext)
    {
        $this->introtext = $introtext;
    }


    /**
     * Returns the url
     *
     * @return string $url
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Sets the url
     *
     * @param string $url
     * @return void
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * Returns the companyName
     *
     * @return string $companyName
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Sets the companyName
     *
     * @param string $companyName
     * @return void
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * Returns the address
     *
     * @return string $address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Sets the address
     *
     * @param string $address
     * @return void
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * Returns the zipCode
     *
     * @return string $zipCode
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Sets the zipCode
     *
     * @param string $zipCode
     * @return void
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;
    }

    /**
     * Returns the city
     *
     * @return string $city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Sets the city
     *
     * @param string $city
     * @return void
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * Returns the country
     *
     * @return string $country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Sets the country
     *
     * @param string $country
     * @return void
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * Returns the bannerImageUrl
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $bannerImageUrl
     */
    public function getBannerImageUrl()
    {
        return $this->bannerImageUrl;
    }

    /**
     * Sets the bannerImageUrl
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $bannerImageUrl
     * @return void
     */
    public function setBannerImageUrl(\TYPO3\CMS\Extbase\Domain\Model\FileReference $bannerImageUrl)
    {
        $this->bannerImageUrl = $bannerImageUrl;
    }

    /**
     * Returns the metaImage
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $metaImage
     */
    public function getMetaImage()
    {
        return $this->metaImage;
    }

    /**
     * Sets the metaImage
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $metaImage
     * @return void
     */
    public function setMetaImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $metaImage)
    {
        $this->metaImage = $metaImage;
    }

    /**
     * Returns the contactEmail
     *
     * @return string $contactEmail
     */
    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    /**
     * Sets the contactEmail
     *
     * @param string $contactEmail
     * @return void
     */
    public function setContactEmail($contactEmail)
    {
        $this->contactEmail = $contactEmail;
    }

    /**
     * Returns the posterUrl
     *
     * @return string $posterUrl
     */
    public function getPosterUrl()
    {
        return $this->posterUrl;
    }

    /**
     * Sets the posterUrl
     *
     * @param string $posterUrl
     * @return void
     */
    public function setPosterUrl($posterUrl)
    {
        $this->posterUrl = $posterUrl;
    }

    /**
     * Returns the replySetting
     *
     * @return string $replySetting
     */
    public function getReplySetting()
    {
        return $this->replySetting;
    }

    /**
     * Sets the replySetting
     *
     * @param string $replySetting
     * @return void
     */
    public function setReplySetting($replySetting)
    {
        $this->replySetting = $replySetting;
    }

    /**
     * Returns the jobType
     *
     * @return string $jobType
     */
    public function getJobType()
    {
        return $this->jobType;
    }

    /**
     * Sets the jobType
     *
     * @param string $jobType
     * @return void
     */
    public function setJobType($jobType)
    {
        $this->jobType = $jobType;
    }

    /**
     * Returns the careerLevel
     *
     * @return string $careerLevel
     */
    public function getCareerLevel()
    {
        return $this->careerLevel;
    }

    /**
     * Sets the careerLevel
     *
     * @param string $careerLevel
     * @return void
     */
    public function setCareerLevel($careerLevel)
    {
        $this->careerLevel = $careerLevel;
    }

    /**
     * Returns the salaryRangeStart
     *
     * @return int $salaryRangeStart
     */
    public function getSalaryRangeStart()
    {
        return $this->salaryRangeStart;
    }

    /**
     * Sets the salaryRangeStart
     *
     * @param int $salaryRangeStart
     * @return void
     */
    public function setSalaryRangeStart($salaryRangeStart)
    {
        $this->salaryRangeStart = $salaryRangeStart;
    }

    /**
     * Returns the salaryRangeEnd
     *
     * @return int $salaryRangeEnd
     */
    public function getSalaryRangeEnd()
    {
        return $this->salaryRangeEnd;
    }

    /**
     * Sets the salaryRangeEnd
     *
     * @param int $salaryRangeEnd
     * @return void
     */
    public function setSalaryRangeEnd($salaryRangeEnd)
    {
        $this->salaryRangeEnd = $salaryRangeEnd;
    }


    /**
     * Adds a contact
     *
     * @param \SONOTEC\SonotecContact\Domain\Model\Contact $contact
     * @return void
     */
    public function addContact(\SONOTEC\SonotecContact\Domain\Model\Contact $contact) {
        $this->contact->attach($contact);
    }

    /**
     * Removes a contact
     *
     * @param \SONOTEC\SonotecContact\Domain\Model\Contact $contactToRemove The contact to be removed
     * @return void
     */
    public function removeContact(\SONOTEC\SonotecContact\Domain\Model\Contact $contactToRemove) {
        $this->contact->detach($contactToRemove);
    }

    /**
     * Returns the contact
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\SONOTEC\SonotecContact\Domain\Model\Contact> $contact
     */
    public function getContact() {
        return $this->contact;
    }

    /**
     * Sets the contact
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\SONOTEC\SonotecContact\Domain\Model\Contact> $contact
     * @return void
     */
    public function setContact(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $contact) {
        $this->contact = $contact;
    }

    /**
     * Returns the tstamp
     *
     * @return string tstamp
     */
    public function getTstamp()
    {
        return $this->tstamp;
    }

    /**
     * Sets the tstamp
     *
     * @param string $tstamp
     * @return void
     */
    public function setTstamp($tstamp)
    {
        $this->tstamp = $tstamp;
    }

    /**
     * Returns the crdate
     *
     * @return string crdate
     */
    public function getCrdate()
    {
        return $this->crdate;
    }

    /**
     * Sets the crdate
     *
     * @param string $crdate
     * @return void
     */
    public function setCrdate($crdate)
    {
        $this->crdate = $crdate;
    }





    /**
     * Returns the seoTitle
     *
     * @return string seoTitle
     */
    public function getSeoTitle()
    {
        return $this->seoTitle;
    }

    /**
     * Sets the seoTitle
     *
     * @param string $seoTitle
     * @return void
     */
    public function setSeoTitle($seoTitle)
    {
        $this->seoTitle = $seoTitle;
    }

    /**
     * Returns the seoDescription
     *
     * @return string seoDescription
     */
    public function getSeoDescription()
    {
        return $this->seoDescription;
    }

    /**
     * Sets the seoDescription
     *
     * @param string $seoDescription
     * @return void
     */
    public function setSeoDescription($seoDescription)
    {
        $this->seoDescription = $seoDescription;
    }
}
