<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_smaponejobposting_domain_model_jobposting', 'EXT:smapone_jobposting/Resources/Private/Language/locallang_csh_tx_smaponejobposting_domain_model_jobposting.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_smaponejobposting_domain_model_jobposting');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_smaponejobposting_domain_model_location', 'EXT:smapone_jobposting/Resources/Private/Language/locallang_csh_tx_smaponejobposting_domain_model_location.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_smaponejobposting_domain_model_location');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_smaponejobposting_domain_model_category', 'EXT:smapone_jobposting/Resources/Private/Language/locallang_csh_tx_smaponejobposting_domain_model_category.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_smaponejobposting_domain_model_category');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_smaponejobposting_domain_model_unit', 'EXT:smapone_jobposting/Resources/Private/Language/locallang_csh_tx_smaponejobposting_domain_model_unit.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_smaponejobposting_domain_model_unit');

    }
);
