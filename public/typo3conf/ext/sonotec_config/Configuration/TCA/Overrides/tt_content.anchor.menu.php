<?php

/************************* Anchor menu *************************/
// Adds the content element to the "Type" dropdown
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
    array(
        'LLL:EXT:sonotec_config/Resources/Private/Language/locallang_backend.xlf:tt_content.sonotecconfig_anchormenu',
        'sonotecconfig_anchormenu',
        'content-menu-pages'
    ),
    'CType',
    'sonotec_config'
);

$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['sonotecconfig_anchormenu'] = 'content-menu-pages';

// Configure the default backend fields for the content element
$GLOBALS['TCA']['tt_content']['types']['sonotecconfig_anchormenu'] = array(
    'showitem' => '
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.headers;headers,
        pi_flexform,records,
      --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
         --palette--;;language,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
         --palette--;;hidden,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
         categories,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
         rowDescription,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,
   ',
    'columnsOverrides' => [
        'records' => [
            'config' => [
                'allowed' => 'tt_content'
            ]
        ]
    ]
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue('sonotecconfig_anchormenu', 'FILE:EXT:sonotec_config/Configuration/FlexForms/AnchorMenu.xml');
$GLOBALS['TCA']['tt_content']['columns']['pi_flexform']['config']['ds'][',sonotecconfig_anchormenu'] = 'FILE:EXT:sonotec_config/Configuration/FlexForms/AnchorMenu.xml';
