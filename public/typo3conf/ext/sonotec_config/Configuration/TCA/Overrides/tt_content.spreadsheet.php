<?php

$GLOBALS['TCA']['tt_content']['types']['spreadsheets_table']['columnsOverrides']['bodytext']['config']['sheetsOnly'] = false;
$GLOBALS['TCA']['tt_content']['types']['spreadsheets_table']['columnsOverrides']['tx_spreadsheets_ignore_styles']['config']['default'] = 1;

//add field table_header_position
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'tt_content',
    'table_header_position',
    'spreadsheets_table',
    'before:bodytext'
);

$GLOBALS['TCA']['tt_content']['types']['spreadsheets_table']['columnsOverrides']['table_header_position']['config']['items'][3] = ['Oben & Links',3];
