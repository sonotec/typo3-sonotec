<?php

$GLOBALS['TCA']['tt_content']['columns']['bodytext']['l10n_mode'] = '';
$GLOBALS['TCA']['tt_content']['columns']['header']['l10n_mode'] = '';

$fields = [
    'sonotec_icon' => [
        'label' => 'Icon',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => $GLOBALS['TCA']['tt_content']['sonotec_custom_icons'],
            'fieldWizard' => [
                'selectIcons' => [
                    'disabled' => false,
                ],
            ],
            'default' => ''
        ]
    ]
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $fields);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'tt_content',
    'sonotec_icon'
);
