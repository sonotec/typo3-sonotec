<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

call_user_func(function () {

    // pageTS config files
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
        'sonotec_config',
        'Configuration/PageTS/page.typoscript',
        'Default TS Page Configuration'
    );

});


// icons
$GLOBALS['TCA']['tt_content']['sonotec_custom_icons'] = [
    ['', '', ''],
    ["arrow-down-right", "arrow-down-right", "sonotec-arrow-down-right"],
    ["arrow-left", "arrow-left", "sonotec-arrow-left"],
    ["arrow-right", "arrow-right", "sonotec-arrow-right"],
    ["arrow-up-right", "arrow-up-right", "sonotec-arrow-up-right"],
    ["check-circle", "check-circle", "sonotec-check-circle"],
    ["check", "check", "sonotec-check"],
    ["phone", "phone", "sonotec-phone"],
    ["mail", "mail", "sonotec-mail"],
    ["search", "search", "sonotec-search"],
    ["bulb", "bulb", "sonotec-bulb"],
    ["bu-1", "bu-1", "sonotec-bu-1"],
    ["bu-2", "bu-2", "sonotec-bu-2"],
    ["bu-3", "bu-3", "sonotec-bu-3"],
    ["bu-4", "bu-4", "sonotec-bu-4"],
    ["pruefkoepfe", "pruefkopfe", "sonotec-pruefkoepfe"],
    ["expertise", "expertise", "sonotec-expertise"],
    ["worldwide", "worldwide", "sonotec-worldwide"],
    ["application", "application", "sonotec-application"],
    ["biotech", "icon-biotech-sonotec", "sonotec-biotech-sonotec"],
    ["medtech", "icon-medtech-sonotec", "sonotec-medtech-sonotec"],
    ["semicon", "icon-semicon-sonotec", "sonotec-semicon-sonotec"],
];



$fields = [
    'sonotec_showform' => [
        'label' => 'Formular am Seitenende anzeigen',
        'config' => [
            'type' => 'check'
        ],
    ],
    'sonotec_contact' => [
        'label' => 'Kontakt',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectMultipleSideBySide',
            'allowed' => 'tx_sonoteccontact_domain_model_contact',
            'foreign_table' => 'tx_sonoteccontact_domain_model_contact',
            'foreign_table_where' => 'AND ORDER BY tx_sonoteccontact_domain_model_contact.name ASC',
            'maxitems' => 1,
            'minitems' => 0
        ]
    ],
    'sonotec_icon' => [
        'label' => 'Icon',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => $GLOBALS['TCA']['tt_content']['sonotec_custom_icons'],
            'fieldWizard' => [
                'selectIcons' => [
                    'disabled' => false,
                ],
            ],
            'default' => ''
        ]
    ],
    'sonotec_class' => [
        'label' => 'Addtional class',
        'config' => [
            'type' => 'input'
        ],
    ],
    'sonotec_shortcut' => [
        'label' => 'Shortcut auf als Anker',
        'config' => [
            'allowed' => 'tt_content',
            'type' => 'group',
            'internal_type' => 'db',
            'maxitems' => 1,
            'minitems' => 0,
            'size' => 1
        ],
    ],
    'sonotec_form_variant' => [
        'label' => 'Formularvariante',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                ['Standard Kontaktformular', 'contact.form.yaml'],
                ['Karriere Studium Kontaktformular', 'contact_studium.form.yaml'],
                ['Karriere Azubis Kontaktformular', 'contact_azubi.form.yaml'],
                ['Kontaktformular ZfP', 'contact.zfp.form.yaml']
            ]
        ]
    ],
    'sonotec_show_popup' => [
        'label' => 'Karriere Popup anzeigen',
        'config' => [
            'type' => 'check'
        ],
    ]
];

$GLOBALS['TCA']['pages']['palettes']['sonotec_contact_palette'] = [
    'showitem' => 'sonotec_showform,sonotec_form_variant,--linebreak--,sonotec_contact,'
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $fields);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'pages',
    '--palette--;Formular & Kontakt;sonotec_contact_palette,sonotec_icon,sonotec_show_popup,',
    '1',
    'after:nav_title'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'pages',
    'sonotec_shortcut',
    '4',
    'after:shortcut'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'pages',
    'sonotec_class',
    '',
    'after:backend_layout_next_level'
);

