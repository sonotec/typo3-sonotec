<?php


/************************* Iconbox *************************/
// Adds the content element to the "Type" dropdown
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
    array(
        'LLL:EXT:sonotec_config/Resources/Private/Language/locallang_backend.xlf:tt_content.sonotecconfig_iconbox',
        'sonotecconfig_iconbox',
        'content-carousel-html'
    ),
    'CType',
    'sonotec_config'
);

$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['sonotecconfig_iconbox'] = 'content-carousel-html';
// Configure the default backend fields for the content element
$GLOBALS['TCA']['tt_content']['types']['sonotecconfig_iconbox'] = array(
    'showitem' => '
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.headers;headers,
        sonotec_icon,image,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
         --palette--;;language,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
         --palette--;;hidden,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
         categories,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
         rowDescription,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,
   ',
    'columnsOverrides' => [
        'image' => [
            'label' => 'Bild (nur für Startseite!)',
            'config' => [
                'maxitems' => 1
            ]
        ]
    ]
);
