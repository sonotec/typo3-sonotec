<?php
return [
    'frontend' => [
        'redirect-eu-domain/' => [
            'target' => \TYPO3\SonotecConfig\Middleware\Redirect::class,
            'before' => [
                'typo3/cms-frontend/page-resolver',
            ],
        ],

        'sonotec-session-cookies/' => [
            'target' => \TYPO3\SonotecConfig\Middleware\Session::class,
            'after' => [
                'typo3/cms-frontend/tsfe',
            ],
        ]
    ]
];
