#
# Table structure for table 'pages'
#
CREATE TABLE pages (
   sonotec_showform tinyint(4) unsigned DEFAULT '0' NOT NULL,
   sonotec_contact varchar(255) DEFAULT '' NOT NULL,
   sonotec_icon varchar(255) DEFAULT '' NOT NULL,
   sonotec_class varchar(255) DEFAULT '' NOT NULL,
   sonotec_shortcut varchar(255) DEFAULT '' NOT NULL,
   sonotec_form_variant varchar(255) DEFAULT '' NOT NULL,
   sonotec_show_popup tinyint(4) unsigned DEFAULT '0' NOT NULL,
);

#
# Table structure for table 'tt_content'
#
CREATE TABLE tt_content (
   sonotec_icon varchar(255) DEFAULT '' NOT NULL,
);
