$( document ).ready(function() {
  //document.getElementById("bottomList").innerHTML = "";
  $('.calculator-disclaimer').click(function (){
    $('#disclaimer').slideUp();
  });
});

/*currency, might help with localization*/
var currency = "€";

/*variation for the lower and upper bound of the end calculation*/
var variation = .20;

/*whether the user changed the "druckluftkennzahl" manually*/
var changedManually = false;

/*all the vars which were not input by the user*/
var volllastLeistung, leerlaufLeistung, liefermenge, druckluftKennzahl;
var kosten, gesamtkosten;

var currentUnit = "metrisch";

var hpToKw = 1.341;
var ft3ToM3 = 35.315;
var lminToft3h = 2.119;

function switchUnit(element){
  currentUnit = element.getAttribute("value");

  if(currentUnit === "imperial"){
    $('.leakCalculator').addClass('imperial');
    currency = "$";

    var elements = document.getElementsByClassName("m3");
    for (var i = 0; i < elements.length; i++) {
      elements[i].innerHTML = "ft³";
    }

    elements = document.getElementsByClassName("kW");
    for (var i = 0; i < elements.length; i++) {
      elements[i].innerHTML = "hp";
    }

    elements = document.getElementsByClassName("m3min");
    for (var i = 0; i < elements.length; i++) {
      elements[i].innerHTML = "cfm";
    }

    elements = document.getElementsByClassName("m3h");
    for (var i = 0; i < elements.length; i++) {
      elements[i].innerHTML = "cfh";
    }

    elements = document.getElementsByClassName("lmin");
    for (var i = 0; i < elements.length; i++) {
      elements[i].innerHTML = "cfm";
    }

    document.getElementById("bar").innerHTML = "120psi: 0,0035";
  }else if(currentUnit === "metrisch"){
    $('.leakCalculator').removeClass('imperial');
    currency = "€";

    var elements = document.getElementsByClassName("m3");
    for (var i = 0; i < elements.length; i++) {
      elements[i].innerHTML = "m³";
    }

    elements = document.getElementsByClassName("kW");
    for (var i = 0; i < elements.length; i++) {
      elements[i].innerHTML = "kW";
    }

    elements = document.getElementsByClassName("m3min");
    for (var i = 0; i < elements.length; i++) {
      elements[i].innerHTML = "m³/min";
    }

    elements = document.getElementsByClassName("m3h");
    for (var i = 0; i < elements.length; i++) {
      elements[i].innerHTML = "m³/h";
    }

    elements = document.getElementsByClassName("lmin");
    for (var i = 0; i < elements.length; i++) {
      elements[i].innerHTML = "l/min";
    }

    document.getElementById("bar").innerHTML = "8bar: 0,120";
  }

  if(/*offen*/$('.kompressordaten').is(':visible'))
    calculateKennzahl();
  else
    calculateCosts();
}

/**
 *
 * @param {input} element das Element, bei dem die Dezimalstellen begrenzt werden soll
 * @param {int} n die Anzahl der Nachkommastellen
 * @returns {Boolean}
 */
function setNumberDecimal(element, n){
  var value = element.value.replace(/,/g, '.');
  if(value.length === 0)
    return;

  /*alle Nichtzahlen entfernen*/
  var changed = false;
  for (var i = 0; i < value.length; i++) {
    var code = value.charCodeAt(i);
    if((code < 48 || code > 57) && code !== 46){
      value = value.replace(String.fromCharCode(code), "");
      i--;
      changed = true;
    }
  }
  if(changed)
    element.value = value;

  var index = value.lastIndexOf(".");
  if(index === -1)
    return;

  if(index !== value.indexOf(".")){
    while(index !== value.indexOf(".")){
      var tmp = element.value.substring(0, index);
      element.value = tmp;

      index = element.value.replace(/,/g, '.').lastIndexOf(".");
    }
    return;
  }

  element.value = value;

  if(value.length <= index+n+1){
    return;
  }

  var tmp = element.value.substring(0, index+n+1);
  element.value = tmp;
  return;
}

function setValueOfClass(className, value){
  var e = document.getElementsByClassName(className);
  for(var i = 0; i < e.length; i++){
    if(e[i].tagName.toLowerCase() == "input")
      e[i].value = value;
    else
      e[i].innerHTML = value;
  }
}

/**
 * @description toggles the compressor menu
 * @returns {undefined}
 */
function toggleCompressor(){
  $(".kompressordaten").slideToggle("slow");
}

/**
 * @description calculates the "Kennzahl"
 * @returns {undefined}
 */
function calculateKennzahl(){
  var nennleistung = document.getElementById("Nennleistung").value.replace(/,/g, '.');
  var nennliefermenge = document.getElementById("Nennliefermenge").value.replace(/,/g, '.');
  var auslastung = document.getElementById("Auslastung").value.replace(/,/g, '.');

  setValueOfClass("Nennleistung", nennleistung);
  setValueOfClass("Nennliefermenge", nennliefermenge);
  setValueOfClass("Auslastung", auslastung);

  if(isNaN(parseFloat(auslastung))){
    document.getElementById("Auslastung").value = "";
    auslastung = 1;
  }else if(auslastung > 100){
    document.getElementById("Auslastung").value = 100;
    auslastung = 100;
  }else if(auslastung < 1 && !isNaN(parseFloat(auslastung))){
    document.getElementById("Auslastung").value = 1;
    auslastung = 1;
  }

  if(!nennleistung || !nennliefermenge || !auslastung ||
    isNaN(parseFloat(nennleistung)) || isNaN(parseFloat(nennliefermenge)) || isNaN(parseFloat(auslastung)) ||
    nennleistung == 0 || nennliefermenge == 0 || auslastung == 0){
    volllastLeistung = 0;
    leerlaufLeistung = 0;
    liefermenge = 0;
    druckluftKennzahl = 0;

    setValueOfClass("Volllastleistung", "");
    setValueOfClass("Leerlaufleistung", "");
    setValueOfClass("Liefermenge", "");
    setValueOfClass("Druckluftkennzahl", "");

    calculateCosts();

    return;
  }

  volllastLeistung = (nennleistung * auslastung) / 100;
  leerlaufLeistung = 0.3 * nennleistung * ((100 - auslastung) / 100);
  liefermenge = nennliefermenge * 60 * auslastung / 100;
  druckluftKennzahl = (volllastLeistung + leerlaufLeistung) / liefermenge;

  if(currentUnit === "imperial"){
    /*wenn kwh beibehalten wird; wenn es hph werden, dann konv nicht nötig TODO*/
    druckluftKennzahl /= hpToKw;
  }

  setValueOfClass("Volllastleistung", Math.round(volllastLeistung));
  setValueOfClass("Leerlaufleistung", Math.round(leerlaufLeistung));
  setValueOfClass("Liefermenge", Math.round(liefermenge));
  if(currentUnit === "imperial")
    setValueOfClass("Druckluftkennzahl", druckluftKennzahl.toFixed(5));
  else
    setValueOfClass("Druckluftkennzahl", druckluftKennzahl.toFixed(3));

  changedManually = false;
  calculateCosts();
}

/**
 * @description calculates all costs
 * @returns {undefined}
 */
function calculateCosts(){
  var stromPreis = document.getElementById("Strompreis").value.replace(/,/g, '.');
  var verhaeltnis = document.getElementById("kostenProzent").value.replace(/,/g, '.');
  variation = document.getElementById("abweichungProzent").value.replace(/,/g, '.') /100;

  setValueOfClass("Strompreis", stromPreis);

  /*wenn druckluftkennzahl manuell geändert wurde wird der eingegebene und nicht der berechnete Wert genommen*/
  if(changedManually){
    druckluftKennzahl = document.getElementById("Druckluftkennzahl").value.replace(/,/g, '.');
    setValueOfClass("Druckluftkennzahl", druckluftKennzahl);
  }

  /*verhältnis und variation validieren*/
  if(isNaN(parseFloat(verhaeltnis))){
    document.getElementById("kostenProzent").value = "";
    verhaeltnis = 1;
  }else if(verhaeltnis > 100){
    document.getElementById("kostenProzent").value = 100;
    verhaeltnis = 100;
  }else if(verhaeltnis < 1 && !isNaN(parseFloat(verhaeltnis))){
    verhaeltnis = 1;
    document.getElementById("kostenProzent").value = 1;
  }

  if(isNaN(parseFloat(variation))){
    document.getElementById("abweichungProzent").value = "";
    variation = 0;
  }else if(variation*100 > 100){
    document.getElementById("abweichungProzent").value = 100;
    variation = 1;
  }else if(variation < 0 && !isNaN(parseFloat(variation))){
    variation = 0;
    document.getElementById("abweichungProzent").value = 0;
  }

  /*span mit Ergebnis aktualisieren*/
  setValueOfClass("kostenProzent", verhaeltnis);
  setValueOfClass("abw", (variation*100).toFixed(0));

  if(!stromPreis || !druckluftKennzahl || !verhaeltnis ||
    isNaN(parseFloat(stromPreis)) || isNaN(parseFloat(druckluftKennzahl)) || isNaN(parseFloat(verhaeltnis))){
    kosten = 0;
    gesamtkosten = 0;

    setValueOfClass("Energiekosten", "");
    setValueOfClass("Gesamtkosten", "");

    calculateLoss();
    return;
  }

  kosten = druckluftKennzahl * stromPreis;
  gesamtkosten = kosten + (kosten * 100 / verhaeltnis) * ((100 - verhaeltnis) / 100);

  if(currentUnit === "imperial"){
    setValueOfClass("Energiekosten", kosten.toFixed(5));
    setValueOfClass("Gesamtkosten", gesamtkosten.toFixed(5));
  }else{
    setValueOfClass("Energiekosten", kosten.toFixed(3));
    setValueOfClass("Gesamtkosten", gesamtkosten.toFixed(3));
  }

  calculateLoss();
}

/**
 * @description calculates aproximation of the loss caused by leakages
 * @returns {undefined}
 */
function calculateLoss(){
  var arbeitsstunden = document.getElementById("Arbeitsstunden").value.replace(/,/g, '.');
  var leckageverlust = document.getElementById("Gesamtleckageverlust").value.replace(/,/g, '.');

  setValueOfClass("Arbeitsstunden", arbeitsstunden);
  setValueOfClass("Gesamtleckageverlust", leckageverlust);

  if(!arbeitsstunden || !leckageverlust || !gesamtkosten ||
    isNaN(parseFloat(arbeitsstunden)) || isNaN(parseFloat(leckageverlust)) || isNaN(parseFloat(gesamtkosten))){
    gesamtverlustMitte = 0;
    gesamtverlustLinks = 0;
    gesamtverlustRechts = 0;

    setValueOfClass("costLeft", "");
    setValueOfClass("costMedian", "");
    setValueOfClass("costRight", "");

    $(".endcost").hide();
    return;
  }

  if(currentUnit === "metrisch"){
    leckageverlust = leckageverlust * 60 / 1000;
  } else if(currentUnit === "imperial"){

    /*leckageverlust = leckageverlust * lminToft3h;*/ /*wenn Eingabe l/min*/
    leckageverlust = leckageverlust * 60; /*bei ft3/min*/
  }

  gesamtverlustMitte = (gesamtkosten * arbeitsstunden * leckageverlust);
  gesamtverlustLinks = gesamtverlustMitte * (1 - variation);
  gesamtverlustRechts = gesamtverlustMitte * (1 + variation);

  gesamtverlustMitte = Math.round(gesamtverlustMitte / 10) * 10;
  gesamtverlustLinks = Math.round(gesamtverlustLinks / 10) * 10;
  gesamtverlustRechts = Math.round(gesamtverlustRechts / 10) * 10;

  setValueOfClass("costLeft", gesamtverlustLinks.toLocaleString() + currency);
  setValueOfClass("costMedian", gesamtverlustMitte.toLocaleString() + currency);
  setValueOfClass("costRight", gesamtverlustRechts.toLocaleString() + currency);

  $(".endcost").show();
}
