function getQueryParams(qs) {
  qs = qs.split('+').join(' ');

  var params = {},
    tokens,
    re = /[?&]?([^=]+)=([^&]*)/g;

  while (tokens = re.exec(qs)) {
    params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
  }

  return params;
}

$(document).ready(function(){
  //mobile menu
  var menu = new MmenuLight(document.querySelector( '#main-menu-mobile'),'all'),
    navigator = menu.navigation({title: $( '#main-menu-mobile').data('title')}),
    drawer = menu.offcanvas();

  $('#navbar-toggler').click(function (){
    drawer.open();
  });

  //sticky menu bar
  $(window).scroll(function(){
    var sticky = $('.meta-wrap'),
      scroll = $(window).scrollTop();
    if (scroll > 200) {
      sticky.slideUp(100);
    }
    if (scroll < 160) {
      sticky.slideDown(100);
    }
  });

  //lazy load
  var lazyLazy = new LazyLoad({
    elements_selector: ".img-lazy"
  });

  //contact referer
  $('.mask-contact-link').click(function(e){
    e.preventDefault();
    var params = getQueryParams(document.location.search);
    if(Object.keys(params).length == 0) {
      window.location.href = $(this).attr('href') + '?ref='+window.location.href;
    }
  });

  /*accordion - tabs*/
  $('.accordion-tabs.js-scroll').each(function(){
    var $accHeader = $(this);
    $accHeader.find('.nav-link').click(function(){

      $('html, body').animate({
        scrollTop: $('#' + $accHeader.attr('id')).offset().top
      }, 300);
    });
  });

  /*load videos*/
  $('.yt-preview-container').one('click',function(){
    if($(this).data('video')) {
      var iframe = '<iframe src="https://www.youtube-nocookie.com/embed/'+$(this).data('video')+'?autohide=1&controls=2&autoplay=1&enablejsapi=1;showinfo=0" allowfullscreen="" width="480" height="270" class="embed-responsive-item" title="Twelve Foot Ninja - ONE HAND KILLING (OFFICIAL VIDEO)" allow="fullscreen"></iframe>';
      $(this).html(iframe);
    }
  });


  $('.ce-slider-carousel').slick({
    autoplay: true,
    autoplaySpeed: 3000,
    dots: true,
    arrows: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    centerMode: false,
    variableWidth: true,
    prevArrow: '<button type="button" class="slick-prev"><span class="icon-arrow-left"></span></button>',
    nextArrow: '<button type="button" class="slick-next"><span class="icon-arrow-right"></span></button>'
  });



  $('.job-slider').slick({
    autoplay: true,
    autoplaySpeed: 3000,
    dots: true,
    arrows: true,
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    centerMode: false,
    variableWidth: false,
    adaptiveHeight: false,
    prevArrow: '<button type="button" class="slick-prev"><span class="icon-arrow-left"></span></button>',
    nextArrow: '<button type="button" class="slick-next"><span class="icon-arrow-right"></span></button>',
    responsive: [
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 1040,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 1999,
        settings: {
          slidesToShow: 4,
        }
      }
    ]
  });

  $('.job-slider').each(function () {
    $(this).find('.job-slider-item--outer').css('height', $(this).find('.slick-track').height() + 'px');
  });

  $(window).resize(function(){
    $('.job-slider').each(function () {
      $(this).find('.job-slider-item--outer').css('height', $(this).find('.slick-track').height() + 'px');
    });
  });


  $('.frontpage-slider-carousel').slick({
    autoplay: true,
    autoplaySpeed: 5000,
    dots: true,
    arrows: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    centerMode: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: false,
    variableWidth: true,
    prevArrow: '<button type="button" class="slick-prev"><span class="icon-arrow-left"></span></button>',
    nextArrow: '<button type="button" class="slick-next"><span class="icon-arrow-right"></span></button>',
    responsive: [
      {
        breakpoint: 991,
        settings: {
          adaptiveHeight: true,
          variableWidth: true,
        }
      }
    ]
  });

  //form application
  //<p>Bitte haben Sie einen Augenblick Geduld - Ihre Daten werden Ã¼bertragen.</p>
  $('#application-13885,#application-10765').submit(function(){
    $('.form-overlay').html('<div class="form-overlay--inner"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div><p>Bitte haben Sie einen Augenblick Geduld - Ihre Daten werden übertragen.</p></div>').show();
  });

  //job filter
  jobFilter();
  $('.job-filter-select').change(function (){
    jobFilter();
  });

  //career popup
  if(Cookies.get('CareerPopup') === undefined) {
    Cookies.set('CareerPopup', 'false');
  }
  if(Cookies.get('CareerPopup') === 'false') {
    setTimeout(() => {
      $('#careerModal').modal('show');
    }, 20000);

    setTimeout(() => {
      $('#careerModal').modal('show');
    }, 60000);
  }

  $('#careerModalClose').click(function (){
    let expireTime =  new Date(new Date().getTime() + 5 * 60 * 1000);
    Cookies.set('CareerPopup', 'true', {expires: expireTime});
  });
});

function jobFilter() {
  var selection='';
  $('.job-filter-select').each(function(){
    selection = selection + $(this).val();
  });

  $('.job-item').hide();
  $('.job-item'+selection).show();

  $('.job-item').removeClass('odd');
  var i = 1;
  $('.job-item').each(function(){
    if($(this).is(":visible")) {
      if (i%2 == 1) {
        $(this).addClass('odd');
      }
      i++;
    }
  });
  $('.number-of-jobs-count').html(i - 1);
}
