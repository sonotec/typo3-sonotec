<?php

use TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider;
use TYPO3\CMS\Core\Utility\GeneralUtility;

defined('TYPO3_MODE') or die();

// configuration for CK Editor
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['rte'] = 'EXT:sonotec_config/Configuration/RTE/rte.yaml';

// add save & close button
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['Backend\Template\Components\ButtonBar']['getButtonsHook'][] = 'TYPO3\SonotecConfig\Hooks\SaveCloseHook->addSaveCloseButton';

//form prefill hook
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/form']['initializeFormElement'][1612436959] = \TYPO3\SonotecConfig\Hooks\ReceiverPrefill::class;

// icons
$iconRegistry = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);


$iconRegistry->registerIcon(
    'sonotec-arrow-down-right',
    BitmapIconProvider::class,
    ['source' => 'EXT:sonotec_config/Resources/Public/Img/Icons/arrow-down-right.svg']
);
$iconRegistry->registerIcon(
    'sonotec-arrow-left',
    BitmapIconProvider::class,
    ['source' => 'EXT:sonotec_config/Resources/Public/Img/Icons/arrow-left.svg']
);
$iconRegistry->registerIcon(
    'sonotec-arrow-right',
    BitmapIconProvider::class,
    ['source' => 'EXT:sonotec_config/Resources/Public/Img/Icons/arrow-right.svg']
);
$iconRegistry->registerIcon(
    'sonotec-arrow-up-right',
    BitmapIconProvider::class,
    ['source' => 'EXT:sonotec_config/Resources/Public/Img/Icons/arrow-up-right.svg']
);
$iconRegistry->registerIcon(
    'sonotec-check-circle',
    BitmapIconProvider::class,
    ['source' => 'EXT:sonotec_config/Resources/Public/Img/Icons/check-circle.svg']
);
$iconRegistry->registerIcon(
    'sonotec-check',
    BitmapIconProvider::class,
    ['source' => 'EXT:sonotec_config/Resources/Public/Img/Icons/check.svg']
);
$iconRegistry->registerIcon(
    'sonotec-mail',
    BitmapIconProvider::class,
    ['source' => 'EXT:sonotec_config/Resources/Public/Img/Icons/mail.svg']
);
$iconRegistry->registerIcon(
    'sonotec-phone',
    BitmapIconProvider::class,
    ['source' => 'EXT:sonotec_config/Resources/Public/Img/Icons/phone.svg']
);
$iconRegistry->registerIcon(
    'sonotec-search',
    BitmapIconProvider::class,
    ['source' => 'EXT:sonotec_config/Resources/Public/Img/Icons/search.svg']
);
$iconRegistry->registerIcon(
    'sonotec-bulb',
    BitmapIconProvider::class,
    ['source' => 'EXT:sonotec_config/Resources/Public/Img/Icons/bulb.svg']
);

$iconRegistry->registerIcon(
    'sonotec-bu-1',
    BitmapIconProvider::class,
    ['source' => 'EXT:sonotec_config/Resources/Public/Img/Icons/bu-1.svg']
);
$iconRegistry->registerIcon(
    'sonotec-bu-2',
    BitmapIconProvider::class,
    ['source' => 'EXT:sonotec_config/Resources/Public/Img/Icons/bu-2.svg']
);
$iconRegistry->registerIcon(
    'sonotec-bu-3',
    BitmapIconProvider::class,
    ['source' => 'EXT:sonotec_config/Resources/Public/Img/Icons/bu-3.svg']
);
$iconRegistry->registerIcon(
    'sonotec-bu-4',
    BitmapIconProvider::class,
    ['source' => 'EXT:sonotec_config/Resources/Public/Img/Icons/bu-4.svg']
);
$iconRegistry->registerIcon(
    'sonotec-worldwide',
    BitmapIconProvider::class,
    ['source' => 'EXT:sonotec_config/Resources/Public/Img/Icons/worldwide.svg']
);
$iconRegistry->registerIcon(
    'sonotec-expertise',
    BitmapIconProvider::class,
    ['source' => 'EXT:sonotec_config/Resources/Public/Img/Icons/expertise.svg']
);
$iconRegistry->registerIcon(
    'sonotec-pruefkoepfe',
    BitmapIconProvider::class,
    ['source' => 'EXT:sonotec_config/Resources/Public/Img/Icons/pruefkoepfe.svg']
);
$iconRegistry->registerIcon(
    'sonotec-application',
    BitmapIconProvider::class,
    ['source' => 'EXT:sonotec_config/Resources/Public/Img/Icons/application.svg']
);


$iconRegistry->registerIcon(
    'sonotec-biotech-sonotec',
    BitmapIconProvider::class,
    ['source' => 'EXT:sonotec_config/Resources/Public/Img/Icons/biotech-sonotec.svg']
);

$iconRegistry->registerIcon(
    'sonotec-medtech-sonotec',
    BitmapIconProvider::class,
    ['source' => 'EXT:sonotec_config/Resources/Public/Img/Icons/medtech-sonotec.svg']
);

$iconRegistry->registerIcon(
    'sonotec-semicon-sonotec',
    BitmapIconProvider::class,
    ['source' => 'EXT:sonotec_config/Resources/Public/Img/Icons/semicon-sonotec.svg']
);
