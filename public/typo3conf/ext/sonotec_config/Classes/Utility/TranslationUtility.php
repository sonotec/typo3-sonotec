<?php
namespace TYPO3\SonotecConfig\Utility;

use Doctrine\DBAL\Driver\Exception;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * Class RecordExistsUtility
 */
class TranslationUtility
{
    public function getTableAndUidIfExists(array $extbaseConfig, array $getParameters) : array
    {
        $extbaseTable = false;
        $extbaseParam = false;

        if(sizeof($extbaseConfig)>0) {
            foreach ($extbaseConfig as $plugin=>$param) {
                if(array_key_exists($plugin, $getParameters) && isset($getParameters[$plugin][array_key_first($param)])) {
                    $extbaseTable = $param[array_key_first($param)];
                    $extbaseParam = $getParameters[$plugin][array_key_first($param)];
                    break;
                }
            }
        }
        return ['extbaseTable' => $extbaseTable, 'extbaseParam' => $extbaseParam];
    }


    /**
     * @param string $table
     * @param int $origUid
     * @param int $language
     * @return bool
     * @throws Exception
     */
    public function checkIfRecordExists(string $table, int $origUid, int $language) : bool
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable($table)->createQueryBuilder();
        $result = $queryBuilder->select('uid')->from($table)
            ->where(
                $queryBuilder->expr()->eq('l10n_parent', $queryBuilder->createNamedParameter($origUid)),
                $queryBuilder->expr()->eq('sys_language_uid', $queryBuilder->createNamedParameter($language))
            )
            ->execute()
            ->fetchAllAssociative();

        return isset($result[0]);
    }
}
