<?php
namespace TYPO3\SonotecConfig\HrefLang\EventListener;

use TYPO3\SonotecConfig\Utility\TranslationUtility;
use Doctrine\DBAL\Driver\Exception;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Event\ModifyHrefLangTagsEvent;

class RemoveHrefLangTags
{
    /**
     * @param ModifyHrefLangTagsEvent $event
     * @throws Exception
     */
    public function __invoke(ModifyHrefLangTagsEvent $event): void
    {
        //get hreflang tags
        $hrefLangs = $event->getHrefLangs();

        //get language settings
        $languages = $event->getRequest()->getAttributes()['site']->getConfiguration()['languages'];

        if(sizeof($languages) > 1) {
            //check if page translation exists
            $hrefLangs = $this->unsetHrefLangTags($hrefLangs, $languages, 'pages', $GLOBALS['TSFE']->id);

            //get hreflang config for extbase plugins
            $getParameters = GeneralUtility::_GET();

            if(sizeof($getParameters)>0) {
                $hreflangConfig = $event->getRequest()->getAttributes()['site']->getConfiguration()['languageHandlingExtbasePlugins'];
                //check if translation for records exists
                $hrefLangs = $this->checkExtbaseParameters($getParameters, $hrefLangs, $hreflangConfig, $languages);
            }

            //remove all hreflang tags if only default language is left
            if(sizeof($hrefLangs) === 2) {
                  $hrefLangs = [];
            }

            // Update the hreflang tags in the event
            $event->setHrefLangs($hrefLangs);
        }
    }

    /**
     * @param array $getParameters
     * @param array $hrefLangs
     * @param array $hreflangConfig
     * @param array $languages
     * @return array
     * @throws Exception
     */
    public function checkExtbaseParameters(array $getParameters, array $hrefLangs, array $hreflangConfig, array $languages) : array
    {
        //check if any get parameter from hreflang-config is set
        $result = GeneralUtility::makeInstance(TranslationUtility::class)->getTableAndUidIfExists($hreflangConfig, $getParameters);

        if($result['extbaseTable']!==false && $result['extbaseParam']!==false) {
            //check if translation exists - unset hreflang if not
            $hrefLangs = $this->unsetHrefLangTags($hrefLangs, $languages, $result['extbaseTable'], $result['extbaseParam']);
        }

        return $hrefLangs;
    }

    /**
     * @param array $hrefLangs
     * @param array $languages
     * @param string $table
     * @param int $uid
     * @return array
     * @throws Exception
     */
    public function unsetHrefLangTags(array $hrefLangs, array $languages, string $table, int $uid) : array
    {
        foreach ($languages as $language) {
            if($language['languageId'] !== 0) {
                $recordExists = GeneralUtility::makeInstance(TranslationUtility::class)->checkIfRecordExists($table, $uid, $language['languageId']);
                if($recordExists === false) {
                    unset($hrefLangs[$language['hreflang']]);
                }
            }
        }

        return $hrefLangs;
    }
}
