<?php

namespace TYPO3\SonotecConfig\Hooks;


use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\SonotecConfig\Service\FormRecipient;

class ReceiverPrefill
{
    /**
     * @param \TYPO3\CMS\Form\Domain\Model\Renderable\RenderableInterface $renderable
     * @return void
     */
    public function initializeFormElement(\TYPO3\CMS\Form\Domain\Model\Renderable\RenderableInterface $renderable)
    {
        //prefill page path
        if (strpos($renderable->getUniqueIdentifier(), 'pagepath')) {
            if(GeneralUtility::_GP('ref')) {
                $link = GeneralUtility::_GP('ref');
            }
            else {
                $cObj = GeneralUtility::makeInstance(ContentObjectRenderer::class);

                $conf = array(
                    'parameter' => $GLOBALS['TSFE']->id,
                    'useCashHash' => false,
                    'returnLast' => 'url',
                    'forceAbsoluteUrl'  => true
                );
                $link = $cObj->typolink_URL($conf);
            }

            if(GeneralUtility::_GP('utm_campaign') || GeneralUtility::_GP('_unit') || GeneralUtility::_GP('_unitsub') || GeneralUtility::_GP('utm_source') || GeneralUtility::_GP('utm_medium')) {
                $conf = array(
                    'parameter' => 1,
                    'useCashHash' => false,
                    'returnLast' => 'url',
                    'forceAbsoluteUrl'  => true
                );
                $link = $cObj->typolink_URL($conf).substr($_SERVER['REQUEST_URI'], 1);
            }
            $renderable->setDefaultValue($link);
        }

        //tracking parameters
        $session = !empty($GLOBALS["TSFE"]) ? $GLOBALS["TSFE"]->fe_user->getKey("ses","sonotec_tracking") : [];
        if ((strpos($renderable->getUniqueIdentifier(), '_unit') && !strpos($renderable->getUniqueIdentifier(), '_unitsub')) && (GeneralUtility::_GP('_unit') || !empty($session['_unit']))) {
            $renderable->setDefaultValue(GeneralUtility::_GP('_unit') ? GeneralUtility::_GP('_unit') : (!empty($session['_unit']) ? $session['_unit']: ''));
        }
        if (strpos($renderable->getUniqueIdentifier(), '_unitsub')  && (GeneralUtility::_GP('_unitsub') || !empty($session['_unitsub']))) {
            $renderable->setDefaultValue(GeneralUtility::_GP('_unitsub') ? GeneralUtility::_GP('_unitsub') : (!empty($session['_unitsub']) ? $session['_unitsub']: ''));
        }

        //prefill receiver uid
        $formDefinitions = ['contact-1-recipientuid','contact-111-recipientuid','contact-112-recipientuid','contact-zfp-recipientuid'];

        if (in_array($renderable->getUniqueIdentifier(), $formDefinitions) && (GeneralUtility::_GP('contactMap') || GeneralUtility::_GP('contact'))) {
            $recipientUid = GeneralUtility::_GP('contactMap') ? GeneralUtility::_GP('contactMap') : (GeneralUtility::_GP('contact') ? GeneralUtility::_GP('contact') : '');
            $renderable->setDefaultValue($recipientUid);
        }

        //prefill receiver type
        if (in_array($renderable->getUniqueIdentifier(), $formDefinitions) && (GeneralUtility::_GP('contactMap') || GeneralUtility::_GP('contact'))) {
            $recipientType = GeneralUtility::_GP('contactMap') ? 'map' : (GeneralUtility::_GP('contact') ? 'contact' : '');
            $renderable->setDefaultValue($recipientType);
        }

        //prefill receiver text
        $formDefinitions = ['contact-1-receivertext','contact-111-receivertext','contact-112-receivertext','contact-zfp-receivertext'];
        if (in_array($renderable->getUniqueIdentifier(), $formDefinitions)) {
            $recipientUid = GeneralUtility::_GP('contactMap') ? GeneralUtility::_GP('contactMap') : (GeneralUtility::_GP('contact') ? GeneralUtility::_GP('contact') : 0);
            $recipientType = GeneralUtility::_GP('contactMap') ? 'map' : (GeneralUtility::_GP('contact') ? 'contact' : '');

            $recipients = FormRecipient::getContact([], $recipientUid, $recipientType);

            $label = LocalizationUtility::translate('LLL:EXT:sonotec_config/Resources/Private/Language/locallang_frontend.xlf:ll.receiver','sonotec_config');


            $email = (is_array($recipients) && isset($recipients[0])) ? $recipients[0]->getAddress() : '';

            if($email) {
                $renderable->setProperty('text',"<strong>$label:</strong> ". str_replace(['.','@'],['<span>&period;</span>','<span>&commat;</span>'],$email));
            }
        }

        //prefill receiver email address
        $formDefinitions = ['contact-1-receiver','contact-111-receiver','contact-112-receiver','contact-zfp-111-receiver'];
        if (in_array($renderable->getUniqueIdentifier(), $formDefinitions)) {
            $recipientUid = GeneralUtility::_GP('contactMap') ? GeneralUtility::_GP('contactMap') : (GeneralUtility::_GP('contact') ? GeneralUtility::_GP('contact') : 0);
            $recipientType = GeneralUtility::_GP('contactMap') ? 'map' : (GeneralUtility::_GP('contact') ? 'contact' : '');

            $recipients = FormRecipient::getContact([], $recipientUid, $recipientType);

            $email = (is_array($recipients) && isset($recipients[0])) ? $recipients[0]->getAddress() : 'sonotec@sonotec.de';

            $renderable->setDefaultValue($email);
        }
    }
}
