<?php
namespace TYPO3\SonotecConfig\Middleware;


use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TYPO3\CMS\Core\Http\JsonResponse;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class Session implements MiddlewareInterface
{
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $queryParams = $request->getQueryParams();

        if(!empty($queryParams) && (!empty($queryParams['_unit']) || !empty($queryParams['_unitsub'])) ) {
            $sessionValue = [];
            if(!empty($queryParams['_unit'])) {
                $sessionValue['_unit'] = $queryParams['_unit'];
            }
            if(!empty($queryParams['_unitsub'])) {
                $sessionValue['_unitsub'] = $queryParams['_unitsub'];
            }

            $GLOBALS['TSFE']->fe_user->setKey("ses","sonotec_tracking",$sessionValue);
        }



        return $handler->handle($request);
    }
}
