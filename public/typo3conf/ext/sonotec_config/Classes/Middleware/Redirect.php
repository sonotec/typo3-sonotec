<?php
namespace TYPO3\SonotecConfig\Middleware;


use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TYPO3\CMS\Core\Http\JsonResponse;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class Redirect implements MiddlewareInterface
{
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $normalizedParams = $request->getAttribute('normalizedParams');
        $uri = $normalizedParams->getRequestUri();
        $host = $normalizedParams->getRequestHostOnly();

        if (($host === 'www.sonotec.eu' || $host === 'eu.sonotec.localhost') &&
            (strpos($uri, '/en/')!==0 && strpos($uri, '/it/')!==0) && strpos($uri, '/fr/')!==0 && strpos($uri, '/es/')!==0) {
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: https://$host/en$uri");
            header("Connection: close");
        }

        return $handler->handle($request);
    }
}
