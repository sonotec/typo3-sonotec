<?php
namespace TYPO3\SonotecConfig\Service;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use Symfony\Component\Mime\Address;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Service\FlexFormService;

class FormRecipient {

    /**
     * @param array $recipientAddress
     * @param int $contactUid
     * @param string $contactType
     * @return array
     */
    public static function getContact(array $recipientAddress, int $contactUid=0, string $contactType='') : array
    {
        $contactUid = $contactUid ? $contactUid : (intval($GLOBALS['TSFE']->page['sonotec_contact']) ? intval($GLOBALS['TSFE']->page['sonotec_contact']) : NULL);
        $contactType = $contactType ? $contactType : ($contactUid ? 'contact' : '');

        if($contactUid!==NULL && $contactType=='contact') {

            $recipientAddress = FormRecipient::getContactData($recipientAddress, $contactUid);
        }
        if($contactUid!==NULL && $contactType=='map') {
            $recipientAddress = FormRecipient::getMapContactData($recipientAddress, $contactUid);
        }
        return $recipientAddress;
    }

    /**
     * @param array $recipientAddress
     * @param int $contactUid
     * @return array
     */
    public static function getContactData(array $recipientAddress, int $contactUid=0) : array
    {

        $row = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_sonoteccontact_domain_model_contact')
            ->select(
                ['name','email'],
                'tx_sonoteccontact_domain_model_contact', // from
                ['uid' => $contactUid],
                [], // group
                [], // order
                1   // limit
            )
            ->fetch();

        if($row['email']) {
            $modifiedRecipientAddress = new Address($row['email'],$row['name']);
            $recipientAddress = [$modifiedRecipientAddress];
        }

        return $recipientAddress;
    }

    /**
     * @param array $recipientAddress
     * @param int $contactUid
     * @return array
     */
    public static function getMapContactData(array $recipientAddress, int $contactUid=0) : array
    {
        $row = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_sonotecmap_domain_model_address')
            ->select(
                ['email'],
                'tx_sonotecmap_domain_model_address', // from
                ['uid' => $contactUid],
                [], // group
                [], // order
                1   // limit
            )
            ->fetch();

        if($row['email']) {
            $modifiedRecipientAddress = new Address($row['email']);
            $recipientAddress = [$modifiedRecipientAddress];
        }

        return $recipientAddress;
    }

    /**
     * @param int $product
     * @return Address[]|false|void
     * @throws \TYPO3\CMS\Extbase\Configuration\Exception\InvalidConfigurationTypeException
     */
    public static function getRecipientByProduct(int $product) {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_sonotecretour_domain_model_product');
        $product = $queryBuilder
            ->select('*')
            ->from('tx_sonotecretour_domain_model_product')
            ->where(
                $queryBuilder->expr()->eq('uid', $product)
            )
            ->execute()->fetchAll();

        if(!empty($product[0]['product_group'])) {
            $configurationManager = GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Configuration\ConfigurationManager::class);
            $typoScript = $configurationManager->getConfiguration(
                \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT,'sonotec_retour');
            $email = $typoScript['plugin.']['tx_sonotecretour.']['settings.']['receiver.'][$product[0]['product_group']];

            return $email ? [new Address($email,'SONOTEC GmbH')] : false;
        }
    }
}
