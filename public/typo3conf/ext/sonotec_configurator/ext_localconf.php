<?php

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'SonotecConfigurator',
            'Pi1',
            [
                \TYPO3\SonotecConfigurator\Controller\SensorController::class => 'index'
            ],
            [
                \TYPO3\SonotecConfigurator\Controller\SensorController::class => 'index'
            ],
            \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
        );

        $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);

        $iconRegistry->registerIcon(
            'sonotecconfigurator_pi1',
            \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
            ['source' => 'EXT:sonotec_configurator/Resources/Public/Icons/user_plugin_pi1.svg']
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            '
               mod.wizards.newContentElement.wizardItems.common.elements.sonotecconfigurator_pi1 {
                    iconIdentifier = sonotecconfigurator_pi1
                    title = Sensor Types
                    description =
                    tt_content_defValues {
                        CType = sonotecconfigurator_pi1
                    }
                }
                mod.wizards.newContentElement.wizardItems.common.show := addToList(sonotecconfigurator_pi1)
           '
        );
    }
);
