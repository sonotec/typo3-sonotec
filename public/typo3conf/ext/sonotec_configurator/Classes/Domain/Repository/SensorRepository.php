<?php
namespace TYPO3\SonotecConfigurator\Domain\Repository;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
/***
 *
 * This file is part of the "smapOne Jobposting" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Felix Franz <email@felixfranz.de>
 *
 ***/

/**
 * The repository for Categories
 */
class SensorRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * @param array $arguments
     * @return array|\mixed[][]
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    public function findSensors(array $arguments) {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_sonotecconfigurator_domain_model_sensor')->createQueryBuilder();
        $queryBuilder->select('*')
            ->from('tx_sonotecconfigurator_domain_model_sensor')
            ->orderBy('title', 'ASC');

        if(!empty($arguments['diameter'])) {
            $queryBuilder->andWhere($queryBuilder->expr()->eq('tube_dimension', $queryBuilder->createNamedParameter($arguments['diameter'])));
        }

        if(!empty($arguments['material'])) {
            $queryBuilder->andWhere($queryBuilder->expr()->eq('material', $queryBuilder->createNamedParameter($arguments['material'])));
        }

        if(!empty($arguments['flowrate'])) {
            $queryBuilder->andWhere($queryBuilder->expr()->eq('flowrate', $queryBuilder->createNamedParameter($arguments['flowrate'])));
        }

        if(!empty($arguments['housing'])) {
            $queryBuilder->andWhere($queryBuilder->expr()->eq('housing', $queryBuilder->createNamedParameter($arguments['housing'])));
        }

        if(!empty($arguments['accuracy'])) {
            $queryBuilder->andWhere($queryBuilder->expr()->eq('accuracy', $queryBuilder->createNamedParameter($arguments['accuracy'])));
        }

        if(!empty($arguments['display'])) {
            $queryBuilder->andWhere($queryBuilder->expr()->eq('display', $queryBuilder->createNamedParameter($arguments['display'])));
        }

        if(!empty($arguments['interface'])) {
            $queryBuilder->andWhere($queryBuilder->expr()->eq('interface', $queryBuilder->createNamedParameter($arguments['interface'])));
        }

        return $queryBuilder->execute()->fetchAllAssociative();
    }
}
