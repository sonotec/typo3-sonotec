<?php
namespace TYPO3\SonotecConfigurator\Domain\Repository;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
/***
 *
 * This file is part of the "smapOne Jobposting" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Felix Franz <email@felixfranz.de>
 *
 ***/

/**
 * The repository for Categories
 */
class DiameterRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    public function findDiameters() {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_sonotecconfigurator_domain_model_diameter')->createQueryBuilder();
        $queryBuilder->select('uid', 'title')
            ->from( 'tx_sonotecconfigurator_domain_model_diameter')
            ->orderBy('sorting', 'ASC');

        $diameters = $queryBuilder->execute()->fetchAllAssociative();

        $diameters_output = [];

        foreach ($diameters as $diameter) {
            $diameters_output[$diameter['uid']] = $diameter;
        }

        return $diameters_output;
    }
}
