<?php
namespace TYPO3\SonotecConfigurator\Controller;

use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\SonotecConfigurator\Domain\Repository\DiameterRepository;
use TYPO3\SonotecConfigurator\Domain\Repository\SensorRepository;
/***
 *
 * This file is part of the "smapOne Jobposting" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Felix Franz <email@felixfranz.de>
 *
 ***/

/**
 * SensorController
 */
class SensorController extends ActionController
{

    /**
     * diameterRepository
     *
     * @var DiameterRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $diameterRepository = null;

    /**
     * sensorRepository
     *
     * @var SensorRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $sensorRepository = null;

    /**
     * action index
     *
     * @return void
     */
    public function indexAction()
    {
        $arguments = $this->request->getArguments();
        $diameters = $this->diameterRepository->findDiameters();
        $sensors = $this->sensorRepository->findSensors($arguments);

        $formData = $this->getFormData($sensors, $diameters);

        $this->view->assignMultiple([
            'sensors' => $sensors,
            'formData' => $formData,
            'arguments' => $arguments
        ]);
    }

    protected function getFormData($sensors, $diameters) {

        $formData = [
            'diameters' => [],
            'material' => [],
            'flowrate' => [],
            'housing' => [],
            'interface' => [],
            'accuracy' => [],
            'display' => []
        ];

        foreach ($sensors as $sensor) {
            $formData['diameters'][$sensor['tube_dimension']] = $diameters[$sensor['tube_dimension']]['title'];
            $formData['material'][$sensor['material']] = $sensor['material'];
            $formData['flowrate'][$sensor['flowrate']] = $sensor['flowrate'];
            $formData['housing'][$sensor['housing']] = LocalizationUtility::translate('tx_sonotecconfigurator_domain_model_sensor.housing.'.$sensor['housing'], 'SonotecConfigurator');
            $formData['interface'][$sensor['interface']] = $sensor['interface'];
            $formData['accuracy'][$sensor['accuracy']] = $sensor['accuracy'];
            $formData['display'][$sensor['display']] = $sensor['display'];
        }

        ksort($formData['flowrate']);
        asort($formData['housing']);
        ksort($formData['interface']);
        ksort($formData['accuracy']);
        ksort($formData['interface']);

        $formData['diameters'] = array_intersect_key($diameters, $formData['diameters']);

        return $formData;
    }
}
