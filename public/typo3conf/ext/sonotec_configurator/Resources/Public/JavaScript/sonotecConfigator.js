$(document).ready(function (){
  initSonotecConfigurator();
});

function sendForm(event, reset=false) {
  event.preventDefault();

  var $form = $('#sonotec_sensors_form');
  var $actionUrl = $form.attr('action');

  $.ajax({
    type: "POST",
    url: $actionUrl,
    data: !reset ? $form.serialize() : [],
    success: function(data)
    {
      $form.html(data);
      initSonotecConfigurator();
    }
  });
}

function initSonotecConfigurator() {
  $('#sonotec_sensors_reset').click(function (e){
    sendForm(e, true);
  });

  $("#sonotec_sensors_form select").change(function(e) {
    sendForm(e);
  });
}
