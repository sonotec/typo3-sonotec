<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_sonotecconfigurator_domain_model_sensor', 'EXT:sonotec_configurator/Resources/Private/Language/locallang_csh_tx_sonotecconfigurator_domain_model_sensor.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_sonotecconfigurator_domain_model_sensor');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_sonotecconfigurator_domain_model_diameter', 'EXT:sonotec_configurator/Resources/Private/Language/locallang_csh_tx_sonotecconfigurator_domain_model_diameter.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_sonotecconfigurator_domain_model_diameter');
    }
);
