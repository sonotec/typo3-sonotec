<?php
return [
    'ctrl' => [
        'title'	=> 'LLL:EXT:sonotec_configurator/Resources/Private/Language/locallang_db.xlf:tx_sonotecconfigurator_domain_model_sensor',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => TRUE,
        'delete' => 'deleted',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'title,',
        'iconfile' => 'EXT:sonotec_configurator/Resources/Public/Icons/tx_sonotecconfigurator_domain_model_sensor.gif'
    ],
    'types' => [
        '1' => ['showitem' => '
		title,tube_dimension,material,material_title,flowrate,accuracy,accuracy_low,accuracy_high,housing,display,interface,accessories,
		--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid, l10n_parent, l10n_diffsource,
        --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,hidden, starttime, endtime'
        ],
    ],
    'palettes' => [
        '1' => ['showitem' => ''],
    ],
    'columns' => [

        'sys_language_uid' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => -1,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_sonotecreferences_domain_model_category',
                'foreign_table_where' => 'AND {#tx_sonotecreferences_domain_model_category}.{#pid}=###CURRENT_PID### AND {#tx_sonotecreferences_domain_model_category}.{#sys_language_uid} IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ],
        ],
        'starttime' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'endtime' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],


        'title' => [
            'label' => 'LLL:EXT:sonotec_configurator/Resources/Private/Language/locallang_db.xlf:tx_sonotecconfigurator_domain_model_sensor.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim, required'
            ],
        ],

        'tube_dimension' => [
            'label' => 'LLL:EXT:sonotec_configurator/Resources/Private/Language/locallang_db.xlf:tx_sonotecconfigurator_domain_model_sensor.tube_dimension',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'size' => 1,
                'eval' => 'trim',
                'foreign_table' => 'tx_sonotecconfigurator_domain_model_diameter',
                'foreign_table_where' => 'AND ORDER BY tx_sonotecconfigurator_domain_model_diameter.sorting ASC'
            ],
        ],

        'material' => [
            'label' => 'LLL:EXT:sonotec_configurator/Resources/Private/Language/locallang_db.xlf:tx_sonotecconfigurator_domain_model_sensor.material',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'size' => 1,
                'eval' => 'trim',
                'items' => [
                    ['LLL:EXT:sonotec_configurator/Resources/Private/Language/locallang_db.xlf:tx_sonotecconfigurator_domain_model_sensor.material.1', 1],
                    ['LLL:EXT:sonotec_configurator/Resources/Private/Language/locallang_db.xlf:tx_sonotecconfigurator_domain_model_sensor.material.2', 2],
                    ['LLL:EXT:sonotec_configurator/Resources/Private/Language/locallang_db.xlf:tx_sonotecconfigurator_domain_model_sensor.material.3', 3]
                ],
            ],
        ],

        'material_title' => [
            'label' => 'LLL:EXT:sonotec_configurator/Resources/Private/Language/locallang_db.xlf:tx_sonotecconfigurator_domain_model_sensor.material_title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],

        'flowrate' => [
            'label' => 'LLL:EXT:sonotec_configurator/Resources/Private/Language/locallang_db.xlf:tx_sonotecconfigurator_domain_model_sensor.flowrate',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],

        'accuracy' => [
            'label' => 'LLL:EXT:sonotec_configurator/Resources/Private/Language/locallang_db.xlf:tx_sonotecconfigurator_domain_model_sensor.accuracy',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],

        'accuracy_low' => [
            'label' => 'LLL:EXT:sonotec_configurator/Resources/Private/Language/locallang_db.xlf:tx_sonotecconfigurator_domain_model_sensor.accuracy_low',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],

        'accuracy_high' => [
            'label' => 'LLL:EXT:sonotec_configurator/Resources/Private/Language/locallang_db.xlf:tx_sonotecconfigurator_domain_model_sensor.accuracy_high',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],

        'housing' => [
            'label' => 'LLL:EXT:sonotec_configurator/Resources/Private/Language/locallang_db.xlf:tx_sonotecconfigurator_domain_model_sensor.housing',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'size' => 1,
                'eval' => 'trim',
                'items' => [
                    ['LLL:EXT:sonotec_configurator/Resources/Private/Language/locallang_db.xlf:tx_sonotecconfigurator_domain_model_sensor.housing.1', 1],
                    ['LLL:EXT:sonotec_configurator/Resources/Private/Language/locallang_db.xlf:tx_sonotecconfigurator_domain_model_sensor.housing.2', 2],
                    ['LLL:EXT:sonotec_configurator/Resources/Private/Language/locallang_db.xlf:tx_sonotecconfigurator_domain_model_sensor.housing.3', 3]
                ],
            ],
        ],

        'display' => [
            'label' => 'LLL:EXT:sonotec_configurator/Resources/Private/Language/locallang_db.xlf:tx_sonotecconfigurator_domain_model_sensor.display',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'size' => 1,
                'eval' => 'trim',
                'items' => [
                    ['LLL:EXT:sonotec_configurator/Resources/Private/Language/locallang_db.xlf:tx_sonotecconfigurator_domain_model_sensor.display.1', 1],
                    ['LLL:EXT:sonotec_configurator/Resources/Private/Language/locallang_db.xlf:tx_sonotecconfigurator_domain_model_sensor.display.2', 2]
                ],
            ],
        ],

        'interface' => [
            'label' => 'LLL:EXT:sonotec_configurator/Resources/Private/Language/locallang_db.xlf:tx_sonotecconfigurator_domain_model_sensor.interface',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'size' => 1,
                'eval' => 'trim',
                'items' => [
                    ['LLL:EXT:sonotec_configurator/Resources/Private/Language/locallang_db.xlf:tx_sonotecconfigurator_domain_model_sensor.interface.1', 1],
                    ['LLL:EXT:sonotec_configurator/Resources/Private/Language/locallang_db.xlf:tx_sonotecconfigurator_domain_model_sensor.interface.2', 2]
                ],
            ],
        ],

        'accessories' => [
            'label' => 'LLL:EXT:sonotec_configurator/Resources/Private/Language/locallang_db.xlf:tx_sonotecconfigurator_domain_model_sensor.accessories',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],

    ],
];
