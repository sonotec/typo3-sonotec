#
# Table structure for table 'tx_sonotecconfigurator_domain_model_sensor'
#
CREATE TABLE tx_sonotecconfigurator_domain_model_sensor (
    title varchar(255) DEFAULT '' NOT NULL,
	tube_dimension varchar(255) DEFAULT '' NOT NULL,
    material varchar(255) DEFAULT '' NOT NULL,
    material_title varchar(255) DEFAULT '' NOT NULL,
    flowrate varchar(255) DEFAULT '' NOT NULL,
    accuracy varchar(255) DEFAULT '' NOT NULL,
    accuracy_low varchar(255) DEFAULT '' NOT NULL,
    accuracy_high varchar(255) DEFAULT '' NOT NULL,
    housing varchar(255) DEFAULT '' NOT NULL,
    display varchar(255) DEFAULT '' NOT NULL,
    interface varchar(255) DEFAULT '' NOT NULL,
    accessories varchar(255) DEFAULT '' NOT NULL,
);

#
# Table structure for table 'tx_sonotecconfigurator_domain_model_diameter'
#
CREATE TABLE tx_sonotecconfigurator_domain_model_diameter (
    title varchar(255) DEFAULT '' NOT NULL,
    sorting int(11) unsigned NOT NULL default '0'
);
