<?php
namespace TYPO3\FfFormCountrySelect\ViewHelpers;

/***
 *
 * This file is part of the "smapOne Jobposting" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Felix Franz <email@felixfranz.de>
 *
 ***/

use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * CdataViewHelper
 *
 * wraps given in content with <![CDATA[...]]>
 */
class CountrySelectViewHelper extends AbstractViewHelper
{
   /**
    * As this ViewHelper renders HTML, the output must not be escaped.
    *
    * @var bool
    */
    protected $escapeOutput = false;

    public function render()
    {
        $languageAspect = GeneralUtility::makeInstance(Context::class)->getAspect('language');

        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('sys_language');
        $languages = $queryBuilder
            ->select('uid', 'language_isocode')
            ->from('sys_language')
            ->where(
                $queryBuilder->expr()->eq('hidden', $queryBuilder->createNamedParameter(0))
            )
            ->execute()->fetchAll();

        $language = 'en';

        foreach($languages as $l) {
            if ($l['uid'] == $languageAspect->getId()) {
                $language = $l['language_isocode'];
                break;
            }
        }

        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('static_countries');
        $countries = $queryBuilder
            ->select('cn_short_en', 'cn_short_de', 'cn_iso_3')
            ->from('static_countries')
            ->where(
                $queryBuilder->expr()->eq('deleted', $queryBuilder->createNamedParameter(0))
            )
            ->orderBy('cn_short_'.$language)
            ->execute()->fetchAll();

        $selectCountries = [
            '<option value=""></option>'
        ];

        foreach ($countries as $country) {
            $selectCountries[$country['cn_iso_3']] = '<option value="'.$country['cn_iso_3'].'">'.$country['cn_short_'.$language].'</option>';
        }

        return implode(' ', $selectCountries);
    }
}
