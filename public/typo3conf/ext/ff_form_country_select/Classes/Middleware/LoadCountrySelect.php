<?php
namespace TYPO3\FfFormCountrySelect\Middleware;


use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Http\JsonResponse;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class LoadCountrySelect implements MiddlewareInterface
{
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $normalizedParams = $request->getAttribute('normalizedParams');
        $uri = $normalizedParams->getRequestUri();

        if (strpos($uri, '/sonotec-country-select-service/')!==false) {
            $languageAspect = GeneralUtility::makeInstance(Context::class)->getAspect('language');

            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('sys_language');
            $languages = $queryBuilder
                ->select('uid', 'language_isocode')
                ->from('sys_language')
                ->where(
                    $queryBuilder->expr()->eq('hidden', $queryBuilder->createNamedParameter(0))
                )
                ->execute()->fetchAll();

            $language = 'en';

            foreach($languages as $l) {
                if ($l['uid'] == $languageAspect->getId()) {
                    $language = $l['language_isocode'];
                    break;
                }
            }

            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('static_countries');
            $countries = $queryBuilder
                ->select('cn_short_en', 'cn_short_de', 'cn_iso_3')
                ->from('static_countries')
                ->where(
                    $queryBuilder->expr()->eq('deleted', $queryBuilder->createNamedParameter(0))
                )
                ->orderBy('cn_short_'.$language)
                ->execute()->fetchAll();

            $selectCountries = [
                '<option value=""></option>'
            ];

            foreach ($countries as $country) {
                $selectCountries[$country['cn_iso_3']] = '<option value="'.$country['cn_iso_3'].'">'.$country['cn_short_'.$language].'</option>';
            }

            echo implode(' ', $selectCountries);




echo 1;
exit();
            $searchTerm = '';
            if($searchTerm) {


                return new JsonResponse($locations);
            }
            else {
                return new JsonResponse([]);
            }
        }

        return $handler->handle($request);

    }
}
