<?php
return [
    'frontend' => [
        'sonotec-country-select-service'  => [
            'target' => \TYPO3\FfFormCountrySelect\Middleware\LoadCountrySelect::class,
            'before' => [
                'typo3/cms-frontend/page-resolver',
            ],
        ],
    ]
];
