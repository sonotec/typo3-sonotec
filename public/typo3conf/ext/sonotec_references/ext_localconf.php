<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'SonotecReferences',
            'Pi1',
            [
                \SONOTEC\SonotecReferences\Controller\ReferenceController::class => 'reference'
            ],
            // non-cacheable actions
            [
                \SONOTEC\SonotecReferences\Controller\ReferenceController::class => ''
            ],
            \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
        );

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            '
               mod.wizards.newContentElement.wizardItems.common.elements.sonotecreferences_pi1 {
                    iconIdentifier = content-carousel-item-textandimage
                    title = LLL:EXT:sonotec_references/Resources/Private/Language/locallang_db.xlf:tx_sonotec_references_pi1.name
                    description = LLL:EXT:sonotec_references/Resources/Private/Language/locallang_db.xlf:tx_sonotec_references_pi1.description
                    tt_content_defValues {
                        CType = sonotecreferences_pi1
                    }
                }
                mod.wizards.newContentElement.wizardItems.common.show := addToList(sonotecreferences_pi1)
           '
        );

		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);

        $iconRegistry->registerIcon(
            'sonotec_references-plugin-pi1',
            \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
            ['source' => 'EXT:sonotec_references/Resources/Public/Icons/user_plugin_pi1.svg']
        );

    }
);
