#
# Table structure for table 'tx_sonotecreferences_domain_model_reference'
#
CREATE TABLE tx_sonotecreferences_domain_model_reference (
    company varchar(255) DEFAULT '' NOT NULL,
	description text,
    name varchar(255) DEFAULT '' NOT NULL,
    function varchar(255) DEFAULT '' NOT NULL,
    image int(11) unsigned NOT NULL default '0',
    logo int(11) unsigned NOT NULL default '0',
    category int(11) unsigned DEFAULT '0' NOT NULL,
);

#
# Table structure for table 'tx_sonotecreferences_domain_model_category'
#
CREATE TABLE tx_sonotecreferences_domain_model_category (
     title varchar(255) DEFAULT '' NOT NULL
);


#
# Table structure for table 'tx_sonotecreferences_reference_category_mm'
#
CREATE TABLE tx_sonotecreferences_reference_category_mm (
   uid_local int(11) unsigned DEFAULT '0' NOT NULL,
   uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
   sorting int(11) unsigned DEFAULT '0' NOT NULL,
   sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,
   KEY uid_local (uid_local),
   KEY uid_foreign (uid_foreign),
);
