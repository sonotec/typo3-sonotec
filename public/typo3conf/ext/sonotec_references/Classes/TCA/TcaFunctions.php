<?php
namespace SONOTEC\SonotecReferences\TCA;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;

class TcaFunctions
{
    /**
     * Returns a list of category fields for a given table for populating selector "category_field"
     * in tt_content table (called as itemsProcFunc).
     *
     * @param array $params
     */
    public function formatReferenceLabel(array &$params)
    {
        $title = '';

        if(is_int($params['row']['uid']) ) {

            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_sonotecreferences_domain_model_reference');
            $statement = $queryBuilder->select('name','company')
                ->from('tx_sonotecreferences_domain_model_reference')
                ->where(
                    $queryBuilder->expr()->eq('uid',$params['row']['uid'])
                )
                ->execute();

            $record = $statement->fetchAll();

            if(isset($record[0])) {
                $title = $record[0];
            }
        }

        if(is_array($title)) {
            $params['title'] = $title['company'] . ' | ' . $title['name'];
        }
    }
}
