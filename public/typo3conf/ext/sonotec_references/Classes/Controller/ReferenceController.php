<?php
namespace SONOTEC\SonotecReferences\Controller;

/***
 *
 * This file is part of the "SONOTEC reference" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020
 *
 ***/


/**
 * ReferenceController
 */
class ReferenceController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * referenceRepository
     *
     * @var \SONOTEC\SonotecReferences\Domain\Repository\ReferenceRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $referenceRepository = null;



    /**
     * action contact
     *
     * @return void
     */
    public function referenceAction()
    {
        $data = $this->configurationManager->getContentObject()->data;

        if($data['categories']>0) {
            $references = $this->referenceRepository->findReferencesByCategories($data['categories']);
        }
        /*
        if($data['records']) {

        }
        */

        $this->view->assignMultiple([
            'references' => $references,
            'data' => $data
        ]);
    }
}
