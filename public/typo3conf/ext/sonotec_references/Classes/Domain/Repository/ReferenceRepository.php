<?php
namespace SONOTEC\SonotecReferences\Domain\Repository;


/***
 *
 * This file is part of the "SONOTEC contact" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020
 *
 ***/
/**
 * The repository for Contacts
 */
class ReferenceRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    public function findReferencesByCategories($category)
    {
        $query = $this->createQuery();
        $constraints = [];

        $constraints[] = $query->contains('category', $category);

        $query->matching(
            $query->logicalAnd($constraints)
        );

        return $query->execute();
    }
}
