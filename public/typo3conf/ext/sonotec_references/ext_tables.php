<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_sonotecreferences_domain_model_reference', 'EXT:sonotec_references/Resources/Private/Language/locallang_csh_tx_sonotecreferences_domain_model_reference.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_sonotecreferences_domain_model_reference');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_sonotecreferences_domain_model_category', 'EXT:sonotec_references/Resources/Private/Language/locallang_csh_tx_sonotecreferences_domain_model_category.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_sonotecreferences_domain_model_category');
    }
);
