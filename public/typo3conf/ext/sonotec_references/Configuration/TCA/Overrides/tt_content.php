<?php

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
    [
        'LLL:EXT:sonotec_references/Resources/Private/Language/locallang_db.xlf:tx_sonotec_references_pi1.name',
        'sonotecreferences_pi1',
        'content-carousel-item-textandimage'
    ],
    'CType',
    'sonotec_references'
);

$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['sonotecreferences_pi1'] = 'content-carousel-item-textandimage';

$GLOBALS['TCA']['tt_content']['types']['sonotecreferences_pi1'] = [
    'showitem' => '
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.headers;headers,
        categories,
      --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,snm_layout,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
         --palette--;;language,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
         --palette--;;hidden,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
         rowDescription,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,
   ',
    'columnsOverrides' => [
        'records' => [
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'allowed' => 'tx_sonotecreferences_domain_model_reference',
                'foreign_table' => 'tx_sonotecreferences_domain_model_reference',
                'foreign_table_where' => 'AND tx_sonotecreferences_domain_model_reference.sys_language_uid IN(-1,0)'
            ]
        ],
        'categories' => [
            'exclude' => 0,
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'allowed' => 'tx_sonotecreferences_domain_model_category',
                'foreign_table' => 'tx_sonotecreferences_domain_model_category',
                'foreign_table_where' => 'AND tx_sonotecreferences_domain_model_category.sys_language_uid IN(-1,0)',
                'size' => 1,
                'MM' => '',
                'MM_match_fields' => '',
                'MM_opposite_field' => '',
                'items' => [
                    ['', 0]
                ],
                'default' => 0,
            ]
        ]
    ]
];

