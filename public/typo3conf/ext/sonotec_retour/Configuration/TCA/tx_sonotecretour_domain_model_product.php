<?php
return [
    'ctrl' => [
        'title'	=> 'LLL:EXT:sonotec_retour/Resources/Private/Language/locallang_db.xlf:tx_sonotecretour_domain_model_product',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => TRUE,
        'delete' => 'deleted',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'title,',
        'iconfile' => 'EXT:sonotec_retour/Resources/Public/Icons/tx_sonotecretour_domain_model_product.gif'
    ],
    'types' => [
        '1' => ['showitem' => '
		product_group,title,german_customs,hs_code,htsus_code,duty_us,
		--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,sys_language_uid, l10n_parent, l10n_diffsource,
        --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,hidden, starttime, endtime'
        ],
    ],
    'palettes' => [
        '1' => ['showitem' => ''],
    ],
    'columns' => [

        'sys_language_uid' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => -1,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_sonotecreferences_domain_model_category',
                'foreign_table_where' => 'AND {#tx_sonotecreferences_domain_model_category}.{#pid}=###CURRENT_PID### AND {#tx_sonotecreferences_domain_model_category}.{#sys_language_uid} IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ],
        ],
        'starttime' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'endtime' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],


        'title' => [
            'label' => 'LLL:EXT:sonotec_retour/Resources/Private/Language/locallang_db.xlf:tx_sonotecretour_domain_model_product.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim, required'
            ],
        ],

        'german_customs' => [
            'label' => 'LLL:EXT:sonotec_retour/Resources/Private/Language/locallang_db.xlf:tx_sonotecretour_domain_model_product.german_customs',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],

        'hs_code' => [
            'label' => 'LLL:EXT:sonotec_retour/Resources/Private/Language/locallang_db.xlf:tx_sonotecretour_domain_model_product.hs_code',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],

        'htsus_code' => [
            'label' => 'LLL:EXT:sonotec_retour/Resources/Private/Language/locallang_db.xlf:tx_sonotecretour_domain_model_product.htsus_code',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],

        'duty_us' => [
            'label' => 'LLL:EXT:sonotec_retour/Resources/Private/Language/locallang_db.xlf:tx_sonotecretour_domain_model_product.duty_us',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],

        'product_group' => [
            'label' => 'LLL:EXT:sonotec_retour/Resources/Private/Language/locallang_db.xlf:tx_sonotecretour_domain_model_product.product_group',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'size' => 1,
                'eval' => 'trim, required',
                'items' => [
                    [
                        '',
                        ''
                    ],
                    [
                        'LLL:EXT:sonotec_retour/Resources/Private/Language/locallang_db.xlf:tx_sonotecretour_domain_model_product.product_group.1',
                        1
                    ],
                    [
                        'LLL:EXT:sonotec_retour/Resources/Private/Language/locallang_db.xlf:tx_sonotecretour_domain_model_product.product_group.2',
                        2
                    ],
                    [
                        'LLL:EXT:sonotec_retour/Resources/Private/Language/locallang_db.xlf:tx_sonotecretour_domain_model_product.product_group.3',
                        3
                    ],
                    [
                        'LLL:EXT:sonotec_retour/Resources/Private/Language/locallang_db.xlf:tx_sonotecretour_domain_model_product.product_group.4',
                        4
                    ]
                ],
            ],
        ],

    ],
];
