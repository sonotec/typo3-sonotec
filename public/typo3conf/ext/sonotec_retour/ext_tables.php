<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_sonotecretour_domain_model_product', 'EXT:sonotec_retour/Resources/Private/Language/locallang_csh_tx_sonotecretour_domain_model_product.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_sonotecretour_domain_model_product');
    }
);
