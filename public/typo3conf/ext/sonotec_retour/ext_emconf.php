<?php
$EM_CONF[$_EXTKEY] = [
    'title' => 'Sonotec retour',
    'description' => '',
    'category' => 'plugin',
    'author' => 'Felix Franz',
    'author_email' => 'email@felixfranz.de',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0'
];
