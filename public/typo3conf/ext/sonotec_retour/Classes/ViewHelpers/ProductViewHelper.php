<?php
namespace TYPO3\SonotecRetour\ViewHelpers;

/***
 *
 * This file is part of the "smapOne Jobposting" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Felix Franz <email@felixfranz.de>
 *
 ***/

use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * CdataViewHelper
 *
 * wraps given in content with <![CDATA[...]]>
 */
class ProductViewHelper extends AbstractViewHelper
{
   /**
    * As this ViewHelper renders HTML, the output must not be escaped.
    *
    * @var bool
    */
    protected $escapeOutput = false;


    public function initializeArguments()
    {
        $this->registerArgument('product', 'string', '', true, null);
        $this->registerArgument('field', 'string', '', false, 'title');
    }

    public function render()
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_sonotecretour_domain_model_product');
        $product = $queryBuilder
            ->select('*')
            ->from('tx_sonotecretour_domain_model_product')
            ->where(
                $queryBuilder->expr()->eq('uid', (int)$this->arguments['product'])
            )
            ->execute()->fetchAll();

        if(!empty($product[0][$this->arguments['field']])) {
            return $product[0][$this->arguments['field']];
        }

       return '';
    }
}
