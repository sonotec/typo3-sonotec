<?php
namespace TYPO3\SonotecRetour\ViewHelpers;

/***
 *
 * This file is part of the "smapOne Jobposting" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Felix Franz <email@felixfranz.de>
 *
 ***/

use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * CdataViewHelper
 *
 * wraps given in content with <![CDATA[...]]>
 */
class CountryViewHelper extends AbstractViewHelper
{
   /**
    * As this ViewHelper renders HTML, the output must not be escaped.
    *
    * @var bool
    */
    protected $escapeOutput = false;


    public function initializeArguments()
    {
        $this->registerArgument('country', 'string', '', true, null);
    }

    public function render()
    {
        $context = GeneralUtility::makeInstance(Context::class);
        $language = $context->getPropertyFromAspect('language', 'contentId');


        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('static_countries');
        $country = $queryBuilder
            ->select('cn_short_en', 'cn_short_de')
            ->from('static_countries')
            ->where(
                $queryBuilder->expr()->eq('cn_iso_3', $queryBuilder->createNamedParameter($this->arguments['country']))
            )
            ->execute()->fetchAll();

        if(!empty($country[0])) {
            return $language === 1 ? $country[0]['cn_short_de'] : $country[0]['cn_short_en'];
        }

       return '';
    }
}
