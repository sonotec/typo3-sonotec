<?php
namespace TYPO3\SonotecRetour\ViewHelpers;

/***
 *
 * This file is part of the "smapOne Jobposting" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Felix Franz <email@felixfranz.de>
 *
 ***/

use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * CdataViewHelper
 *
 * wraps given in content with <![CDATA[...]]>
 */
class ProductsSelectViewHelper extends AbstractViewHelper
{
   /**
    * As this ViewHelper renders HTML, the output must not be escaped.
    *
    * @var bool
    */
    protected $escapeOutput = false;

    public function render()
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_sonotecretour_domain_model_product');
        $products = $queryBuilder
            ->select('*')
            ->from('tx_sonotecretour_domain_model_product')
            ->addOrderBy('product_group', 'ASC')
            ->execute()->fetchAll();

        $selectProducts = [
            '<option value=""></option>'
        ];

        $i = 0;
        $group = 0;
        foreach ($products as $product) {
            if($group < (int)$product['product_group']) {

                if($i>0) {
                    $selectProducts[] = '</optgroup>';
                }

                $label = LocalizationUtility::translate('tx_sonotecretour_domain_model_product.product_group.'.$product['product_group'], 'sonotec_retour');
                $selectProducts[] = '<optgroup label="'.$label.'">';

                $group = (int)$product['product_group'];
            }
            $selectProducts[] = '<option value="'.$product['uid'].'">'.$product['title'].'</option>';

            $i++;
        }
        $selectProducts[] = '</optgroup>';

        return implode(' ', $selectProducts);
    }
}
