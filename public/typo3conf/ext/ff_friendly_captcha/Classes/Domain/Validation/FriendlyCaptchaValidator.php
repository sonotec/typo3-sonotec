<?php
namespace TYPO3\FfFriendlyCaptcha\Domain\Validation;


use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;

class FriendlyCaptchaValidator extends \TYPO3\CMS\Extbase\Validation\Validator\AbstractValidator
{
    /**
     * This validator always needs to be executed even if the given value is empty.
     * See AbstractValidator::validate()
     *
     * @var bool
     */
    protected $acceptsEmptyValues = false;

    /**
     * Checks if the given property ($propertyValue) is not empty (NULL, empty string, empty array or empty object).
     *
     * @param mixed $value The value that should be validated
     */
    protected function isValid($value)
    {
        if ( $this->validateCaptcha() === false) {
            $this->addError($this->translateErrorMessage(
                'validator.friendlycaptcha.notvalid',
                'ff_friendly_captcha'
            ), 1619778494);
        }
    }

    protected function validateCaptcha() {
        $captcha = GeneralUtility::_GP('frc-captcha-solution');

        if(!$captcha) {
            return false;
        }

        $friendlyCaptchaConfiguration = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(ExtensionConfiguration::class)
            ->get('ff_friendly_captcha');

        $post = [
            'solution' => $captcha,
            'secret' => $friendlyCaptchaConfiguration['secret'],
            'sitekey' => $friendlyCaptchaConfiguration['sitekey']
        ];

        $ch = curl_init($friendlyCaptchaConfiguration['requestUrl']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $response = curl_exec($ch);
        curl_close($ch);

        return $response->success;
    }
}
