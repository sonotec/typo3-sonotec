$(document).ready(function(){
    
    var $input = $('#tx-srfeuserregister-pi1-customer_serialnumber_registration');
    
    $( $input ).keyup(function() {
       $('#ajax-error').hide();
       if($input.val().length == 6) {
            
            $.ajax({
                type: "POST",
                url: "/?eID=check_service_number",
                data: {
                    serialNumber: $input.val()
                }
              }).done( function( data ) {
                if ( data ==1 ) {
                   $('#ajax-error').hide();
                }
                if ( data ==-1 ) {
                   $('#ajax-error').show();
                }
            } );
       }
    });
    
});