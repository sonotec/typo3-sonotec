<?php
declare(strict_types = 1);

return [
    \Sonotec\SonotecCustomers\Domain\Model\User::class => [
        'tableName' => 'fe_users',
        'recordType' => 0,
        'properties' => [
            'customerSerialnumberRegistration' => [
                'fieldName' => 'customer_serialnumber_registration'
            ],
            'moduleSysDmailHtml' => [
                'fieldName' => 'module_sys_dmail_html'
            ],
        ],
    ],

    \In2code\Femanager\Domain\Model\User::class => [
        'subclasses' => [
            \Sonotec\SonotecCustomers\Domain\Model\User::class,
        ]
    ],
];
