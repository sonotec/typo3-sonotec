<?php
return [
    'ctrl' => [
        'title'	=> 'LLL:EXT:sonotec_customers/Resources/Private/Language/locallang_db.xlf:tx_sonoteccustomers_domain_model_serialnumber',
        'label' => 'serialnumber',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
		'versioningWS' => true,
		'delete' => 'deleted',
		'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
		'searchFields' => 'serialnumber,type',
        'iconfile' => 'EXT:sonotec_customers/Resources/Public/Icons/tx_sonoteccustomers_domain_model_serialnumber.gif'
    ],
    'types' => [
		'1' => ['showitem' => 'serialnumber, type, feuser,  --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, hidden, starttime, endtime'],
    ],
    'columns' => [
		'hidden' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
            ],
        ],
		'starttime' => [
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ]
        ],
        'endtime' => [
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ]
            ],
        ],
        'serialnumber' => [
	        'label' => 'LLL:EXT:sonotec_customers/Resources/Private/Language/locallang_db.xlf:tx_sonoteccustomers_domain_model_serialnumber.serialnumber',
	        'config' => [
			    'type' => 'input',
			    'size' => 30,
			    'eval' => 'trim,required'
			],
	    ],
	    'type' => [
	        'label' => 'LLL:EXT:sonotec_customers/Resources/Private/Language/locallang_db.xlf:tx_sonoteccustomers_domain_model_serialnumber.type',
	        'config' => [
                    'type' => 'select',
                    'eval' => 'required',
                    'renderType' => 'selectSingle',
                    'items' => array(
                            array('', '')
                    ),
                    'foreign_table' => 'tx_sonoteccustomers_domain_model_type',
                    'minitems' => 1,
                    'maxitems' => 1
                ],
	    ],
            'feuser' => [
	        'label' => 'LLL:EXT:sonotec_customers/Resources/Private/Language/locallang_db.xlf:tx_sonoteccustomers_domain_model_license.feuser',
	        'config' => [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'readOnly' =>1,
                    'items' => array(
                            array('', 0)
                    ),
                    'foreign_table' => 'fe_users',
                    'foreign_table_where' => ' AND fe_users.usergroup=###PAGE_TSCONFIG_ID###',
                    'minitems' => 0,
                    'maxitems' => 1
                ],
	    ],
        /*
	    'file' => [
	        'label' => 'LLL:EXT:sonotec_customers/Resources/Private/Language/locallang_db.xlf:tx_sonoteccustomers_domain_model_license.file',
	        'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
				'file',
				[
					'appearance' => [
						'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:media.addFileReference'
					],
					'foreign_types' => [
						'0' => [
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						],
						\TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						],
						\TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						],
						\TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						],
						\TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						],
						\TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						]
					],
					'maxitems' => 1
				]
			),
	    ],
        */
    ],
];
