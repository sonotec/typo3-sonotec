<?php

// pageTS config files
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    'sonotec_customers',
    'Configuration/PageTS/page.ts',
    'Sonotec Customers TS Page Configuration'
);
