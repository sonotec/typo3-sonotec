<?php

$tempColumns = array (
    'static_info_country' => array (
        'exclude' => 0,
        'label' => 'Land',

        'config' => array (
            'type' => 'select',
            'renderType' => 'selectSingle',
            'foreign_table' => 'tx_sonotecmap_domain_model_country',
            'foreign_table_where' => 'AND tx_sonotecmap_domain_model_country.sys_language_uid=0 ORDER BY title ASC',
            'items' => array (
                array("",0),
            ),
            'minitems'=>0,
            'maxitems'=>1,
        ),
    ),

    'terms_acknowledged' => array(
        'exclude' => 0,
        'label' => 'Terms (old)',
        'config' => array(
            'type' => 'check',
            'default' => '0',
            'readOnly' => '1'
        )
    ),
    'customer_serialnumber' => array (
        'exclude' => 0,
        'label' => 'LLL:EXT:sonotec_customers/Resources/Private/Language/locallang_db.xlf:fe_users.serialnumber',
        'config' => array (
            'type' => 'inline',
            'foreign_table' => 'tx_sonoteccustomers_domain_model_serialnumber',
            'foreign_field' => 'feuser',
            'items' => array (
                array("",0),
            ),
            'minitems'=>0,
            'maxitems'=>100,
        )
    ),
    'module_sys_dmail_html' => array(
        'label' => 'Receive newsletter?',
        'exclude' => '1',
        'config'=>Array(
            'type'=>'check'
        )
    ),

    'customer_serialnumber_registration' => array (
        'exclude' => 0,
        'label' => 'LLL:EXT:sonotec_customers/Resources/Private/Language/locallang_db.xlf:fe_users.customer_serialnumber_registration',
        'config' => array (
            'type' => 'input',
            'size' => 30,
            'eval' => 'trim'
        )
    ),

    'customer_device_registration' => array (
        'exclude' => 0,
        'label' => 'LLL:EXT:sonotec_customers/Resources/Private/Language/locallang_db.xlf:fe_users.customer_device_registration',
        'config' => array (
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => array(
                array('', '')
            ),
            'foreign_table' => 'tx_sonoteccustomers_domain_model_type',
            'minitems' => 1,
            'maxitems' => 1
        )
    ),

    'customer_license' => array (
        'exclude' => 0,
        'label' => 'LLL:EXT:sonotec_customers/Resources/Private/Language/locallang_db.xlf:fe_users.license',
        'config' => array (
            'type' => 'inline',
            'foreign_table' => 'tx_sonoteccustomers_domain_model_license',
            'foreign_field' => 'feuser',
            'items' => array (
                array("",0),
            ),
            'minitems'=>0,
            'maxitems'=>100,
        )
    ),

);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tempColumns, 1);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('fe_users', '--div--;LLL:EXT:sonotec_customers/Resources/Private/Language/locallang_db.xlf:fe_users.extended,module_sys_dmail_html,terms_acknowledged,customer_serialnumber_registration,customer_device_registration,customer_serialnumber,customer_license,static_info_country');

$GLOBALS['TCA']['fe_users']['ctrl']['label_alt'] = 'last_name,first_name';
$GLOBALS['TCA']['fe_users']['ctrl']['label_alt_force'] = true;
