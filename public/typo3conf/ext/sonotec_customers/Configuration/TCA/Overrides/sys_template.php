<?php
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'sonotec_customers',
    'Configuration/TypoScript',
    'Sonotec Customers'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'sonotec_customers',
    'Configuration/TypoScript/ChangePassword',
    'Sonotec Customers change password'
);
