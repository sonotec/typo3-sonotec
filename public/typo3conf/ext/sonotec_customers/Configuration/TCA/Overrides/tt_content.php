<?php

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'SonotecCustomers',
    'Serialnumber',
    'Serialnumber'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'SonotecCustomers',
    'License',
    'License'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'SonotecCustomers',
    'User',
    'User'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'SonotecCustomers',
    'Confirm',
    'Confirm'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'SonotecCustomers',
    'Contact',
    'Contact'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['sonoteccustomers_serialnumber'] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    'sonoteccustomers_serialnumber',
    'FILE:EXT:sonotec_customers/Configuration/FlexForms/Serialnumber.xml'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['sonoteccustomers_user'] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    'sonoteccustomers_user',
    'FILE:EXT:sonotec_customers/Configuration/FlexForms/User.xml'
);
