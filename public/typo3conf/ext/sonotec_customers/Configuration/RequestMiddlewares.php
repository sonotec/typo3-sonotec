<?php
return [
    'frontend' => [
        'sonotec-customers-download/file' => [
            'target' => \Sonotec\SonotecCustomers\Middleware\DownloadService::class,
            'before' => [
                'typo3/cms-frontend/page-resolver',
            ],
            'after' => [
                'typo3/cms-frontend/authentication'
            ],
        ]
    ]
];
