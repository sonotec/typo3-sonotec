<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_sonoteccustomers_domain_model_type', 'EXT:sonotec_customers/Resources/Private/Language/locallang_csh_tx_sonoteccustomers_domain_model_type.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_sonoteccustomers_domain_model_type');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_sonoteccustomers_domain_model_serialnumber', 'EXT:sonotec_customers/Resources/Private/Language/locallang_csh_tx_sonoteccustomers_domain_model_serialnumber.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_sonoteccustomers_domain_model_serialnumber');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_sonoteccustomers_domain_model_license', 'EXT:sonotec_customers/Resources/Private/Language/locallang_csh_tx_sonoteccustomers_domain_model_license.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_sonoteccustomers_domain_model_license');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_sonoteccustomers_domain_model_mail', 'EXT:sonotec_customers/Resources/Private/Language/locallang_csh_tx_sonoteccustomers_domain_model_mail.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_sonoteccustomers_domain_model_mail');
    }
);

if (TYPO3_MODE === 'BE') {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'Sonotec.SonotecCustomers',
        'web',          // Main area
        'releasenotification',         // Name of the module
        '',             // Position of the module
        array(          // Allowed controller action combinations
            'Backend' => 'index,overview,send',
        ),
        array(          // Additional configuration
            'access' => 'user,group',
            'icon' => 'EXT:sonotec_customers/Resources/Public/Icons/be-module.svg',
            'labels' => 'Release notification'
        )
    );
}
