<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
	{

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'SonotecCustomers',
            'Serialnumber',
            [
                \Sonotec\SonotecCustomers\Controller\SerialnumberController::class => 'list,new,create,delete'
            ],
            // non-cacheable actions
            [
                \Sonotec\SonotecCustomers\Controller\SerialnumberController::class => 'list,new,create,delete'
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'SonotecCustomers',
            'License',
            [
                \Sonotec\SonotecCustomers\Controller\LicenseController::class => 'list'
            ],
            // non-cacheable actions
            [
                \Sonotec\SonotecCustomers\Controller\LicenseController::class  => 'list'
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'SonotecCustomers',
            'User',
            [
                \Sonotec\SonotecCustomers\Controller\UserController::class => 'profile,delete'
            ],
            // non-cacheable actions
            [
                \Sonotec\SonotecCustomers\Controller\UserController::class => 'profile,delete'
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'SonotecCustomers',
            'Confirm',
            [
                \Sonotec\SonotecCustomers\Controller\ConfirmController::class => 'index'
            ],
            // non-cacheable actions
            [
                \Sonotec\SonotecCustomers\Controller\ConfirmController::class => 'index'
            ]
        );


        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'SonotecCustomers',
            'Contact',
            [
                \Sonotec\SonotecCustomers\Controller\ContactController::class => 'show'
            ],
            // non-cacheable actions
            [
                \Sonotec\SonotecCustomers\Controller\ContactController::class => 'show'
            ]
        );

	// wizards
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(

		'
		<INCLUDE_TYPOSCRIPT: source="DIR:EXT:sonotec_customers/Configuration/PageTS/" >
		mod {
			wizards.newContentElement.wizardItems.plugins {
				elements {
					serialnumber {
						icon = ' . 'EXT:sonotec_customers/Resources/Public/Icons/user_plugin_serialnumber.svg
						title = LLL:EXT:sonotec_customers/Resources/Private/Language/locallang_db.xlf:tx_sonotec_customers_domain_model_serialnumber
						description = LLL:EXT:sonotec_customers/Resources/Private/Language/locallang_db.xlf:tx_sonotec_customers_domain_model_serialnumber.description
						tt_content_defValues {
							CType = list
							list_type = sonoteccustomers_serialnumber
						}
					}
					license {
						icon = ' . 'EXT:sonotec_customers/Resources/Public/Icons/user_plugin_license.svg
						title = LLL:EXT:sonotec_customers/Resources/Private/Language/locallang_db.xlf:tx_sonotec_customers_domain_model_license
						description = LLL:EXT:sonotec_customers/Resources/Private/Language/locallang_db.xlf:tx_sonotec_customers_domain_model_license.description
						tt_content_defValues {
							CType = list
							list_type = sonoteccustomers_license
						}
					}

					user {
						icon = ' . 'EXT:sonotec_customers/Resources/Public/Icons/user_plugin_license.svg
						title = User profile
						description = Display user data
						tt_content_defValues {
							CType = list
							list_type = sonoteccustomers_user
						}
					}

                                        confirm {
						icon = ' . 'EXT:sonotec_customers/Resources/Public/Icons/user_plugin_license.svg
						title = Confirm newsletter
						description = Confirm newsletter after registration
						tt_content_defValues {
							CType = list
							list_type = sonoteccustomers_confirm
						}
					}

					contact {
						icon = ' . 'EXT:sonotec_customers/Resources/Public/Icons/user_plugin_license.svg
						title = User contact
						description = Display user\'s conactsperson
						tt_content_defValues {
							CType = list
							list_type = sonoteccustomers_contact
						}
					}
				}
				show = *
			}
	   }'
	);
    }
);



//xclass for PHP error thrown because of extended user model
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['TYPO3\\CMS\\Extbase\\Mvc\\Controller\\Argument'] = ['className' => 'Sonotec\\SonotecCustomers\\Xclass\\Extbase\\Mvc\\Controller\\Argument'];

//override femanager extension settings
$GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['femanager']['setCookieOnLogin'] = 1;

//addtional german language file for fe_manager
$GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['de']['EXT:femanager/Resources/Private/Language/locallang.xlf'][] = 'EXT:sonotec_customers/Resources/Private/Language/de.locallang.xlf';

//Override User model
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][In2code\Femanager\Domain\Model\User::class] = [
    'className' => \Sonotec\SonotecCustomers\Domain\Model\User::class,
];
\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class)
    ->registerImplementation(
        \In2code\Femanager\Domain\Model\User::class,
        \Sonotec\SonotecCustomers\Domain\Model\User::class
);

//Override NewController
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][In2code\Femanager\Controller\NewController::class] = [
    'className' => \Sonotec\SonotecCustomers\Controller\NewController::class,
];
\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class)
    ->registerImplementation(
        \In2code\Femanager\Controller\NewController::class,
        \Sonotec\SonotecCustomers\Controller\NewController::class
    );

//Override EditController
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][In2code\Femanager\Controller\EditController::class] = [
    'className' => \Sonotec\SonotecCustomers\Controller\EditController::class,
];
\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class)
    ->registerImplementation(
        \In2code\Femanager\Controller\EditController::class,
        \Sonotec\SonotecCustomers\Controller\EditController::class
    );

//Validation for serialnumber
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][In2code\Femanager\Domain\Validator\ServersideValidator::class] = [
    'className' => \Sonotec\SonotecCustomers\Domain\Validator\ServersideValidator::class,
];

\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class)
    ->registerImplementation(
        \In2code\Femanager\Domain\Validator\ServersideValidator::class,
        \Sonotec\SonotecCustomers\Domain\Validator\ServersideValidator::class
    );


//Scheduler task for serial number import
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['Sonotec\SonotecCustomers\Task\ImportTask'] = array(
        'extension' => 'sonotec_customers',
        'title' => 'Import serial numbers',
        'description' => 'Import task'
);


