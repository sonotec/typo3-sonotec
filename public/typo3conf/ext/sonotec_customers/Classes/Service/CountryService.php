<?php
namespace Sonotec\SonotecCustomers\Service;

/* *************************************************************
 *  Copyright notice
 *
 *  (c) 2017 Felix Franz <email@felixrranz.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 * ************************************************************* */

use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class to provide country related functions
 */
class CountryService
{
    /**
     * Render HTML options of available countries
     *
     */
    static public function renderCountryListOptions()
    {
        $languageAspect = GeneralUtility::makeInstance(Context::class)->getAspect('language');

        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_sonotecmap_domain_model_country');
        $resultRows = $queryBuilder
            ->select('uid','title','l10n_parent')
            ->from('tx_sonotecmap_domain_model_country')
            ->where(
                $queryBuilder->expr()->eq('sys_language_uid', $languageAspect->getId())
            )
            ->execute()->fetchAll();


        $countryList = [];

        foreach($resultRows as $country) {
            $uid = $country['l10n_parent'] > 0 ? $country['l10n_parent'] :  $country['uid'];
            $countryList[$uid] = $country['title'];
        }

        return CountryService::ArraySort($countryList);
    }

   static public function ArraySort($tArray) {
        $aOriginal = $tArray;
        if (count($aOriginal) == 0) { return $aOriginal; }
        $aModified = array();
        $aReturn   = array();
        $aSearch   = array("Ä","ä","Ö","ö","Ü","ü","ß","-");
        $aReplace  = array("Ae","ae","Oe","oe","Ue","ue","ss"," ");
        foreach($aOriginal as $key => $val) {
            $aModified[$key] = str_replace($aSearch, $aReplace, $val);
        }
        natcasesort($aModified);
        foreach($aModified as $key => $val) {
            $aReturn[$key] = $aOriginal[$key];
        }
        return $aReturn;
    }
}
