<?php
namespace Sonotec\SonotecCustomers\Service;

/* *************************************************************
 *  Copyright notice
 *
 *  (c) 2017 Felix Franz <email@felixrranz.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 * ************************************************************* */

use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class to provide country related functions
 */
class DeviceService
{
    /**
     * Render HTML options of available countries
     *
     */
    static public function renderDeviceList()
    {

        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_sonoteccustomers_domain_model_type');
        $resultRows = $queryBuilder
            ->select('uid','title')
            ->from('tx_sonoteccustomers_domain_model_type')
            ->execute()->fetchAll();

        $devices = [];

        if(is_array($resultRows)) {
            foreach ($resultRows as $row) {
                $devices[$row['uid']] = $row['title'];
            }
        }

        return $devices;
    }
}
