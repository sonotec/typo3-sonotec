<?php
namespace Sonotec\SonotecCustomers\Task;

use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Object\ObjectManager;

class ImportTask extends \TYPO3\CMS\Scheduler\Task\AbstractTask {

    /**
     * settings
     * @var array
     */
    private $settings;

    public function execute() {
            echo '<br><br><br/><br><hr/>';


        $settings = GeneralUtility::makeInstance(ObjectManager::class)
                ->get(ConfigurationManagerInterface::class)
                ->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT)['plugin.']['tx_sonoteccustomers.']['settings.'] ?? [];

        //fetch csv file
        $file = Environment::getPublicPath().'/'.$settings['importSerialnumbersFile'];
        $test = [];
        $test2 = [];
        if(file_exists($file)) {

            if (($handle = fopen($file, "r")) !== FALSE) {

                //get devices mapping
                $devices = $settings['import.']['devices.'];


                $countImportedItems = 0;

                while (($data = fgetcsv($handle, 1000, "\t")) !== FALSE) {
                    if(is_numeric(trim($data[1]))) {



                        $serialNumber = trim($data[1]);
                        $deviceKey = substr(trim($data[0]), 0,3);
                        $deviceKeyFull = str_replace(' ', '',trim($data[0]));
                        $currentDevice = !empty($devices[$deviceKeyFull]) ? trim($devices[$deviceKeyFull]) : (!empty($devices[$deviceKey]) ? $devices[$deviceKey] : 0);
                        $where = 'deleted=0 AND serialnumber='.$serialNumber.' AND type='.$currentDevice;

                        $check = false;
                        if($currentDevice) {
                            $check = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                                'uid', 'tx_sonoteccustomers_domain_model_serialnumber', $where
                            );
                        }

                        if(!is_array($check)) {
                            $test2['import'] = $test2['import'] ? $test2['import'] + 1 : 1;

                            $GLOBALS['TYPO3_DB']->exec_INSERTquery(
                                'tx_sonoteccustomers_domain_model_serialnumber',
                                array(
                                    //@todo constants / settings --> type, pid
                                    'pid' => 410,
                                    'type' => trim($currentDevice),
                                    'serialnumber' => $serialNumber,
                                    'crdate' => time(),
                                    'tstamp' => time()
                                )
                            );
                        }
                        else {

                            $test2['update'] = $test2['update'] ? $test2['update'] + 1 : 1;

                            $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
                                'tx_sonoteccustomers_domain_model_serialnumber',
                                $where,
                                array(
                                    //Update device
                                    'type' => trim($currentDevice),
                                    'tstamp' => time()
                                )
                            );
                        }

                    }
                }

                #echo $countImportedItems;

                fclose($handle);
            }

            print_r($test);
            print_r($test2);
        }

        return TRUE;
    }
}
