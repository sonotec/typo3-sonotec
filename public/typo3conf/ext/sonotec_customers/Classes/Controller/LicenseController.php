<?php
namespace Sonotec\SonotecCustomers\Controller;

/***
 *
 * This file is part of the "Sonotec Customers" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Felix Franz <email@felixfranz.de>
 *
 ***/

/**
 * LicenseController
 */
class LicenseController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * licenseRepository
     *
     * @var \Sonotec\SonotecCustomers\Domain\Repository\LicenseRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $licenseRepository = null;

    public function initializeAction() {
        $user = new \Sonotec\SonotecCustomers\Domain\Model\User();
        $this->user = $user->getUser();
    }
    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $licenses = '';
        if($this->user['is_online']) {

            if($this->user['customer_license']) {
                $licenses = $this->licenseRepository->findByFeuser($this->user['uid']);
            }
        }
        $this->view->assign('licenses', $licenses);
        $this->view->assign('user', $this->user);
    }
}
