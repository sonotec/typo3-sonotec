<?php
namespace Sonotec\SonotecCustomers\Controller;

/***
 *
 * This file is part of the "Sonotec Customers" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Felix Franz <email@felixfranz.de>
 *
 ***/

/**
 * UserController
 */
class UserController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    public function initializeAction() {
        $user = new \Sonotec\SonotecCustomers\Domain\Model\User();
        $this->user = $user->getUser();
        $this->user['static_info_country'] = $user->getCountry();
    }

    /**
     * action profile
     *
     * @return void
     */
    public function profileAction()
    {
        $this->view->assign('user',$this->user);
    }


    /**
     * action delete
     *
     * @return void
     */
    public function deleteAction()
    {
        $arguments = $this->request->getArguments();
        $this->view->assign('user',$this->user);


        if($this->user['is_online'] && $arguments['delete']==1) {

            //remove serialnumbers from current user
            $serialNumbers = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                '*', 'tx_sonoteccustomers_domain_model_serialnumber', 'deleted=0 AND hidden=0 AND feuser='.$GLOBALS['TSFE']->fe_user->user['uid']
            );

            if($serialNumbers) {
                foreach ($serialNumbers as $serialNumber) {
                    $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
                        'tx_sonoteccustomers_domain_model_serialnumber',
                        'deleted=0 AND hidden=0 AND uid='.$serialNumber['uid'],
                        ['feuser'=>'']
                    );
                }
            }

            //delete user
            $GLOBALS['TYPO3_DB']->exec_DELETEquery(
                'fe_users',
                'deleted=0 AND disable=0 AND uid='.$GLOBALS['TSFE']->fe_user->user['uid']
            );

            $uriBuilder = $this->uriBuilder;
            $uri = $uriBuilder->setTargetPageUid($this->settings['login'])->build();
            $this->redirectToUri($uri, 0, 404);
        }
    }

    /**
     * action confirm newsletter
     *
     * @return void
     */
    public function newsletterconfirmAction()
    {
        $this->view->assign('user',$this->user);
    }
}
