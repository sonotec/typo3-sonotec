<?php
namespace Sonotec\SonotecCustomers\Controller;

/***
 *
 * This file is part of the "Sonotec Customers" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Felix Franz <email@felixfranz.de>
 *
 ***/

use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * ConfirmController
 */
class ConfirmController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    
    /**
     * action index
     *
     * @return void
     */
    public function indexAction()
    {
        $userHash = strip_tags(trim(GeneralUtility::_GET('newsletterConfirm')));
        $response = 'Invalid hash';
        
        if( ctype_alnum($userHash) && strlen($userHash)==32 ) {
            
            $feUser = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                'uid', 'fe_users', 
                'deleted=0 AND disable=0 AND comments="'.$userHash.'"'
            );
            
            if($feUser['uid']) {
                $res = $GLOBALS['TYPO3_DB']->exec_UPDATEquery( 
                    'fe_users', 'uid='.$feUser['uid'], 
                    ['module_sys_dmail_html'=>1]    
                );
                $response = $res ? 'Confirmation successful.' : 'Unexpected error. Please reload your browser.';
            }
            else {
                $response = 'No user found.  --> Please register';
            }
        }
        
        $this->view->assign('response',$response);
    }
}
