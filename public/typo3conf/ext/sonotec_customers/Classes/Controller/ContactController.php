<?php
namespace Sonotec\SonotecCustomers\Controller;

/***
 *
 * This file is part of the "Sonotec Customers" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Felix Franz <email@felixfranz.de>
 *
 ***/

/**
 * ContactController
 */
class ContactController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    
    public function initializeAction() {
        $user = new \Sonotec\SonotecCustomers\Domain\Model\User();
        $this->address = $user->getAddress($this->settings['contact']['default']);
    }
    
    /**
     * action show
     *
     * @return void
     */
    public function showAction()
    {
        $this->view->assign('address',$this->address);
    }
}
