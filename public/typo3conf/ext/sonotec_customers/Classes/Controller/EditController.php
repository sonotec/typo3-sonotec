<?php
namespace Sonotec\SonotecCustomers\Controller;

use Sonotec\SonotecCustomers\Domain\Model\User;
use Sonotec\SonotecCustomers\Service\CountryService;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class EditController extends \In2code\Femanager\Controller\EditController {

    /**
     * @return void
     */
    public function editAction()
    {
        $this->view->assign('countries', CountryService::renderCountryListOptions());
        parent::editAction();
    }

    /**
     * action update
     *
     * @param User|\In2code\Femanager\Domain\Model\User $user
     * @TYPO3\CMS\Extbase\Annotation\Validate("In2code\Femanager\Domain\Validator\ServersideValidator", param="user")
     * @TYPO3\CMS\Extbase\Annotation\Validate("In2code\Femanager\Domain\Validator\PasswordValidator", param="user")
     * @TYPO3\CMS\Extbase\Annotation\Validate("In2code\Femanager\Domain\Validator\CaptchaValidator", param="user")
     * @return void
     */
    public function updateAction(\In2code\Femanager\Domain\Model\User $user) {
        $this->view->assign('countries', CountryService::renderCountryListOptions());
        parent::updateAction($user);
    }
}
