<?php
namespace Sonotec\SonotecCustomers\Controller;

use Sonotec\SonotecCustomers\Domain\Model\User;
use Sonotec\SonotecCustomers\Service\CountryService;
use Sonotec\SonotecCustomers\Service\DeviceService;

class NewController extends \In2code\Femanager\Controller\NewController {

    /**
     * Render registration form
     *
     * @param User|\In2code\Femanager\Domain\Model\User $user
     * @return void
     */
    public function newAction(\In2code\Femanager\Domain\Model\User $user = null)
    {
        $this->view->assign('currentUser', $this->user);
        $this->view->assign('countries', CountryService::renderCountryListOptions());
        $this->view->assign('devices', DeviceService::renderDeviceList());

        parent::newAction($user);
    }

    /**
     * action create
     *
     * @param User|\In2code\Femanager\Domain\Model\User $user
     *
     * @TYPO3\CMS\Extbase\Annotation\Validate("In2code\Femanager\Domain\Validator\ServersideValidator", param="user")
     * @TYPO3\CMS\Extbase\Annotation\Validate("In2code\Femanager\Domain\Validator\PasswordValidator", param="user")
     * @TYPO3\CMS\Extbase\Annotation\Validate("In2code\Femanager\Domain\Validator\CaptchaValidator", param="user")
     * @return void
     */
    public function createAction(\In2code\Femanager\Domain\Model\User $user) {
        parent::createAction($user);
    }
}
