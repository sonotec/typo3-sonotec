<?php
namespace Sonotec\SonotecCustomers\Controller;

/***
 *
 * This file is part of the "Sonotec Customers" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Felix Franz <email@felixfranz.de>
 *
 ***/

use Sonotec\SonotecCustomers\Service\DeviceService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

/**
 * SerialnumberController
 */
class SerialnumberController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var PersistenceManager
     */
    protected $persistenceManager;

    /**
     * serialnumberRepository
     *
     * @var \Sonotec\SonotecCustomers\Domain\Repository\SerialnumberRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $serialnumberRepository = null;

    /**
     * licenseRepository
     *
     * @var \Sonotec\SonotecCustomers\Domain\Repository\LicenseRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $licenseRepository = null;

    /**
     * typeRepository
     *
     * @var \Sonotec\SonotecCustomers\Domain\Repository\TypeRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $typeRepository = null;



    public function initializeAction() {
        $user = new \Sonotec\SonotecCustomers\Domain\Model\User();
        $this->user = $user->getUser();

        $this->objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        $this->persistenceManager = $this->objectManager->get(PersistenceManager::class);
    }


    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        if($this->user['is_online']) {

            if($this->user['customer_serialnumber_registration']) {

                //@todo: bereits vergebene Seriennummern ausschließen!
                $serialnumber = $this->serialnumberRepository->checkSerialNumber($this->user['customer_serialnumber_registration'],$this->user['customer_device_registration']);
                if($serialnumber && !$serialnumber->getFeuser()) {
                    $serialnumber->setFeuser($this->user['uid']);
                    $this->serialnumberRepository->update($serialnumber);
                    $this->persistenceManager->persistAll();

                    $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
                        'fe_users',
                        'deleted=0 AND disable=0 AND uid='.$this->user['uid'],
                        ['customer_serialnumber_registration'=>'']
                    );
                }
            }

            $serialnumbers = $this->serialnumberRepository->findByFeuser($this->user['uid']);
        }

        $downloads = [];
        foreach ($serialnumbers as $serialnumber) {
            $downloads[$serialnumber->getType()->getUid()] = $serialnumber;
        }

        $this->view->assignMultiple([
            'data' => $this->configurationManager->getContentObject()->data,
            'serialnumbers' => $serialnumbers,
            'downloads' => $downloads,
            'user' => $this->user
        ]);
    }

    /**
     * action new
     *
     * @param \Sonotec\SonotecCustomers\Domain\Model\Serialnumber $newSerialnumber
     * @return void
     */
    public function newAction(\Sonotec\SonotecCustomers\Domain\Model\Serialnumber $newSerialnumber = null)
    {
        if($this->user['is_online']) {
            $serialNumber = '';

            if(NULL !== $newSerialnumber) {
                $serialNumber = $this->checkSerialNumber($newSerialnumber->getSerialnumber(), $newSerialnumber->getType()->getUid());

                if($serialNumber) {
                    $this->addFlashMessage(str_replace('###number###',$newSerialnumber->getSerialnumber(),\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate( 'new.number_added', 'sonotec_customers' ) ), '',  \TYPO3\CMS\Core\Messaging\AbstractMessage::OK);
                    $serialNumber->setFeuser($this->user['uid']);
                    $this->serialnumberRepository->update($serialNumber);
                }
                else {
                    switch($newSerialnumber->getSerialnumber()) {
                        case strlen($newSerialnumber->getSerialnumber()) > $this->settings['serialnumberLength']:
                            $this->addFlashMessage(str_replace('###number###',$this->settings['serialnumberLength'],\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate( 'new.too_long', 'sonotec_customers' ) ), '',  \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
                            break;
                        case strlen($newSerialnumber->getSerialnumber()) < $this->settings['serialnumberMinLength']:
                            $this->addFlashMessage(str_replace('###number###',$this->settings['serialnumberLength'],\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate( 'new.too_short', 'sonotec_customers' ) ), '',  \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
                            break;
                        default:
                            $this->addFlashMessage(\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate( 'new.does_not_exist', 'sonotec_customers' ), '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
                            break;
                    }
                }
            }

            $this->view->assignMultiple([
                'devices' => DeviceService::renderDeviceList(),
                'newSerialnumber' => $newSerialnumber,
                'added' => $serialNumber ? TRUE : '',
            ]);
        }



    }

    public function checkSerialNumber($serialnumber,$type) {
        $result = $this->serialnumberRepository->checkSerialNumber($serialnumber,$type);
        return $result;
    }

    /**
     * action delete
     *
     * @param \Sonotec\SonotecCustomers\Domain\Model\Serialnumber $serialnumber
     * @return void
     */
    public function deleteAction(\Sonotec\SonotecCustomers\Domain\Model\Serialnumber $serialnumber = null)
    {
        $deleted = '';

        if($this->user['is_online']) {
            $arguments = $this->request->getArguments();
            if(NULL !== $serialnumber && $serialnumber->getFeuser() == $this->user['uid']) {
                if($arguments['delete']==1) {
                    $this->addFlashMessage(str_replace('###number###',$serialnumber->getSerialnumber(),\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate( 'deleted.message_deleted', 'sonotec_customers' ) ), '',  \TYPO3\CMS\Core\Messaging\AbstractMessage::OK);
                    $serialnumber->setFeuser('');
                    $this->serialnumberRepository->update($serialnumber);
                    $deleted = TRUE;
                }
                else {
                    $this->addFlashMessage(str_replace('###number###',$serialnumber->getSerialnumber(),\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate( 'delete.message_will_delete', 'sonotec_customers' ) ), '',  \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
                }


            }
            else {
                $uriBuilder = $this->uriBuilder;
                $uri = $uriBuilder->setTargetPageUid($this->settings['overviewUid'])->build();
                $this->redirectToUri($uri, 0, 404);
            }

            $this->view->assignMultiple([
                'serialnumber' => $serialnumber,
                'deleted' => $deleted,
            ]);
        }
    }



    /**
     * action downloadbutton
     *
     * @return void
     */
    public function downloadbuttonAction()
    {
        if($this->user['is_online']) {
            $serialnumber = $this->serialnumberRepository->findByFeuser($this->user['uid']);

            $this->view->assignMultiple([
                'serialnumber' => $serialnumber,
                'user' => $this->user
            ]);
        }

    }
}
