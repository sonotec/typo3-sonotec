<?php
namespace Sonotec\SonotecCustomers\Controller;

/***
 *
 * This file is part of the "Sonotec Customers" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Felix Franz <email@felixfranz.de>
 *
 ***/

/**
 * BackendController
 */
class BackendController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
   /**
     * serialnumberRepository
     *
     * @var \Sonotec\SonotecCustomers\Domain\Repository\SerialnumberRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $serialnumberRepository = null;

    /**
     * typeRepository
     *
     * @var \Sonotec\SonotecCustomers\Domain\Repository\TypeRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $typeRepository = null;

     /**
     * mailRepository
     *
     * @var \Sonotec\SonotecCustomers\Domain\Repository\MailRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $mailRepository = null;


    /**
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $configurationManager;


    public function initializeAction() {
        $this->configuration = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
    }

    /**
     * action index
     *
     * @return void
     */
    public function indexAction()
    {
        $this->view->assignMultiple([
            'mails' => $this->mailRepository->findAll(),
            'URL' => str_replace('; return false;','', \TYPO3\CMS\Backend\Utility\BackendUtility::editOnClick(''))
        ]);
    }

    /**
     * action overview
     *
     * @return void
     */
    public function overviewAction()
    {
        $arguments = $this->request->getArguments();
        $mail = $this->mailRepository->findOneByUid($arguments['mail']);
        $url = $this->settings['email']['domains'][$mail->getLang()].'index.php?id='.$this->settings['login'].'&type=6677&mail='.$mail->getUid();
        $message = \TYPO3\CMS\Core\Utility\GeneralUtility::getURL($url);
        $this->view->assignMultiple([
            'mail' => $mail,
            'message' => $message,
        ]);
    }

    /**
     * action send
     *
     * @return void
     */
    public function sendAction()
    {
        $arguments = $this->request->getArguments();
        $mail = $this->mailRepository->findOneByUid($arguments['mail']);

        if($mail) {
            //mailingtype: newsletter or normal software update mailing
            $addWhere = $mail->getMailingtype()==2 ? ' AND module_sys_dmail_html' : '';

            //language: germany, austria, switzerland OR rest of the world
            $addWhere.= $mail->getLang()==1 ? ' AND static_info_country IN('.$this->settings['email']['german'].')' : ' AND static_info_country NOT IN('.$this->settings['email']['german'].')';

            $feUsers = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                'email', 'fe_users',
                'deleted=0 AND disable=0  AND usergroup='.$this->settings['usergroup'].$addWhere
            );

            //prepare receivers
            $receivers = [];
            foreach ($feUsers as $feUser) {
                $validator = new \TYPO3\CMS\Extbase\Validation\Validator\EmailAddressValidator();
                if( !sizeof($validator->validate($feUser['email'])->getErrors()) ) {
                    $receivers[] = $feUser['email'];
                }
            }

            //message
            $mail = $this->mailRepository->findOneByUid($arguments['mail']);
            $message = \TYPO3\CMS\Core\Utility\GeneralUtility::getURL($this->settings['email']['domains'][$mail->getLang()].'index.php?id='.$this->settings['login'].'&type=6677&mail='.$mail->getUid());

            //attachments
            $attachments = [];
            foreach($mail->getAttachment() as $attachment) {
                $attachments[] = \TYPO3\CMS\Core\Core\Environment::getPublicPath().'/fileadmin'.$attachment->getOriginalResource()->getOriginalFile()->getIdentifier();
            }

            #####TESTING######
            #$receivers = [];
            #$receivers[] = 'felixfranz82@googlemail.com';
            #$receivers[] = 'email@felixfranz.de';
            #####TESTING######

            //send mail
            foreach($receivers as $receiver) {
                $mailSent = \Sonotec\SonotecCustomers\Utility\MailUtility::sendMail(
                    $receiver,
                    $this->settings['email']['from'],
                    $this->settings['email']['fromName'],
                    $mail->getSubject(),
                    $message,
                    $attachments,
                    $this->settings['email']['replyTo']
                );
            }
            if($mailSent) {
                $this->addFlashMessage(\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate( 'send_message', 'sonotec_customers' ) , '',  \TYPO3\CMS\Core\Messaging\AbstractMessage::OK);
            }
        }
        else {
            $this->addFlashMessage('Error fetching mail' , '',  \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
            $this->redirect('index');
        }

    }


    public function fluidStandAlone($message) {

        $standaloneView = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Fluid\View\StandaloneView::class);
        $standaloneView->setLayoutRootPaths([
            \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName('EXT:sonotec_customers/Resources/Private/Layouts'),
        ]);
        $standaloneView->setPartialRootPaths([
            \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName('EXT:sonotec_customers/Resources/Private/Partials'),
        ]);
        $standaloneView->setTemplateRootPaths([
            \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName('EXT:sonotec_customers/Resources/Private/Templates'),
        ]);
        $standaloneView->setFormat('html');
        $standaloneView->setTemplate('Mail/Mail');
        $standaloneView->assignMultiple([
            'message' => $message
        ]);
        return $standaloneView->render();
    }
}
