<?php

declare(strict_types=1);

namespace Sonotec\SonotecCustomers\Middleware;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Context\Context;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TYPO3\CMS\Core\Http\Response;
use TYPO3\CMS\Core\Http\Stream;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Object\ObjectManager;

class DownloadService implements MiddlewareInterface
{
    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $normalizedParams = $request->getAttribute('normalizedParams');
        $uri = $normalizedParams->getRequestUri();

        if (strpos($uri, '/sonotec-customers-download/')) {
            $context = GeneralUtility::makeInstance(Context::class);
            $userAspect = $context->getAspect('frontend.user');

            $fileUid = GeneralUtility::_GP('file');


            $resourceFactory = GeneralUtility::makeInstance(ResourceFactory::class);
            $file = $resourceFactory->getFileObject($fileUid);

            if (($file &&  $userAspect->isLoggedIn()) || ($file && !strpos($file->getIdentifier(),'user_upload/mysonaphone/'))) {
                $filename = $file->getName();
                $file_url = $_SERVER['DOCUMENT_ROOT'].'/fileadmin'.$file->getIdentifier();
                $filesize = $file->getSize();
                $fileExtension = $file->getExtension();

                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.$filename.'"');
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
                header('Content-Length: ' . $filesize); //Absolute URL
               # ob_clean();
               # flush();
               # readfile($file_url);

                $chunkSize = 1024 * 1024;
                $handle = fopen($file_url, 'rb');
                while (!feof($handle))
                {
                    $buffer = fread($handle, $chunkSize);
                    echo $buffer;
                    ob_flush();
                    flush();
                }
                fclose($handle);



                exit();
            }
        }




        return $handler->handle($request);
    }

}
