<?php
namespace Sonotec\SonotecCustomers\Utility;

/**
 * Class MailUtility
 *
 * @package Sonotec\SonotecCustomers\Utility
 */

use Symfony\Component\Mime\Address;
use TYPO3\CMS\Core\Mail\MailMessage;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class MailUtility {

    /**
     * Send a html mail
     *
     * @param string $receivers Email address to send to
     * @param string $sender Email address from sender
     * @param string $senderName Email address to send to
     * @param string $subject Subject line
     * @param string $message Message content
     * @param array $attachments attachments
     * @param string $replyTo replyTo
     * @return bool mail was sent?
     */
    public static function sendMail(string $receiver, string $sender,string $senderName,string $subject, string $message,array $attachments,string $replyTo) {

        $mail = GeneralUtility::makeInstance(MailMessage::class);

        $mail->subject($subject)
             ->to(new Address($receiver,''))
             ->from(new Address($sender,$senderName))
             ->replyTo(new Address($replyTo,$senderName))
             ->html($message);

        if(sizeof($attachments)>0){
            foreach($attachments as $attachment) {
                $mail->attachFromPath($attachment);
            }
        }

        $mail->send();
        return $mail->isSent();

    }
}
