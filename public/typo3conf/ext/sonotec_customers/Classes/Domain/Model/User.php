<?php

namespace Sonotec\SonotecCustomers\Domain\Model;


use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/***
 *
 * This file is part of the "Sonotec Customers" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Felix Franz <email@felixfranz.de>
 *
 ***/

/**
 * User
 */
class User extends \In2code\Femanager\Domain\Model\User
{
    /**
     * user
     *
     * @var string
     */
    protected $user = '';

    /**
     * Returns the user
     *
     * @return string $user
     */
    public function getUser()
    {
        if($GLOBALS['TSFE']->fe_user->user['is_online']) {
            #$GLOBALS['TSFE']->fe_user->user['static_info_country'] = $this->getCountry();
            return $GLOBALS['TSFE']->fe_user->user;
        }
        else {
            return '';
        }
    }

    /**
     * country
     *
     * @var string
     */
    protected $country = '';

    /**
     * Returns the country
     *
     * @return string $country
     */
    public function getCountry()
    {
        $country = '';


        if($GLOBALS['TSFE']->fe_user->user['is_online'] && $GLOBALS['TSFE']->fe_user->user['static_info_country']) {

            $languageAspect = GeneralUtility::makeInstance(Context::class)->getAspect('language');

            $countryRow = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                    '*', 'tx_sonotecmap_domain_model_country',
                    'uid='.$GLOBALS['TSFE']->fe_user->user['static_info_country'],
                    '', '', ''
                );

            if($languageAspect->getId()>0) {
               $countryRow = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                    '*', 'tx_sonotecmap_domain_model_country',
                    'sys_language_uid='.$languageAspect->getId().' AND l10n_parent='.$GLOBALS['TSFE']->fe_user->user['static_info_country'],
                    '', '', ''
                );
            }
            $country = $countryRow['title'] ? $countryRow['title'] : '';
        }
        return $country;
    }

     /**
     * address
     *
     * @var array
     */
    protected $address = '';

    /**
     * Returns the address
     *
     * @return array $address
     */
    public function getAddress($defaultContact='')
    {

        $address = [];

        if($GLOBALS['TSFE']->fe_user->user['is_online']) {
            $defaultWhere = 'uid='.$defaultContact;
            $where = $GLOBALS['TSFE']->fe_user->user['static_info_country'] ? 'country='.$GLOBALS['TSFE']->fe_user->user['static_info_country'] : $defaultWhere;

            $address = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                '*', 'tx_sonotecmap_domain_model_address',
                'deleted=0 AND hidden=0 AND '.$where
            );

            if(!$address) {
                $address = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                    '*', 'tx_sonotecmap_domain_model_address',
                    'deleted=0 AND hidden=0 AND '.$defaultWhere
                );
            }
        }

        return $address;
    }



    /**
     * customerSerialnumberRegistration
     *
     * @var string
     */
    protected $customerSerialnumberRegistration;

    /**
     * Returns the customerSerialnumberRegistration
     *
     * @return string $customerSerialnumberRegistration
     */
    public function getCustomerSerialnumberRegistration() {
        return $this->customerSerialnumberRegistration;
    }

    /**
     * Sets the customerSerialnumberRegistration
     *
     * @param string $customerSerialnumberRegistration
     * @return void
     */
    public function setCustomerSerialnumberRegistration($customerSerialnumberRegistration) {
        $this->customerSerialnumberRegistration = $customerSerialnumberRegistration;
    }


    /**
     * customerDeviceRegistration
     *
     * @var string
     */
    protected $customerDeviceRegistration;

    /**
     * Returns the customerDeviceRegistration
     *
     * @return string $customerDeviceRegistration
     */
    public function getCustomerDeviceRegistration() {
        return $this->customerDeviceRegistration;
    }

    /**
     * Sets the customerDeviceRegistration
     *
     * @param string $customerDeviceRegistration
     * @return void
     */
    public function setCustomerDeviceRegistration($customerDeviceRegistration) {
        $this->customerDeviceRegistration = $customerDeviceRegistration;
    }

    /**
     * language
     *
     * @var string
     */
    protected $language;

    /**
     * Returns the language
     *
     * @return string $language
     */
    public function getLanguage() {
        return $this->language;
    }

    /**
     * Sets the language
     *
     * @param string $language
     * @return void
     */
    public function setLanguage($language) {
        $this->language = $language;
    }


    /**
     * moduleSysDmailHtml
     *
     * @var bool
     */
    protected $moduleSysDmailHtml;

    /**
     * Returns the moduleSysDmailHtml
     *
     * @return bool $moduleSysDmailHtml
     */
    public function getModuleSysDmailHtml()
    {
        return $this->moduleSysDmailHtml;
    }

    /**
     * Sets the moduleSysDmailHtml
     *
     * @param bool $moduleSysDmailHtml
     * @return void
     */
    public function setModuleSysDmailHtml($moduleSysDmailHtml)
    {
        $this->moduleSysDmailHtml = $moduleSysDmailHtml;
    }

    /**
     * staticInfoCountry
     *
     * @var int
     */
    protected $staticInfoCountry;

    /**
     * Returns the staticInfoCountry
     *
     * @return int $staticInfoCountry
     */
    public function getStaticInfoCountry()
    {
        return $this->staticInfoCountry;
    }

    /**
     * Sets the staticInfoCountry
     *
     * @param int $staticInfoCountry
     * @return void
     */
    public function setStaticInfoCountry($staticInfoCountry)
    {
        $this->staticInfoCountry = $staticInfoCountry;
    }
}
