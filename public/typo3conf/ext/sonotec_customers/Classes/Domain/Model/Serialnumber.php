<?php
namespace Sonotec\SonotecCustomers\Domain\Model;

/***
 *
 * This file is part of the "Sonotec Customers" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Felix Franz <email@felixfranz.de>
 *
 ***/

/**
 * Serialnumber
 */
class Serialnumber extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
     /**
     * feuser
     *
     * @var string
     */
    protected $feuser = '';

    

    /**
     * serialnumber
     *
     * @var string
     */
    protected $serialnumber = '';

    /**
     * type
     *
     * @var \Sonotec\SonotecCustomers\Domain\Model\Type
     */
    protected $type = null;

    

    /**
     * Returns the feuser
     *
     * @return string $feuser
     */
    public function getFeuser()
    {
        return $this->feuser;
    }

    /**
     * Sets the feuser
     *
     * @param string $feuser
     * @return void
     */
    public function setFeuser($feuser)
    {
        $this->feuser = $feuser;
    }

    

    /**
     * Returns the serialnumber
     *
     * @return string serialnumber
     */
    public function getSerialnumber()
    {
        return $this->serialnumber;
    }

    /**
     * Sets the serialnumber
     *
     * @param string $serialnumber
     * @return void
     */
    public function setSerialnumber($serialnumber)
    {
        $this->serialnumber = $serialnumber;
    }

    /**
     * Returns the type
     *
     * @return \Sonotec\SonotecCustomers\Domain\Model\Type $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the type
     *
     * @param \Sonotec\SonotecCustomers\Domain\Model\Type $type
     * @return void
     */
    public function setType(\Sonotec\SonotecCustomers\Domain\Model\Type $type)
    {
        $this->type = $type;
    }
}
