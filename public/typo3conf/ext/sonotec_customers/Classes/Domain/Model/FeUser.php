<?php
namespace Sonotec\SonotecCustomers\Domain\Model;

class FeUser extends \In2code\Femanager\Domain\Model\User {

    /**
     * customerSerialnumberRegistration
     *
     * @var string
     */
    protected $customerSerialnumberRegistration;

    /**
     * Returns the customerSerialnumberRegistration
     *
     * @return string $customerSerialnumberRegistration
     */
    public function getCustomerSerialnumberRegistration() {
        return $this->customerSerialnumberRegistration;
    }

    /**
     * Sets the customerSerialnumberRegistration
     *
     * @param string $customerSerialnumberRegistration
     * @return void
     */
    public function setCustomerSerialnumberRegistration($customerSerialnumberRegistration) {
        $this->customerSerialnumberRegistration = $customerSerialnumberRegistration;
    }

    /**
     * customerSerialnumberRegistration
     *
     * @var string
     */
    protected $customerDeviceRegistration;

    /**
     * Returns the customerDeviceRegistration
     *
     * @return string $customerDeviceRegistration
     */
    public function getCustomerDeviceRegistration() {
        return $this->customerDeviceRegistration;
    }

    /**
     * Sets the customerDeviceRegistration
     *
     * @param string $customerDeviceRegistration
     * @return void
     */
    public function setCustomerDeviceRegistration($customerDeviceRegistration) {
        $this->customerDeviceRegistration = $customerDeviceRegistration;
    }


    /**
     * moduleSysDmailHtml
     *
     * @var bool
     */
    protected $moduleSysDmailHtml;

    /**
     * Returns the moduleSysDmailHtml
     *
     * @return bool $moduleSysDmailHtml
     */
    public function getModuleSysDmailHtml()
    {
        return $this->moduleSysDmailHtml;
    }

    /**
     * Sets the moduleSysDmailHtml
     *
     * @param bool $moduleSysDmailHtml
     * @return void
     */
    public function setModuleSysDmailHtml($moduleSysDmailHtml)
    {
        $this->moduleSysDmailHtml = $moduleSysDmailHtml;
    }
}
