<?php
namespace Sonotec\SonotecCustomers\Domain\Model;

/***
 *
 * This file is part of the "Sonotec Customers" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Felix Franz <email@felixfranz.de>
 *
 ***/

/**
 * Mail
 */
class Mail extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * subject
     *
     * @var string
     */
    protected $subject = '';

     /**
     * mailingtype
     *
     * @var string
     */
    protected $mailingtype = '';

    /**
     * type
     *
     * @var \Sonotec\SonotecCustomers\Domain\Model\Type
     */
    protected $type = null;

    /**
     * emailtext
     *
     * @var string
     */
    protected $emailtext = '';

    /**
     * lang
     *
     * @var int
     */
    protected $lang = '';

    /**
     * attachment
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $attachment = null;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality

        $this->initStorageObjects();

    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {

        $this->attachment = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the subject
     *
     * @return string $subject
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Sets the subject
     *
     * @param string $subject
     * @return void
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * Returns the mailingtype
     *
     * @return string $mailingtype
     */
    public function getMailingtype()
    {
        return $this->mailingtype;
    }

    /**
     * Sets the mailingtype
     *
     * @param string $mailingtype
     * @return void
     */
    public function setMailingtype($mailingtype)
    {
        $this->mailingtype = $mailingtype;
    }

    /**
     * Returns the emailtext
     *
     * @return string $emailtext
     */
    public function getEmailtext()
    {
        return $this->emailtext;
    }

    /**
     * Sets the emailtext
     *
     * @param string $emailtext
     * @return void
     */
    public function setEmailtext($emailtext)
    {
        $this->emailtext = $emailtext;
    }

    /**
     * Returns the lang
     *
     * @return string $lang
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Sets the lang
     *
     * @param string $lang
     * @return void
     */
    public function setLang($lang)
    {
        $this->lang = $lang;
    }



    /**
     * Returns the type
     *
     * @return \Sonotec\SonotecCustomers\Domain\Model\Type $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the type
     *
     * @param \Sonotec\SonotecCustomers\Domain\Model\Type $type
     * @return void
     */
    public function setType(\Sonotec\SonotecCustomers\Domain\Model\Type $type)
    {
        $this->type = $type;
    }

    /**
     * Returns the attachment
     *
     * @return @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $attachment
     */
    public function getAttachment()
    {
        return $this->attachment;
    }

    /**
     * Sets the attachment
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $attachment
     * @return void
     */
    public function setAttachment(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $attachment)
    {
        $this->attachment = $attachment;
    }
}
