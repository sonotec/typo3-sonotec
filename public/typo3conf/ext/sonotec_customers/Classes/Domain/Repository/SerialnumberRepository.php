<?php
namespace Sonotec\SonotecCustomers\Domain\Repository;

/***
 *
 * This file is part of the "Sonotec Customers" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Felix Franz <email@felixfranz.de>
 *
 ***/

/**
 * The repository for Serialnumbers
 */
class SerialnumberRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{


    public function checkSerialNumber($serialnumber, $type) {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectSysLanguage(false);

        $query->matching(
            $query->logicalAnd(
                $query->equals('feuser', ''),
                $query->equals('serialnumber', $serialnumber),
                $query->equals('type', $type)
            )
        );

        return $query->execute()->getFirst();
    }

    public function getSerialNumber($serialnumber) {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectSysLanguage(false);

        $query->matching(
            $query->logicalAnd(
                $query->equals('feuser', ''),
                $query->equals('serialnumber', $serialnumber)
            )
        );

        return $query->execute()->getFirst();
    }

    public function getSerialNumbersByType($type) {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectSysLanguage(false);

        $query->matching(
            $query->logicalAnd(
                $query->greaterThan('feuser', 0),
                $query->equals('type', $type)
            )
        );

        return $query->execute();
    }


}
