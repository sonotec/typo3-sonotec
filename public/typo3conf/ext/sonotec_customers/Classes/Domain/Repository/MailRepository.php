<?php
namespace Sonotec\SonotecCustomers\Domain\Repository;

/***
 *
 * This file is part of the "Sonotec Customers" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Felix Franz <email@felixfranz.de>
 *
 ***/

/**
 * The repository for Mails
 */
class MailRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    }
