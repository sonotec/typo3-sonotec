<?php

namespace Sonotec\SonotecCustomers\Domain\Validator;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2020 SLUB Dresden <typo3@slub-dresden.de>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;

/**
 * Class ServersideValidator
 */
class ServersideValidator extends \In2code\Femanager\Domain\Validator\ServersideValidator
{
    /**
     * SerialNumber Validator
     *
     * @param string $value Given value from input field
     * @param string $validationSetting TypoScript Setting for this field
     * @return bool
     */
    protected function validateSerialNumber($value, $validationSetting)
    {
        $arguments = GeneralUtility::_GP('tx_femanager_pi1');

        if($value) {
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_sonoteccustomers_domain_model_serialnumber');
            $resultRows = $queryBuilder
                ->select('feuser','uid','serialnumber')
                ->from('tx_sonoteccustomers_domain_model_serialnumber')
                ->where(
                    $queryBuilder->expr()->eq('serialnumber', $queryBuilder->createNamedParameter($value)),
                    $queryBuilder->expr()->eq('feuser', $queryBuilder->createNamedParameter('')),
                    $queryBuilder->expr()->eq('deleted', $queryBuilder->createNamedParameter(0)),
                    $queryBuilder->expr()->eq('hidden', $queryBuilder->createNamedParameter(0)),
                    $queryBuilder->expr()->eq('type', $queryBuilder->createNamedParameter($arguments['user']['customerDeviceRegistration']))
                )
                ->execute()->fetchAll();

            if(isset($resultRows[0]['serialnumber'])) {
                return true;
            }
        }

        return false;
    }
}
