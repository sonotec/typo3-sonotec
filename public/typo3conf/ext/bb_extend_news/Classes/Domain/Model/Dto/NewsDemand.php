<?php
namespace BB\BbExtendNews\Domain\Model\Dto;

/***
 *
 * This file is part of the "Extend News" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Kay Röseler <support@bippesbrandao.de>, BippesBrandão GmbH
 *
 ***/

/**
 * Class NewsDemand
 * @package BB\BbExtendNews\Domain\Model\Dto
 */
class NewsDemand extends \GeorgRinger\News\Domain\Model\Dto\NewsDemand
{
    /**
     * @var \BB\BbExtendNews\Domain\Model\Authors
     */
    protected $newsAuthor;

    /**
     * Set news author
     *
     * @param \BB\BbExtendNews\Domain\Model\Authors $newsAuthor
     * @return NewsDemand
     */
    public function setNewsAuthor($newsAuthor)
    {
        $this->newsAuthor = $newsAuthor;
        return $this;
    }

    /**
     * Get news author
     *
     * @return \BB\BbExtendNews\Domain\Model\Authors
     */
    public function getNewsAuthor()
    {
        return $this->newsAuthor;
    }
}