<?php
namespace BB\BbExtendNews\Domain\Model;

/***
 *
 * This file is part of the "BippesBrandão - SmapOne Erweiterung News" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Kay Röseler <support@bippesbrandao.de>, BippesBrandão GmbH
 *
 ***/

/**
 * Authors
 */
class Authors extends \TYPO3\CMS\Extbase\DomainObject\AbstractValueObject
{
    /**
     * Name of author
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $name = '';

    /**
     * title of author
     *
     * @var string
     */
    protected $title = '';

    /**
     * description from the author
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $description = '';

    /**
     * image from the author
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $image = null;

    /**
     * @var int
     */
    protected $_localizedUid = 0;

    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     *
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     *
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Returns the image
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Sets the image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     */
    public function setImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image)
    {
        $this->image = $image;
    }

    /**
     * Returns the title
     *
     * @return string $localizedUid
     */
    public function getLocalizedUid()
    {
        return $this->localizedUid;
    }

    /**
     * Sets the title
     *
     * @param string $localizedUid
     * @return void
     */
    public function setLocalizedUid($localizedUid)
    {
        $this->localizedUid = $localizedUid;
    }
}
