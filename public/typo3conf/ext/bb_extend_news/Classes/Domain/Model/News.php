<?php
namespace BB\BbExtendNews\Domain\Model;

/***
 *
 * This file is part of the "Extend News" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Kay Röseler <support@bippesbrandao.de>, BippesBrandão GmbH
 *
 ***/

/**
 * News
 */
class News extends \GeorgRinger\News\Domain\Model\NewsDefault
{
    /**
     * author
     *
     * @var \BB\BbExtendNews\Domain\Model\Authors
     */
    protected $newsAuthor = null;

    /**
     * Returns the newsAuthor
     *
     * @return \BB\BbExtendNews\Domain\Model\Authors newsAuthor
     */
    public function getNewsAuthor()
    {
        return $this->newsAuthor;
    }

    /**
     * Sets the newsAuthor
     *
     * @param \BB\BbExtendNews\Domain\Model\Authors $newsAuthor
     * @return void
     */
    public function setNewsAuthor(\BB\BbExtendNews\Domain\Model\Authors $newsAuthor)
    {
        $this->newsAuthor = $newsAuthor;
    }
}
