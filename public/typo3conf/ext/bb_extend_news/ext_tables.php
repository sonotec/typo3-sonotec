<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_bbextendnews_domain_model_authors', 'EXT:bb_extend_news/Resources/Private/Language/locallang_csh_tx_bbextendnews_domain_model_authors.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_bbextendnews_domain_model_authors');
    }
);
