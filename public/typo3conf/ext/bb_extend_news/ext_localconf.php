<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

# Erweiterung des News-Plugin-Felxform um eigene Actions
$GLOBALS['TYPO3_CONF_VARS']['EXT']['news']['switchableControllerActions']['newItems']['News->listAuthor'] = 'Listenansicht Autoren';
# Zuweisung des erweiterten NewsConrrollers mit zusätzlicher Action zu News-Plugin
$GLOBALS['TYPO3_CONF_VARS']['EXT']['news']['classes']['Controller/NewsController'][] = 'bb_extend_news';
# Erweiterung NewsDemand für eigenes zusätzliches Feld in Abfragen
$GLOBALS['TYPO3_CONF_VARS']['EXT']['news']['classes']['Domain/Model/Dto/NewsDemand'][] = 'bb_extend_news';

/************************************************************************
 * XCLASS
 ************************************************************************/
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\GeorgRinger\News\Domain\Model\NewsDefault::class] = [
    'className' => \BB\BbExtendNews\Domain\Model\News::class,
];
// Register extended domain class
\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\Container\Container::class)
    ->registerImplementation(
        \GeorgRinger\News\Domain\Model\NewsDefault::class,
        \BB\BbExtendNews\Domain\Model\News::class
    );

$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\GeorgRinger\News\Domain\Repository\NewsRepository::class] = [
    'className' => \BB\BbExtendNews\Domain\Repository\NewsRepository::class
];
