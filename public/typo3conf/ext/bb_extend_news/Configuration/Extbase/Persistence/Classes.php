<?php

declare(strict_types = 1);
return [
    \BB\BbExtendNews\Domain\Model\News::class => [
        'tableName' => 'tx_news_domain_model_news'
    ],
];
