<?php

$tmp_extend_news_columns = [

    'news_author' => [
        'exclude' => true,
        'label' => 'Author',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'items' => [
                ['-', 0]
            ],
            'foreign_table' => 'tx_bbextendnews_domain_model_authors',
            'foreign_table_where' => 'AND tx_bbextendnews_domain_model_authors.sys_language_uid IN (-1,0) ',
            'minitems' => 0,
            'maxitems' => 1,
        ],
    ],


];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tx_news_domain_model_news',$tmp_extend_news_columns);
$GLOBALS['TCA']['tx_news_domain_model_news']['palettes']['paletteAuthor']['showitem'] = 'news_author';
