<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_sonoteccontact_domain_model_contact', 'EXT:sonotec_contact/Resources/Private/Language/locallang_csh_tx_sonoteccontact_domain_model_contact.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_sonoteccontact_domain_model_contact');
    }
);
