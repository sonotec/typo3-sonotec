<?php
namespace SONOTEC\SonotecContact\Controller;


/***
 *
 * This file is part of the "SONOTEC contact" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020
 *
 ***/


/**
 * ContactController
 */
class ContactController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * contactRepository
     *
     * @var \SONOTEC\SonotecContact\Domain\Repository\ContactRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $contactRepository = null;



    /**
     * action contact
     *
     * @return void
     */
    public function contactAction()
    {
        $data = $this->configurationManager->getContentObject()->data;

        //get id from content element - fallback to id in page properties
        $contactUid = isset($data['records']) ? intval($data['records']) : ($GLOBALS['TSFE']->page['sonotec_contact'] ? intval($GLOBALS['TSFE']->page['sonotec_contact']) : false);

        $contact = $this->contactRepository->findOneByUid($contactUid);

        $this->view->assignMultiple([
            'contact' => $contact,
            'data' => $data,
            'isFooter' => !isset($data['records']) && $GLOBALS['TSFE']->page['sonotec_contact'] && $contact!==null && $contact->getEmail()
        ]);
    }
}
