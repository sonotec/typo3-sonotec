<?php
namespace SONOTEC\SonotecContact\TCA;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;

class TcaFunctions
{
    /**
     * Returns a list of category fields for a given table for populating selector "category_field"
     * in tt_content table (called as itemsProcFunc).
     *
     * @param array $params
     */
    public function formatContactLabels(array &$params)
    {
        $sysLanguageUid = 0;

        if(is_int($params['row']['uid']) ) {

            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_sonoteccontact_domain_model_contact');
            $statement = $queryBuilder->select('sys_language_uid')
                ->from('tx_sonoteccontact_domain_model_contact')
                ->where(
                    $queryBuilder->expr()->eq('uid',$params['row']['uid'])
                )
                ->execute();

            $record = $statement->fetchAll();

            if(isset($record[0])) {
                $sysLanguageUid = $record[0]['sys_language_uid'];
            }
        }
        $params['title'] = $params['row']['name'] .  ' ['.$params['options']['lang'][$sysLanguageUid].']';
    }
}
