#
# Table structure for table 'tx_sonoteccontact_domain_model_contact'
#
CREATE TABLE tx_sonoteccontact_domain_model_contact (

	header varchar(255) DEFAULT '' NOT NULL,
	description text,
    name varchar(255) DEFAULT '' NOT NULL,
    function varchar(255) DEFAULT '' NOT NULL,
	phone varchar(255) DEFAULT '' NOT NULL,
	email varchar(255) DEFAULT '' NOT NULL,
	email_link varchar(255) DEFAULT '' NOT NULL,
	address text,
	image int(11) unsigned NOT NULL default '0',

);
