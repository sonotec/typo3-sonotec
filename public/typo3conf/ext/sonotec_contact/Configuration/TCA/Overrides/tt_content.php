<?php

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
    [
        'LLL:EXT:sonotec_contact/Resources/Private/Language/locallang_db.xlf:tx_sonotec_contact_pi1.name',
        'sonoteccontact_pi1',
        'apps-pagetree-backend-user'
    ],
    'CType',
    'sonotec_contact'
);

$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['sonoteccontact_pi1'] = 'apps-pagetree-backend-user';

$GLOBALS['TCA']['tt_content']['types']['sonoteccontact_pi1'] = [
    'showitem' => '
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.headers;headers,
        records,
      --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,snm_layout,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
         --palette--;;language,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
         --palette--;;hidden,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
         categories,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
         rowDescription,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,
   ',
    'columnsOverrides' => [
        'records' => [
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'allowed' => 'tx_sonoteccontact_domain_model_contact',
                'foreign_table' => 'tx_sonoteccontact_domain_model_contact',
                'foreign_table_where' => 'AND ORDER BY tx_sonoteccontact_domain_model_contact.name ASC',
                #'foreign_table_where' => 'AND tx_sonoteccontact_domain_model_contact.sys_language_uid IN(-1,###REC_FIELD_sys_language_uid###)',
                # 'foreign_table_where' => 'AND tx_sonoteccontact_domain_model_contact.sys_language_uid=###REC_FIELD_sys_language_uid###',
                'maxitems' => 1,
                'minitems' => 1
            ]
        ]
    ]
];

