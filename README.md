# TYPO3 Create Dummy

1. Composer installieren (https://getcomposer.org/)
2. Terminal als Administrator öffnen und ins Projektverzeichnis wechseln
3. <code>composer install</code> ausführen
4. Domain anlegen und auf "typo3/public" zeigen lassen
5. "typo3/public/typo3conf/AdditionalConfiguration.php.default" kopieren, in "AdditionalConfiguration.php" umbenennen und
instanzabhängige Parameter eintragen
6. Install-Tool aufrufen und Wizzard durcharbeiten


-----------
# Neuerungen
1. **config.yaml angelegt, enthält bereits Konfiguration für**
   - robots.txt
   - sitemap.xml
   - 404-config
   - appending slash für urls
   - konfiguriert für http://localdomain.localhost
   - Defaultsprache: deutsch

2. **Konfigurierte Inhaltselemente**
   - Header
   - Text
   - Textmedia (Bilder, Galerie & click enlarge, Video -
   mit Vorschaubild --> load YT-iframe on click)
   - Bullet List
   - Table
   - File links (download)
   - Contact
   - Teaser
   - Form (inklusive vorkonfiguriertem Kontaktformular)
   - Suche (indexed_search konfiguriert / ins Template integriert)
   - Divider
   - HTML
   - Insert Records
   - diverse Menü-Plugins

3. **Seitentemplates**
   - Frontpage
   - Default (1-spaltig)
   - 66-33 (2-spaltig)
   - 33-66 (2-spaltig)

4. **Javascript und CSS**
   - Bootstrap
   - Lightbox
   - Cookiebar

5. **In der composer.json bereits hinterlegte Extensions**
(falls nicht benötigt, entfernen!)
   - t3/min

6. **Initialer Dump, Bilder & Beispiele**
   - DB-Dump im Rootverzeichnis (typo3_v10-initial_dump)
   - example-images.zip aus Rootverzeichnis in "fileadmin/user_upload/" entpacken
   - USER snm / PW passwort123

-----------
# Lokale Entwicklung - CSS / JS
Um mit der Bibliothek lokal arbeiten zu können, benötigt man Node v13.8+ und npm v6.10+. Mit älteren Node-Versionen wird die Entwicklungsumgebung nicht getestet und somit keine Funktionalität gewährleistet.

Zunächst müssen via
```
npm install
```
alle Abhängigkeiten installiert werden.

Im Anschluss stehen die folgenden 3 npm Tasks zur Verfügung:
* `npm run serve`
    Startet den Build- und Watch-Prozess für die lokale Entwicklung. Die localhost:xxxx Domain wird automatisch im Browser geöffnet. Man muss sich dann durch die angezeigten Ordner bis zu seinem Beispiel durchnavigieren. HMR und LR ist aktiv.

* `npm run build`
    Erstellt ein Production-Build der Komponenten für JS & CSS.


-----------
# Todo
- ggf. mobiles Menü wie Wackerbarth?!
- ggf. Lazyloading für Inhaltsbilder?!
