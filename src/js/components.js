import TestClass from './components/test/TestClass';
// import Comments from './components/comments';

export default [
  {
    selector: '.site-header',
    cls: TestClass,
    options: {
      customOption: 1
    }
  },
  {
    selector: '.site-footer',
    cls: TestClass,
    options: {
      customOption: 2
    }
  },
  // {
  //   selector: '.vue-comments-wrapper',
  //   cls: Comments,
  //   options: {
  //     abc: 1
  //   }
  // }
];
