import Vue from 'vue';
import CommentsApp from './CommentsApp.vue';

class Comments {
  constructor(el, eventBus, options = {}) {
    this.el = el;
    this.eventBus = eventBus;

    this.initialize();
  }

  initialize() {
    this.comments = new (Vue.extend(CommentsApp));
    this.comments.eventBus = this.eventBus;
    this.comments.$mount(this.el.querySelector('.vue-comments'));
  }
}

export default Comments;
