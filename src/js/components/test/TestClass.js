class TestClass {
  constructor(el, eventBus, options = {}) {
    this.el = el;
    this.eventBus = eventBus;

    this.defaults = {
      customOption: false
    };

    this.settings = {
      ...this.defaults,
      ...options
    };

    console.log(this.settings);
    this.eventBus.on('window-resize', (event) => {
      console.log(event);
    });
  }
}

export default TestClass;
