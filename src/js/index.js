import 'babel-polyfill';
import * as JsEventBus from 'js-event-bus';

import '../scss/index.scss';
import components from './components';
import throttle from './util/throttle';

const eventBus = JsEventBus();

window.app = {
  eventBus
};

components.map((cmp) => {
  if (typeof cmp.onlyIf === 'function' && !cmp.onlyIf()) {
    return;
  }

  const els = document.querySelectorAll(cmp.selector);

  [...els].map(el => {
    new cmp.cls(el, eventBus, {
      selector: cmp.selector,
      ...(cmp.options || {})
    });
  });
});

window.app.components = components;

window.addEventListener('resize', throttle(() => {
  eventBus.emit('window-resize');
}, 100, false));
