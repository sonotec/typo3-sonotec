const keys = {
  LEFT_ARROW: 37,
  UP_ARROW: 38,
  RIGHT_ARROW: 39,
  DOWN_ARROW: 40,
  ESCAPE: 27,
  SPACE: 32,
  ENTER: 13,
  TAB: 9
};

export const keyMap = {
  'ArrowLeft': keys.LEFT_ARROW,
  'ArrowRight': keys.RIGHT_ARROW,
  'ArrowUp': keys.UP_ARROW,
  'ArrowDown': keys.DOWN_ARROW,
  'Escape': keys.ESCAPE,
  ' ': keys.SPACE,
  'Enter': keys.ENTER,
  'Tab': keys.TAB
};

export function getKeycode (event) {
  const key = event.key || event.keyCode || event.which;

  if (typeof key === 'number' && isFinite(key) && Math.floor(key) === key) {
    return key;
  }

  return keyMap[key];
}

export default keys;
