const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { VueLoaderPlugin } = require('vue-loader');
const cacheIdentifier = require('./build/cacheIdentifier');

const vueLoaderCacheConfig = cacheIdentifier('vue-loader', {
  'vue-loader': require('vue-loader/package.json').version,
  '@vue/component-compiler-utils': require('@vue/component-compiler-utils/package.json').version,
  'vue-template-compiler': require('vue-template-compiler/package.json').version
}, path.resolve(__dirname, '../'), [
  'package.json'
]);

const babelLoaderCacheConfig = cacheIdentifier('babel-loader', {
  '@babel/core': require('@babel/core/package.json').version,
  '@babel/preset-env': require('@babel/preset-env/package.json').version,
  'babel-loader': require('babel-loader/package.json').version
}, path.resolve(__dirname, '../'), [
  'package.json'
]);

const isProd = process.env.NODE_ENV === 'production';
const sourceMap = !isProd;

module.exports = {
  mode: isProd ? 'production' : 'development',
  entry: './src/js/index.js',
  output: {
    path: path.resolve(__dirname, 'public/typo3conf/ext/sonotec_config/Resources/Public'),
    filename: 'Js/main.js'
  },
  devtool: isProd ? false : 'source-map',
  devServer: {
    contentBase: ['./public/typo3conf/ext/sonotec_config/Resources/Public'],
    watchContentBase: true,
    open: false,
    stats: {
      children: false, // Hide children information
      maxModules: 0 // Set the maximum number of modules to be shown
    },
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
      "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
    },
    https: true,
    public: 'localhost:8080',
    host: '0.0.0.0',
    disableHostCheck: true
  },
  plugins: [
    new VueLoaderPlugin(),
    new MiniCssExtractPlugin({
      filename: 'Css/main.css'
    })
  ],
  module: {
    rules: [
      {
        test: /\.vue$/,
        use: [
          {
            loader: 'cache-loader',
            options: vueLoaderCacheConfig
          },
          {
            loader: 'vue-loader',
            options: {
              compilerOptions: {
                preserveWhiteSpace: true
              },
              optimizeSSR: false
            }
          }
        ]
      },
      {
        test: /\.js$/, //using regex to tell babel exactly what files to transcompile
        exclude: /node_modules/, // files to be ignored
        use: [
          {
            loader: 'cache-loader',
            options: babelLoaderCacheConfig
          },
          {
            loader: 'babel-loader' // specify the loader
          }
        ]
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: false,
              publicPath: '../',
            }
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: sourceMap,
              importLoaders: 2
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: sourceMap
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: sourceMap
            }
          }
        ]
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[folder]/[name].[ext]',
              outputPath: 'Fonts/'
            }
          }
        ]
      }
    ]
  }
};
