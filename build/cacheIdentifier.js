const fs = require('fs');
const hash = require('hash-sum');
const path = require('path');

// replace \r\n to \n generate consistent hash
const fixNewLine = conf => {
  if (typeof conf === 'function') {
    return conf.toString().replace(/\r\n?/g, '\n');
  }
  return conf.replace(/\r\n?/g, '\n');
};

const readConfig = (file, context) => {
  const absolutePath = (context !== false) ? path.resolve(context, file) : file;

  if (!fs.existsSync(absolutePath)) {
    return;
  }

  if (absolutePath.endsWith('.js')) {
    // should evaluate config scripts to reflect environment variable changes
    try {
      return JSON.stringify(require(absolutePath));
    } catch (e) {
      return fs.readFileSync(absolutePath, 'utf-8');
    }
  } else {
    return fs.readFileSync(absolutePath, 'utf-8');
  }
};

const getAllFilesInFolder = (folder) => {
  const files = [];

  try {
    fs.readdirSync(folder).forEach(function (file) {
      const absFilePath = path.join(folder, file);
      files.push(absFilePath);
    });
  } catch (e) {
    console.log(e);
  }

  return files;
};

// setup base variables to hash
const baseVariables = {
  'cache-loader': require('cache-loader/package.json').version,
  env: process.env.NODE_ENV,
  test: !!process.env.VUE_TEST
};

/**
 * Generate a cache identifier from a number of variables
 */
module.exports = function cacheIdentifier(id, partialIdentifier, context, configFiles = []) {
  const cnfs = [];

  // setup cache directory
  const cacheDirectory = path.resolve(context, `./node_modules/.cache/${id}`);

  const variables = {
    partialIdentifier,
    ...baseVariables,
    config: cnfs
  };

  if (!Array.isArray(configFiles)) {
    configFiles = [configFiles];
  }
  configFiles = configFiles.concat([
    'package-lock.json',
    'yarn.lock',
    'pnpm-lock.yaml'
  ]);

  for (const file of configFiles) {
    const content = readConfig(file, context);
    if (content) {
      variables.configFiles = fixNewLine(content);
      break
    }
  }

  const cacheIdentifier = hash(variables);
  return { cacheDirectory, cacheIdentifier };
};
